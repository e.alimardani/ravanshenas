import {SET_USER} from "../Actions/Type";

const initialState = {
    fullName: null,
    name:null,
    family:null,
    phoneNumber:null,
    apiToken: null,
    id:null,
    email:null
};

export default user = (state = initialState, action = {}) => {
    switch (action.type)
    {
        case SET_USER:
            const { user } = action;
            return {
                fullName: user.fullName,
                name:user.name,
                family:user.family,
                phoneNumber:user.phoneNumber,
                apiToken: user.apiToken,
                id:user.id,
                email: user.email
            };
            break;
        default:
            return state;
    }
}