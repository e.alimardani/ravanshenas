
export default global = (state = {}, action = {}) => {
    return {
        baseApiUrl: 'http://ravanshenasesabz.com/api/v1',
        adminToken: '2f13922caae23b51c852ad2c45f37efa46d89c228c5c740d75711ca4cc1dda20',
        grColorOne:'#3b43ec',
        grColorTwo:'#747af1',
        newDate:new Date().toISOString(),
        website:'http://ravanshenasesabz.com',
        appName:'روانشناس سبز',
        slogan:'روانشناس سبز'
    };
}