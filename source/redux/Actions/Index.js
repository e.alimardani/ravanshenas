import {SET_USER} from "./Type";
import {SET_APP} from "./Type";

export const setUser = (user)=>({
    type: SET_USER,
    user
});

export const setApp = (app)=>({
    type: SET_APP,
    app
});
