import React, { Component } from 'react';
import {I18nManager, Platform, BackHandler, ToastAndroid, NetInfo, Alert, Dimensions} from 'react-native';
import {Drawer, Router, Scene, Stack,Actions} from 'react-native-router-flux';
import { connect , Provider } from 'react-redux';
import store from "./redux/Store/Index";
import EStyleSheet from "react-native-extended-stylesheet";
import Orientation from 'react-native-orientation';
import RNRestart from 'react-native-restart';

import Splash from "./components/Splash/Splash";
import Intro from "./components/Intro/Intro";
import Home from "./components/Home/Home";
import MainDrawer from "./components/Shared/MainDrawer";
import Product from "./components/Product/Product";
import MyAddress from "./components/Product/MyAddress";
import MyOrder from "./components/Product/MyOrder";
import EditProfile from "./components/Profile/EditProfile";
import Cart from "./components/Product/Cart";
import Products from "./components/Product/Products";
import EditPassword from "./components/Profile/EditPassword";
import AddAddress from "./components/Product/AddAddress";
import EditAddress from "./components/Product/EditAddress";
import ProductSearch from "./components/Product/ProductSearch";
import Browser from "./components/Shared/Browser";
import SelectAddress from "./components/Product/SelectAddress";
import PdfViewer from "./components/Shared/PdfViewer";
import Posts from "./components/Blog/Posts";
import Post from "./components/Blog/Post";
import Videos from "./components/Blog/Videos";
import Video from "./components/Blog/Video";
import ContactUs from "./components/Shared/ContactUs";
import AllComments from './components/Product/AllComments'
import Movafaghiat from './components/Home/Movafaghiat';
import Aramesh from './components/Home/Aramesh';
import Salamati from './components/Home/Salamati';
import Khoshbakhti from './components/Home/Khoshbakhti'
import AboutUs from './components/Home/AboutUs'

EStyleSheet.build({
    $YekanRegular : Platform.OS === 'ios' ? 'IRANSansMobile' : 'IRANSansMobile',
    $YekanBold : Platform.OS === 'ios' ? 'IRANSansMobile' :  'IRANSansMobileBold',
    $MainColorSingleVideoPlay: '#0082f0',
    $MainColor: '#0082f0',
    $MainFontColor: '#ffffff',
    $ButtonBackgroundColor: '#ffffff',
    $ButtonFontColor: '#0082f0'
});

let backButtonPressedOnceToExit = false;
const width = Dimensions.get('window').width;
export default class App extends Component {
    constructor(props) {
        super(props);

        I18nManager.allowRTL(false);
        Orientation.lockToPortrait();

        if(I18nManager.isRTL)
        {
            RNRestart.Restart();
        }
        this.onBackPress = this.onBackPress.bind(this);
    }

    componentDidMount(){
        function handleFirstConnectivityChange(connectionInfo) {
            if(connectionInfo.type === 'none')
            {
                Alert.alert(
                    'ارتباط اینترنت شما قطع میباشد!',
                    'لطفا تنظیمات اینترنت خود را چک نمایید و مجددا درخواست خود را اجرا کنید.',
                    [
                        {text: 'تایید'}
                    ]
                )
            }
        }
        NetInfo.addEventListener(
            'connectionChange',
            handleFirstConnectivityChange
        );
    }

    onBackPress() {
        if (backButtonPressedOnceToExit) {
            BackHandler.exitApp();
        } else {
            if (Actions.currentScene !== 'splash' && Actions.currentScene !== 'home') {
                Actions.pop();
                return true;
            } else {
                backButtonPressedOnceToExit = true;
                ToastAndroid.show("برای خروج از برنامه دوباره دکمه بازگشت را بفشارید.",ToastAndroid.SHORT);
                //setting timeout is optional
                setTimeout( () => { backButtonPressedOnceToExit = false }, 2000);
                return true;
            }
        }
    }


    render() {
        const RouterWithRedux = connect()(Router);
        return (

            <Provider store={store}>
                <RouterWithRedux backAndroidHandler={this.onBackPress.bind(this)}>
                    <Stack hideNavBar>
                        <Scene key="splash" component={Splash} initial/>
                        <Scene key="intro" component={Intro} />
                        <Scene key="addAddress" component={AddAddress} />
                        <Scene key="editAddress" component={EditAddress} />
                        <Scene key="browser" component={Browser} />
                        <Scene key="selectAddress" component={SelectAddress} />
                        <Scene key="pdfViewer" component={PdfViewer} />
                        <Scene key="contact" component={ContactUs} />

                        <Drawer key="root" contentComponent={MainDrawer} drawerPosition="right"  drawerWidth={width * .65} >
                            <Scene key="main" hideNavBar initial>
                                <Scene key="home" component={Home} initial/>
                                <Scene key="products" component={Products} />
                                <Scene key="productSearch" component={ProductSearch} />
                                <Scene key="product" component={Product}  />
                                <Scene key="myAddress" component={MyAddress} />
                                <Scene key="myOrder" component={MyOrder} />
                                <Scene key="editProfile" component={EditProfile} />
                                <Scene key="editPassword" component={EditPassword} />
                                <Scene key="cart" component={Cart} />
                                <Scene key="posts" component={Posts} />
                                <Scene key="post" component={Post} />
                                <Scene key="videos" component={Videos} />
                                <Scene key="video" component={Video} />
                                <Scene key="allComments" component={AllComments} />
                                <Scene key="khoshbakhti" component={Khoshbakhti} />
                                <Scene key="movafaghiat" component={Movafaghiat} />
                                <Scene key="aramesh" component={Aramesh} />
                                <Scene key="salamati" component={Salamati} />
                                <Scene key="aboutUs" component={AboutUs} />

                            </Scene>
                        </Drawer>
                    </Stack>
                </RouterWithRedux>
            </Provider>
        );
    }
}
