import React from 'react';
import Svg,{ G, Path,Ellipse,Circle} from 'react-native-svg';

export default class OtherContactUs extends React.Component {
    render() {
        return (
            <Svg height="20" width="20" viewBox="0 0 64 42">
                <Path fill='#b7c4cb' d="M58,0H6C2.7,0,0,2.7,0,6v30c0,3.3,2.7,6,6,6h52c3.3,0,6-2.7,6-6V6C64,2.7,61.3,0,58,0z M8,4h48
	c0.6,0,1.1,0.1,1.6,0.3L33.7,25.7c-0.4,0.3-1,0.5-1.5,0.5c-0.6,0-1.1-0.2-1.5-0.6L7,4.1C7.4,4,7.7,4,8,4z M4,8
	c0-0.4,0.1-0.8,0.2-1.2l15.7,14.3l-15.7,14C4.1,34.8,4,34.4,4,34L4,8L4,8z M56,38H8c-0.3,0-0.6,0-0.8-0.1l15.8-14l5.1,4.7
	c2.4,1.9,5.7,2,8.2,0.1l5.1-4.6l15.5,13.8C56.6,38,56.3,38,56,38z M60,34c0,0.4-0.1,0.8-0.2,1.2L44.3,21.4L59.9,7.5
	c0,0.2,0,0.3,0,0.5L60,34L60,34z"/>
            </Svg>
        );
    }
}