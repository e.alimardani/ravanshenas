import React from 'react';
import Svg,{ G, Path,Ellipse,Circle} from 'react-native-svg';

export default class ProfileTabIcon extends React.Component {
    render() {
        return (
            <Svg height="23" width="23" viewBox="0 0 72.9 78">
                <Path fill={this.props.color} d="M48.1,18.9c-0.8-15.3-23.3-15.3-24.2,0C22.7,41.8,49.3,41.8,48.1,18.9z M16.6,18.5
	C17.1,7.8,26.2-0.5,36.9,0c10,0.5,18,8.5,18.5,18.5C57.3,51.8,14.7,51.8,16.6,18.5L16.6,18.5z M72,78h-7.4
	c-0.8-9,0.6-22.5-28.6-22.5C6.3,55.5,8.1,70.5,7.4,78H0l0.7-7.7C2.2,55.9,14.7,48,36,48C72.9,48,71.3,70.5,72,78z"/>
            </Svg>
        );
    }
}