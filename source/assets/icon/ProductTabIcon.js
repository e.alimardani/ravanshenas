import React from 'react';
import Svg,{ G, Path,Ellipse,Circle} from 'react-native-svg';

export default class ProductTabIcon extends React.Component {
    render() {
        return (
            <Svg height="23" width="23" viewBox="0 0 72.7 67.4">
                <Path fill={this.props.color} d="M66.1,60.8H6.6V25.1h21.2v-6.6H8.3L17,6.6h16v18.5h6.6V6.6h15.8l8.7,11.9H45v6.6h21.2
	L66.1,60.8L66.1,60.8z M58.8,0H13.7L0,18.7v48.7h72.7V18.7L58.8,0z"/>
            </Svg>
        );
    }
}