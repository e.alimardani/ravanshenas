import React from 'react';
import Svg,{ G, Path,Ellipse,Circle} from 'react-native-svg';

export default class HomeTabIcon extends React.Component {
    render() {
        return (
            <Svg height="30" width="30" viewBox="0 0 85.8 73.5">
                <Path fill={this.props.color} d="M67,66H56.5V42h-27v24H19V42.8l-7.5,7.4v23.2H37v-24h12v24h25.5V50.3L67,42.8L67,66
	L67,66z M85.8,42.6l-5.3,5.3L42.9,10.6L5.3,47.9L0,42.6L42.9,0L85.8,42.6z"/>
            </Svg>
        );
    }
}