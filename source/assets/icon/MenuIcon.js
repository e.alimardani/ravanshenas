import React from 'react';
import Svg,{ G, Path,Ellipse,Circle} from 'react-native-svg';

export default class MenuIcon extends React.Component {
    render() {
        return (
            <Svg height="30" width="30" viewBox="0 0 82.4 60">
                <Path fill="#ffffff" d="M4.3,34.7H78c2.6-0.2,4.5-2.6,4.3-5.2c-0.2-2.3-2-4.1-4.3-4.3H4.3c-2.6,0.2-4.5,2.6-4.3,5.2
	C0.2,32.7,2.1,34.5,4.3,34.7z M30,9.5h48c2.6-0.2,4.5-2.5,4.3-5.2C82.1,2,80.3,0.2,78,0H30c-2.6,0.2-4.5,2.5-4.3,5.2
	C25.9,7.5,27.7,9.3,30,9.5L30,9.5z M78,50.5H17.2c-2.6,0.2-4.5,2.5-4.3,5.2c0.2,2.3,2,4.1,4.3,4.3H78c2.6-0.2,4.5-2.5,4.3-5.2
	C82.1,52.5,80.3,50.7,78,50.5z"/>
            </Svg>
        );
    }
}