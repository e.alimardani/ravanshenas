import React from 'react';
import Svg,{ G, Path,Ellipse,Circle} from 'react-native-svg';

export default class ProfileHistoryIcon extends React.Component {
    render() {
        return (
            <Svg height="20" width="20" viewBox="0 0 64 64">
                <Path fill='#b7c4cb' d="M54,65H10c-3.3,0-6-2.7-6-6V23c0-3.3,2.7-6,6-6h8v-1.3c0-8.1,5.9-14.8,14.1-14.8C40.2,0.9,46,7.5,46,15.7
				V17h8c3.3,0,6,2.7,6,6v36C60,62.3,57.3,65,54,65L54,65z M42,15.7c0-5.9-4-10.8-9.9-10.8S22,9.8,22,15.7V17h20V15.7L42,15.7z
				 M56,25c0-2.2-1.8-4-4-4h-6v6.6c1.2,0.7,2,2,2,3.4c0,2.2-1.8,4-4,4c-2.2,0-4-1.8-4-4c0-1.5,0.8-2.8,2-3.4V21H22v6.6
				c1.2,0.7,2,2,2,3.4c0,2.2-1.8,4-4,4c-2.2,0-4-1.8-4-4c0-1.5,0.8-2.8,2-3.4V21h-6c-2.2,0-4,1.8-4,4v32c0,2.2,1.8,4,4,4h40
				c2.2,0,4-1.8,4-4V25L56,25z"/>
            </Svg>
        );
    }
}