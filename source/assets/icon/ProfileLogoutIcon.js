import React from 'react';
import Svg,{ G, Path,Ellipse,Circle} from 'react-native-svg';

export default class ProfileLogoutIcon extends React.Component {
    render() {
        return (
            <Svg height="20" width="20" viewBox="0 0 64 64">
                <Path fill='#fd5459' d="M58,64H28c-3.3,0-6-2.7-6-6V42h4v14c0,2.2,1.8,4,4,4h26c2.2,0,4-1.8,4-4V8c0-2.2-1.8-4-4-4H30
				c-2.2,0-4,1.8-4,4v14h-4V6c0-3.3,2.7-6,6-6h30c3.3,0,6,2.7,6,6v52C64,61.3,61.3,64,58,64L58,64z M7.1,30H44c1.1,0,2,0.9,2,2
				c0,1.1-0.9,2-2,2H7.1l6.3,6.3c0.8,0.8,0.8,2.2,0,3.1c-0.8,0.8-2.2,0.8-3.1,0l-9.7-9.7C0.2,33.2,0,32.6,0,32
				c0-0.6,0.2-1.2,0.6-1.7l9.7-9.7c0.8-0.8,2.2-0.8,3.1,0c0.8,0.8,0.8,2.2,0,3.1L7.1,30L7.1,30z M24,24c-1.1,0-2-0.9-2-2
				c0-1.1,0.9-2,2-2c1.1,0,2,0.9,2,2C26,23.1,25.1,24,24,24L24,24z M24,44c-1.1,0-2-0.9-2-2c0-1.1,0.9-2,2-2c1.1,0,2,0.9,2,2
				C26,43.1,25.1,44,24,44L24,44z"/>
            </Svg>
        );
    }
}