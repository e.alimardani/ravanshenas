import EStyleSheet from 'react-native-extended-stylesheet';
import {Platform,Dimensions} from "react-native";
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
export const styles = EStyleSheet.create({
    container: {
        width: width,
        height: height,
        alignSelf: 'center',
        justifyContent: 'flex-start',
        backgroundColor: '#fff'
    },
    image_load: {width: width, height: height / 1.5},
    btn1: {
        width: width * .70,
        height: width * .15,
        borderRadius: width * .05,
        backgroundColor: '#26d735',
        alignSelf: 'center',
        marginTop: height * .05,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    btn2: {
        width: width * .70,
        height: width * .15,
        borderRadius: width * .05,
        backgroundColor: '#147eff',
        alignSelf: 'center',
        marginTop: height * .03,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewSpinner: {width: width, height: height * .33, alignItems: 'center', justifyContent: 'center'},
    txtBtn: {fontSize: width / 20, color: '#fff', fontFamily: '$YekanRegular',},
    imgBtn: {width: width * .10, height: height * .05, marginHorizontal: width * .05},
    splashBottomSection:{
        backgroundColor:'transparent',
        position:'absolute' ,
        width:Dimensions.get('window').width,
        height:Dimensions.get('window').height * 0.3,
        bottom:0,
        justifyContent:'center',
        alignItems:'center'
    },
    buttonTransparentText:{
        textAlign:'center',
        writingDirection:'rtl',
        fontSize:14,
        color:'#393939',
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'normal'
            },
            android: {
                fontFamily: '$YekanRegular'
            }
        }),
    }
});


export default styles;
