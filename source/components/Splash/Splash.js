import React from 'react';
import {
    StatusBar,
    Image,
    AsyncStorage,
    ActivityIndicator,
    Platform,
    TouchableOpacity,
    ImageBackground
} from 'react-native';
import {Container, Text, View} from 'native-base';
import styles from './Style';
import {Actions} from 'react-native-router-flux';
import {setUser} from "../../redux/Actions/Index";
import {connect} from "react-redux";
import Helpers from "../Shared/Helper";

class Splash extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: true
        }
    }

    async componentDidMount() {
        if (await Helpers.CheckNetInfo()) {
            await this.setState({
                loading: false
            });
        }

        if (await AsyncStorage.getItem("apiToken") !== null) {
            let apiToken = await AsyncStorage.getItem("apiToken");

            try {
                let formData = new FormData();
                formData.append("admintoken", this.props.global.adminToken);
                formData.append("usertoken", apiToken);

                let response = await fetch(this.props.global.baseApiUrl + '/user/get',
                    {
                        method: "POST",
                        body: formData
                    });

                if (response.status !== 200) {
                    await this.setState({
                        loading: false
                    });
                } else {
                    let json = await response.json();
                    if (json.success) {
                        console.warn(json.data.firstname)
                        await this.props.setUser({
                            fullName: json.data.firstname + " " + json.data.lastname,
                            name: json.data.firstname,
                            family:json.data.lastname,
                            username: json.data.username,
                            phone_number: json.data.phone_number,
                            apiToken: json.data.token,
                            id: json.data.id,
                            email: json.data.email
                        });
                        await Actions.reset('root');

                    } else {
                        await this.setState({
                            loading: false
                        });
                    }
                }

            } catch (error) {
                await this.setState({
                    loading: false
                });
            }

        } else {
            setTimeout(async () => {
                await this.setState({
                    loading: false
                });
            }, 1000)
        }
    }

    render() {
console.log(this.props.user)
        return (
            <View style={styles.container}>
                <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content"/>
                <Image source={require('../../assets/images/splash/splashSabz.png')} style={styles.image_load}
                       resizeMode={'stretch'}/>
                {(this.state.loading) &&
                    <View style={styles.splashBottomSection}>
                        <ActivityIndicator size='large' color='#0082f0' />
                    </View>}
                {(!this.state.loading) &&
                <View>
                    <TouchableOpacity activeOpacity={.7} style={styles.btn1} onPress={() => Actions.push('intro',{initialIndex:1})}>
                        <Text style={styles.txtBtn}>{'ورود'}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={.7} style={styles.btn2}
                                      onPress={() => Actions.push('intro',{initialIndex:0})}>
                        <Text style={styles.txtBtn}>{'عضویت'}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{marginTop: 10}} activeOpacity={0.8} onPress={async () => await Actions.reset('root')}>
                      <Text style={styles.buttonTransparentText}>ورود بدون عضویت و ورود</Text>
                    </TouchableOpacity>
                </View>
                }
            </View>
            // <View style={styles.mainContainer}>
            //     <StatusBar backgroundColor="transparent" translucent={true} barStyle="dark-content" />
            //
            //     <View style={styles.splashMainSection}>
            //         <Image style={styles.splashImage} source={require("./../../assets/images/shared/splash.png")} />
            //     </View>
            //
            //     {(this.state.loading) &&
            //     <View style={styles.splashBottomSection}>
            //         <ActivityIndicator size='large' color='white' />
            //     </View>}
            //
            //     {(!this.state.loading) &&
            //     <View style={styles.splashBottomSection}>
            //         <TouchableOpacity style={styles.button} activeOpacity={0.8} onPress={() => Actions.push('intro',{initialIndex:1})}>
            //             <Text style={styles.buttonText}>ورود</Text>
            //         </TouchableOpacity>
            //
            //         <TouchableOpacity style={styles.button} activeOpacity={0.8} onPress={() => Actions.push('intro',{initialIndex:0})}>
            //             <Text style={styles.buttonText}>عضو شوید</Text>
            //         </TouchableOpacity>
            //
            //         {/*<TouchableOpacity style={{marginTop: 15,borderBottomWidth: 1,borderBottomColor:'#cccccc',paddingBottom: 10}} activeOpacity={0.8} onPress={() => Actions.push('intro',{initialIndex:0})}>*/}
            //         {/*    <Text style={styles.buttonTransparentText}>حساب کاربری ندارید؟ عضو شوید.</Text>*/}
            //         {/*</TouchableOpacity>*/}
            //
            //         <TouchableOpacity style={{marginTop: 10}} activeOpacity={0.8} onPress={async () => await Actions.reset('root')}>
            //             <Text style={styles.buttonTransparentText}>ورود به صورت مهمان</Text>
            // {/*        </TouchableOpacity>*/}
            // {/*    </View>}*/}

            // {/*</View>*/}
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setUser: user => {
            dispatch(setUser(user))
        }
    }
};

const mapStateToProps = (state) => {
    return {
        user: state.user,
        global: state.global
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Splash);