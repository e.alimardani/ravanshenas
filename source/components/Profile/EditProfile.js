import React from 'react';
import {
    StatusBar,
    Image,
    TextInput,
    ActivityIndicator,
    Text,
    Dimensions,
    TouchableOpacity,
    View,
    AsyncStorage,
} from 'react-native';
import {Header, Icon, Left, Right, Content, Container, Body} from 'native-base';
import styles from './Style';
import { Actions } from 'react-native-router-flux';
import {setUser} from "../../redux/Actions/Index";
import {connect} from "react-redux";
import HeaderRightLabel from "../Shared/HeaderRightLabel";
import BasketButton from "../Shared/BasketButton";
import BackButton from "../Shared/BackButton";
import SearchButton from "../Shared/SearchButton";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import OtherPasswordIcon from "../../assets/icon/OtherPasswordIcon";
import EditProfilePersonIcon from "../../assets/icon/EditProfilePersonIcon";
import EditProfileCalendarIocn from "../../assets/icon/EditProfileCalendarIocn";
import EditProfileEmailIcon from "../../assets/icon/EditProfileEmailIcon";
import EditProfileMobileIcon from "../../assets/icon/EditProfileMobileIcon";
import LinearGradient from "react-native-linear-gradient";
import Helpers from "../Shared/Helper";
import RequestModal from "../Shared/RequestModal";


class EditProfile extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            name:this.props.user.name,
            family:this.props.user.family,
            email:this.props.user.email,
            phoneNumber:this.props.user.phoneNumber,
            //Request Modal
            showModal:false,
            hasError:false,
            errorDesc:'',
            loading:false
        };

        this.onNameChanged = this.onNameChanged.bind(this);
        this.onFamilyChanged = this.onFamilyChanged.bind(this);
        this.onEmailChanged = this.onEmailChanged.bind(this);
        this.editProfile = this.editProfile.bind(this);
    }

    onNameChanged(text) {
        this.setState({
            name:text
        });
    }
    onFamilyChanged(text) {
        this.setState({
            family:text
        });
    }
    onEmailChanged(text) {
        this.setState({
            email:text
        });
    }

    async editProfile(){
        if(await Helpers.CheckNetInfo()){
            return;
        }

        await this.setState({
            showModal:true,
            loading:true,
            hasError:false,
            errorDesc:''
        });

        //Validation
        let errors = '';

        if(this.state.name === '')
        {
            errors += 'نام نمیتواند خالی باشد. \n'
        }

        if(this.state.family === '')
        {
            errors += 'نام خانوادگی نمیتواند خالی باشد. \n'
        }

        if(this.state.email === '')
        {
            errors += 'پست الکترونیک نمیتواند خالی باشد. \n'
        }
        else {
            let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

            if(!reg.test(this.state.email)){
                errors += 'پست الکترونیک معتبر نیست \n'
            }
        }

        if(errors !== '')
        {
            await this.setState({
                loading:false,
                hasError:true,
                errorDesc:errors
            });
        }
        else {
            //Request
            try {
                let formData = new FormData();
                formData.append("admintoken", this.props.global.adminToken);
                formData.append("usertoken", this.props.user.apiToken);

                formData.append("username", this.state.phoneNumber);
                formData.append("email", this.state.email);
                formData.append("firstname", this.state.name);
                formData.append("lastname", this.state.family);
                //formData.append("password", this.state.pass);
                formData.append("phone_number", this.state.phoneNumber);

                let response = await fetch(this.props.global.baseApiUrl + '/user/update',
                    {
                        method: "POST",
                        body: formData
                    });
                let json = await response.json();

                if(response.status === 200 && json.success)
                {
                    await this.props.setUser({
                        fullName: json.data.firstname + " " + json.data.lastname,
                        name:json.data.firstname,
                        family:json.data.lastname,
                        phoneNumber:json.data.phone_number,
                        apiToken: json.data.token,
                        id:json.data.id,
                        email:json.data.email
                    });

                    await this.setState({
                        loading:false,
                        hasError: false,
                        errorDesc: 'با موفقیت انجام شد.'
                    });

                    setTimeout(async () => {
                        await this.setState({
                            showModal:false
                        });
                    },1500)
                }
                else {
                    let messages = '';

                    if(json.messages.length > 0){
                        json.messages.forEach(item => {
                            messages += item + '\n'
                        })
                    }

                    await this.setState({
                        loading: false,
                        hasError: true,
                        errorDesc: messages
                    });
                }

            } catch (error) {
                //console.log(error);
                this.setState({
                    loading: false,
                    hasError: true,
                    errorDesc: 'ارتباط با سرور برقرار نشد!' + '\n' + 'دوباره تلاش کنید'
                });
            }
        }
    }


    render() {

        return (
            <Container style={styles.mainContainer}>
                <Image resizeMode='stretch' style={[styles.mainBackground,{height:1000}]} source={require("./../../assets/images/shared/menu-bg.png")} />

                <Header style={styles.header}>
                    <Left style={{flex:1,flexDirection:'row',alignItems:'center',justifyContent:'flex-start'}}>
                        <BackButton/>
                        <SearchButton/>
                        <BasketButton/>
                    </Left>
                    <Right style={{flex:2,alignItems:'center',justifyContent:'flex-end'}}>
                        <HeaderRightLabel title='ویرایش مشخصات'/>
                    </Right>
                </Header>

                <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content" />

                <KeyboardAwareScrollView style={{flex:1,backgroundColor:'#ffffff'}} keyboardShouldPersistTaps={'handled'}>
                    <Text style={styles.profileSectionTitle}>مشخصات شما</Text>
                    <View style={styles.profileFormContainer}>
                        <View style={styles.formRow}>
                            <TextInput underlineColorAndroid="rgba(0,0,0,0)" style={styles.textInput}
                                       placeholderTextColor="#cccccc" placeholder='نام' value={this.state.name}
                                       onChangeText={this.onNameChanged}/>
                            <EditProfilePersonIcon/>
                        </View>
                        <View style={styles.formRow}>
                            <TextInput underlineColorAndroid="rgba(0,0,0,0)" style={styles.textInput}
                                       placeholderTextColor="#cccccc" placeholder='نام خانوادگی' value={this.state.family}
                                       onChangeText={this.onFamilyChanged}/>
                            <EditProfilePersonIcon/>
                        </View>
                        <View style={styles.formRow}>
                            <TextInput underlineColorAndroid="rgba(0,0,0,0)" style={styles.textInput}
                                       placeholderTextColor="#cccccc" placeholder='پست الکترونیک' value={this.state.email}
                                       onChangeText={this.onEmailChanged} keyboardType="email-address"/>
                            <EditProfileEmailIcon/>
                        </View>
                        <View style={styles.formRow}>
                            <TextInput underlineColorAndroid="rgba(0,0,0,0)" style={styles.textInput} editable={false} keyboardType="number-pad" maxLength={11}
                                       placeholderTextColor="#cccccc" placeholder='تلفن همراه' value={Helpers.ToPersianNumber(this.props.user.phoneNumber)}/>
                            <EditProfileMobileIcon/>
                        </View>

                        <TouchableOpacity style={[styles.colorGrButton,{marginTop:20}]} activeOpacity={.8} onPress={this.editProfile}>
                            <LinearGradient colors={[this.props.global.grColorOne,this.props.global.grColorTwo]} style={styles.grButtonGradient} start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}>
                                <Text style={styles.grButtonText}>ذخیره تغییرات</Text>
                            </LinearGradient>
                        </TouchableOpacity>
                    </View>
                </KeyboardAwareScrollView>

                <RequestModal showModal={this.state.showModal} setter={this} loading={this.state.loading} hasError={this.state.hasError} errorDesc={this.state.errorDesc}/>

            </Container>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setUser : user => {
            dispatch(setUser(user))
        }
    }
};

const mapStateToProps =(state) =>  {
    return{
        user: state.user,
        global:state.global
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(EditProfile);