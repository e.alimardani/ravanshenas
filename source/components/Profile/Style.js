import EStyleSheet from 'react-native-extended-stylesheet';
import {Platform,Dimensions,PixelRatio,StatusBar} from "react-native";

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
export const styles = EStyleSheet.create({
    mainContainer : {
        backgroundColor:'$MainColor'
    },
    mainBackground:{
        position: 'absolute',
        width:Dimensions.get('window').width,
        height:Dimensions.get('window').height *.43,
        //opacity:0.7
    },
    header : {
        backgroundColor : 'transparent',
        borderBottomWidth:0,
        elevation: 0,
        ...Platform.select({
            ios: {

            },
            android: {
                marginTop:StatusBar.currentHeight
            }
        }),
    },
    profileSectionTitle:{
        textAlign: 'right',
        color: '#393939',
        writingDirection: 'rtl',
        fontSize: 16,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold',
                //paddingTop:10
            },
            android: {
                fontFamily: '$YekanBold'
            }
        }),
        paddingHorizontal: 15,
        marginTop: 20
    },
    profileFormContainer:{
        borderWidth:5/PixelRatio.get(),
        borderColor:'#eceff1',
        margin:20,
        borderRadius:20,
        marginTop:20,
        marginBottom: 0,
        paddingBottom: 20
    },
    formRow:{
        flexDirection: 'row',
        justifyContent:'center',
        alignItems:'center',
        borderBottomWidth:5/PixelRatio.get(),
        borderBottomColor:'#eceff1',
        marginHorizontal: 10,
        paddingVertical: 10
    },
    textInput:{
        textAlign:'right',
        writingDirection:'rtl',
        fontSize:14,
        color:'#393939' ,
        paddingVertical:2,
        paddingHorizontal:0,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'normal',
                //paddingTop:10
            },
            android: {
                fontFamily: '$YekanRegular'
            }
        }),
        width: '90%',
        marginRight:10
        //backgroundColor:'#800',
    },
    colorGrButton:{
        alignSelf:'center',
        backgroundColor:'$ButtonBackgroundColor',
        width:'90%',
        height:40,
        justifyContent:'center',
        alignItems:'center',
        borderRadius:20,
        ...Platform.select({
            ios: {
                shadowColor: '$MainColor',
                shadowOffset: { width: 0, height: 0 },
                shadowOpacity: 0.6,
                shadowRadius: 7,
            },
            android: {
                elevation: 2
            }
        }),
    },
    grButtonGradient:{
        width:'100%',
        height:40,
        borderRadius:20,
        justifyContent:'center',
        alignItems:'center',
        paddingHorizontal:40
    },
    grButtonText:{
        textAlign:'center',
        writingDirection:'rtl',
        fontSize:14,
        //lineHeight:14,
        color:'#fff',
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold'
            },
            android: {
                fontFamily: '$YekanBold',
            }
        }),
    },
    eyeButton:{
        position: 'absolute',
        top:18,
        left:5,
    },
    eyeButtonIcon:{
        fontSize:24,
        color:'$MainColor'
    }
});


export default styles;
