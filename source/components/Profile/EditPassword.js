import React from 'react';
import {
    StatusBar,
    Image,
    TextInput,
    ActivityIndicator,
    Text,
    Dimensions,
    TouchableOpacity,
    View,
    AsyncStorage,
} from 'react-native';
import {Header, Icon, Left, Right, Content, Container, Body} from 'native-base';
import styles from './Style';
import { Actions } from 'react-native-router-flux';
import {setUser} from "../../redux/Actions/Index";
import {connect} from "react-redux";
import HeaderRightLabel from "../Shared/HeaderRightLabel";
import BasketButton from "../Shared/BasketButton";
import BackButton from "../Shared/BackButton";
import SearchButton from "../Shared/SearchButton";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import OtherPasswordIcon from "../../assets/icon/OtherPasswordIcon";
import EditProfilePersonIcon from "../../assets/icon/EditProfilePersonIcon";
import EditProfileCalendarIocn from "../../assets/icon/EditProfileCalendarIocn";
import EditProfileEmailIcon from "../../assets/icon/EditProfileEmailIcon";
import EditProfileMobileIcon from "../../assets/icon/EditProfileMobileIcon";
import LinearGradient from "react-native-linear-gradient";
import Helpers from "../Shared/Helper";
import RequestModal from "../Shared/RequestModal";


class EditPassword extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            name:this.props.user.name,
            family:this.props.user.family,
            email:this.props.user.email,
            phoneNumber:this.props.user.phoneNumber,
            password:'',
            password2:'',
            passOn:true,
            pass2On:true,
            //Request Modal
            showModal:false,
            hasError:false,
            errorDesc:'',
            loading:false
        };

        this.onPasswordChanged = this.onPasswordChanged.bind(this);
        this.onPassword2Changed = this.onPassword2Changed.bind(this);

        this.editPassword = this.editPassword.bind(this);
        this.logOutUser = this.logOutUser.bind(this);
    }

    onPasswordChanged(text) {
        this.setState({
            password:text
        });
    }
    onPassword2Changed(text) {
        this.setState({
            password2:text
        });
    }

    async editPassword(){
        if(await Helpers.CheckNetInfo()){
            return;
        }

        await this.setState({
            showModal:true,
            loading:true,
            hasError:false,
            errorDesc:''
        });

        //Validation
        let errors = '';

        if(this.state.password === '')
        {
            errors += 'رمز عبور نمیتواند خالی باشد. \n'
        }
        else if(this.state.password.length < 6 || this.state.password.length > 100)
        {
            errors += 'رمز عبور باید بین ۶ تا ۱۰۰ کاراکتر باشد. \n'
        }

        if(this.state.password2 === '')
        {
            errors += 'تکرار رمز عبور نمیتواند خالی باشد. \n'
        }
        else if(this.state.password2.length < 6 || this.state.password2.length > 100)
        {
            errors += 'تکرار رمز عبور باید بین ۶ تا ۱۰۰ کاراکتر باشد. \n'
        }

        if(this.state.password2 !== this.state.password)
        {
            errors += 'رمزعبور با تکرار آن مغایرت دارد. \n'
        }

        if(errors !== '')
        {
            await this.setState({
                loading:false,
                hasError:true,
                errorDesc:errors
            });
        }
        else {
            //Request
            try {
                let formData = new FormData();
                formData.append("admintoken", this.props.global.adminToken);
                formData.append("usertoken", this.props.user.apiToken);

                formData.append("username", this.state.phoneNumber);
                formData.append("email", this.state.email);
                formData.append("firstname", this.state.name);
                formData.append("lastname", this.state.family);
                formData.append("password", this.state.password);
                formData.append("phone_number", this.state.phoneNumber);

                let response = await fetch(this.props.global.baseApiUrl + '/user/update',
                    {
                        method: "POST",
                        body: formData
                    });

                let json = await response.json();

                if(response.status === 200 && json.success)
                {
                    await this.setState({
                        loading:false,
                        hasError: false,
                        errorDesc: 'با موفقیت انجام شد.'
                    });

                    setTimeout(async () => {
                        this.logOutUser();
                    },1500)
                }
                else {
                    let messages = '';

                    if(json.messages.length > 0){
                        json.messages.forEach(item => {
                            messages += item + '\n'
                        })
                    }

                    await this.setState({
                        loading: false,
                        hasError: true,
                        errorDesc: messages
                    });
                }

            } catch (error) {
                //console.log(error);
                this.setState({
                    loading: false,
                    hasError: true,
                    errorDesc: 'ارتباط با سرور برقرار نشد!' + '\n' + 'دوباره تلاش کنید'
                });
            }
        }
    }

    async logOutUser(){
        AsyncStorage.removeItem("apiToken");
        AsyncStorage.removeItem("cart");

        this.props.setUser({
            fullName: null,
            name:null,
            family:null,
            phoneNumber:null,
            apiToken: null,
            id:null,
            email:null
        });

        this.setState({
            showModal:false
        });

        await Actions.reset('intro',{initialIndex:1});
    }

    render() {

        return (
            <Container style={styles.mainContainer}>
                <Image resizeMode='stretch' style={[styles.mainBackground,{height:1000}]} source={require("./../../assets/images/shared/menu-bg.png")} />

                <Header style={styles.header}>
                    <Left style={{flex:1,flexDirection:'row',alignItems:'center',justifyContent:'flex-start'}}>
                        <BackButton/>
                        <SearchButton/>
                        <BasketButton/>
                    </Left>
                    <Right style={{flex:2,alignItems:'center',justifyContent:'flex-end'}}>
                        <HeaderRightLabel title='تغییر رمز عبور'/>
                    </Right>
                </Header>

                <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content" />

                <KeyboardAwareScrollView style={{flex:1,backgroundColor:'#ffffff'}} keyboardShouldPersistTaps={'handled'}>
                    <Text style={styles.profileSectionTitle}>تغییر رمز عبور</Text>
                    <View style={styles.profileFormContainer}>
                        {/*<View style={styles.formRow}>*/}
                            {/*<TextInput underlineColorAndroid="rgba(0,0,0,0)" style={styles.textInput}*/}
                                       {/*placeholderTextColor="#cccccc" placeholder='رمزعبور فعلی'/>*/}
                            {/*<OtherPasswordIcon/>*/}
                        {/*</View>*/}
                        <View style={styles.formRow}>
                            <TextInput underlineColorAndroid="rgba(0,0,0,0)" style={styles.textInput}
                                       placeholderTextColor="#cccccc" placeholder='رمزعبور جدید' value={this.state.password}
                                       secureTextEntry={this.state.passOn}
                                       onChangeText={this.onPasswordChanged}/>
                            <OtherPasswordIcon/>

                            {this.state.passOn && <TouchableOpacity activeOpacity={.7} style={styles.eyeButton} onPress={() => this.setState({passOn:false})} hitSlop={{top: 10, left: 10, bottom: 10, right: 10}}>
                                <Icon name='md-eye-off' style={styles.eyeButtonIcon}/>
                            </TouchableOpacity>}
                            {!this.state.passOn && <TouchableOpacity activeOpacity={.7} style={styles.eyeButton} onPress={() => this.setState({passOn:true})} hitSlop={{top: 10, left: 10, bottom: 10, right: 10}}>
                                <Icon name='md-eye' style={styles.eyeButtonIcon}/>
                            </TouchableOpacity>}
                        </View>
                        <View style={styles.formRow}>
                            <TextInput underlineColorAndroid="rgba(0,0,0,0)" style={styles.textInput}
                                       placeholderTextColor="#cccccc" placeholder='تکرار رمزعبور جدید' value={this.state.password2}
                                       secureTextEntry={this.state.pass2On}
                                       onChangeText={this.onPassword2Changed}/>
                            <OtherPasswordIcon/>

                            {this.state.pass2On && <TouchableOpacity activeOpacity={.7} style={styles.eyeButton} onPress={() => this.setState({pass2On:false})} hitSlop={{top: 10, left: 10, bottom: 10, right: 10}}>
                                <Icon name='md-eye-off' style={styles.eyeButtonIcon}/>
                            </TouchableOpacity>}
                            {!this.state.pass2On && <TouchableOpacity activeOpacity={.7} style={styles.eyeButton} onPress={() => this.setState({pass2On:true})} hitSlop={{top: 10, left: 10, bottom: 10, right: 10}}>
                                <Icon name='md-eye' style={styles.eyeButtonIcon}/>
                            </TouchableOpacity>}
                        </View>
                        <TouchableOpacity style={[styles.colorGrButton,{marginTop:20}]} activeOpacity={.8} onPress={this.editPassword}>
                            <LinearGradient colors={[this.props.global.grColorOne,this.props.global.grColorTwo]} style={styles.grButtonGradient} start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}>
                                <Text style={styles.grButtonText}>به روزرسانی رمزعبور</Text>
                            </LinearGradient>
                        </TouchableOpacity>
                    </View>

                    <RequestModal showModal={this.state.showModal} setter={this} loading={this.state.loading} hasError={this.state.hasError} errorDesc={this.state.errorDesc}/>

                </KeyboardAwareScrollView>

            </Container>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setUser : user => {
            dispatch(setUser(user))
        }
    }
};

const mapStateToProps =(state) =>  {
    return{
        user: state.user,
        global:state.global
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(EditPassword);