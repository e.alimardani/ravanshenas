import React from 'react';
import {StatusBar, Image, TextInput, ActivityIndicator, AsyncStorage,TouchableOpacity,Dimensions} from 'react-native';
import {Container, Icon, Text, View} from 'native-base';
import styles from './Style';
import LinearGradient from 'react-native-linear-gradient';
import { Actions } from 'react-native-router-flux';
import {setUser} from "../../redux/Actions/Index";
import {connect} from "react-redux";
import Swiper from 'react-native-swiper';
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import RequestModal from "../Shared/RequestModal";
import Helpers from "../Shared/Helper";

const width = Dimensions.get('window').width;
class Intro extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            currentIndex:props.initialIndex,
            swiperHeight:props.initialIndex === 0 ? Dimensions.get('window').height * .75 :Dimensions.get('window').height * .45,
            //Register
            name:'',
            family:'',
            email:'',
            phoneNumber:'',
            pass:'',
            passOn:true,
            //Login
            username:'',
            password:'',
            loginPassOn:true,
            //Request Modal
            showModal:false,
            hasError:false,
            errorDesc:'',
            loading:false
        };

        this.changeIndex = this.changeIndex.bind(this);

        this.onNameChanged = this.onNameChanged.bind(this);
        this.onFamilyChanged = this.onFamilyChanged.bind(this);
        this.onEmailChanged = this.onEmailChanged.bind(this);
        this.onPhoneNumberChanged = this.onPhoneNumberChanged.bind(this);
        this.onPassChanged = this.onPassChanged.bind(this);

        this.onUserNameChanged = this.onUserNameChanged.bind(this);
        this.onPasswordChanged = this.onPasswordChanged.bind(this);

        this.registerRequest = this.registerRequest.bind(this);

        this.loginRequest = this.loginRequest.bind(this);
    }

    async changeIndex(index){
        await this.setState({
            currentIndex:index,
            swiperHeight:index === 0 ? Dimensions.get('window').height * .75 :Dimensions.get('window').height * .45
        });
    }

    onNameChanged(text) {
        this.setState({
            name:text
        });
    }
    onFamilyChanged(text) {
        this.setState({
            family:text
        });
    }
    onEmailChanged(text) {
        this.setState({
            email:text
        });
    }
    onPhoneNumberChanged(text) {
        this.setState({
            phoneNumber:text
        });
    }
    onPassChanged(text) {
        this.setState({
            pass:text
        });
    }
    onPasswordChanged(text) {
        this.setState({
            password:text
        });
    }
    onUserNameChanged(text) {
        this.setState({
            username:text
        });
    }

    async registerRequest(){

        if(await Helpers.CheckNetInfo()){
            return;
        }

        await this.setState({
            showModal:true,
            loading:true,
            hasError:false,
            errorDesc:''
        });

        //Validation
        let errors = '';

        if(this.state.name === '')
        {
            errors += 'نام نمیتواند خالی باشد. \n'
        }

        if(this.state.family === '')
        {
            errors += 'نام خانوادگی نمیتواند خالی باشد. \n'
        }

        if(this.state.email === '')
        {
            errors += 'پست الکترونیک نمیتواند خالی باشد. \n'
        }
        else {
            let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

            if(!reg.test(this.state.email)){
                errors += 'پست الکترونیک معتبر نیست \n'
            }
        }
        // if(this.state.email !== ''){
        //     let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        //
        //     if(!reg.test(this.state.email)){
        //         errors += 'پست الکترونیک معتبر نیست \n'
        //     }
        // }

        if(this.state.phoneNumber === '')
        {
            errors += 'تلفن همراه نمیتواند خالی باشد. \n'
        }
        else if(this.state.phoneNumber.length < 11){
            errors += 'شماره موبایل معتبر نیست \n'
        }

        if(this.state.pass === '')
        {
            errors += 'رمز عبور نمیتواند خالی باشد. \n'
        }
        else if(this.state.pass.length < 6 || this.state.pass.length > 100)
        {
            errors += 'رمز عبور باید بین ۶ تا ۱۰۰ کاراکتر باشد. \n'
        }

        if(errors !== '')
        {
            await this.setState({
                loading:false,
                hasError:true,
                errorDesc:errors
            });
        }
        else {
            //Request
            try {
                let formData = new FormData();
                formData.append("admintoken", this.props.global.adminToken);

                formData.append("username", this.state.phoneNumber);
                formData.append("email", this.state.email);
                formData.append("firstname", this.state.name);
                formData.append("lastname", this.state.family);
                formData.append("password", this.state.pass);
                formData.append("phone_number", this.state.phoneNumber);

                let response = await fetch(this.props.global.baseApiUrl + '/user/adduser',
                    {
                        method: "POST",
                        body: formData
                    });

                let json = await response.json();

                if(response.status === 200 && json.success)
                {
                    //console.log(json);
                    await AsyncStorage.setItem("apiToken",json.data.token);

                    await this.props.setUser({
                        fullName: json.data.firstname + " " + json.data.lastname,
                        name:json.data.firstname,
                        family:json.data.lastname,
                        phoneNumber:json.data.phone_number,
                        apiToken: json.data.token,
                        id:json.data.id,
                        email:json.data.email
                    });
                    console.warn(this.props.user)
                    await this.setState({
                        showModal:false
                    });
                    await Actions.reset('root');
                }
                else {
                    let messages = '';

                    if(json.messages.length > 0){
                        json.messages.forEach(item => {
                            messages += item + '\n'
                        })
                    }

                    await this.setState({
                        loading: false,
                        hasError: true,
                        errorDesc: messages
                    });
                }

            } catch (error) {
                console.log(error);
                this.setState({
                    loading: false,
                    hasError: true,
                    errorDesc: 'ارتباط با سرور برقرار نشد!' + '\n' + 'دوباره تلاش کنید'
                });
            }
        }

    }

    async loginRequest(){

        if(await Helpers.CheckNetInfo()){
            return;
        }

        await this.setState({
            showModal:true,
            loading:true,
            hasError:false,
            errorDesc:''
        });

        //Validation
        let errors = '';

        if(this.state.username === '')
        {
            errors += 'نام کاربری نمیتواند خالی باشد. \n'
        }
        // else if(this.state.username.length < 11){
        //     errors += 'شماره موبایل معتبر نیست \n'
        // }

        if(this.state.password === '')
        {
            errors += 'رمز عبور نمیتواند خالی باشد. \n'
        }
        else if(this.state.password.length < 6 || this.state.password.length > 100)
        {
            errors += 'رمز عبور باید بین ۶ تا ۱۰۰ کاراکتر باشد. \n'
        }

        if(errors !== '')
        {
            await this.setState({
                loading:false,
                hasError:true,
                errorDesc:errors
            });
        }
        else {
            //Request
            try {
                let formData = new FormData();
                formData.append("admintoken", this.props.global.adminToken);

                formData.append("username", this.state.username);
                formData.append("password", this.state.password);

                let response = await fetch(this.props.global.baseApiUrl + '/user/login',
                    {
                        method: "POST",
                        body: formData
                    });

                let json = await response.json();

                if(response.status === 200 && json.success)
                {
                    await AsyncStorage.setItem("apiToken",json.data.token);
                    await this.props.setUser({
                        fullName: json.data.firstname + " " + json.data.lastname,
                        name:json.data.firstname,
                        family:json.data.lastname,
                        phoneNumber:json.data.phone_number,
                        apiToken: json.data.token,
                        id:json.data.id,
                        email:json.data.email
                    });
            
                    await this.setState({
                        showModal:false
                    });
                    await Actions.reset('root');
                }
                else {
                    await this.setState({
                        loading: false,
                        hasError: true,
                        errorDesc: json.messages
                    });
                }
            } catch (error) {
                console.log(error);
                this.setState({
                    loading: false,
                    hasError: true,
                    errorDesc: 'ارتباط با سرور برقرار نشد!' + '\n' + 'دوباره تلاش کنید'
                });
            }
        }
    }

    render() {

        const dot = <View style={styles.dot} />;
        const activeDot = <View style={styles.activeDot} />;

        return (
            <View style={styles.mainContainer}>
                <Image resizeMode='stretch' style={[styles.mainBackground,{height:1000}]} source={require("./../../assets/images/shared/menu-bg.png")} />

                <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content" />

                <KeyboardAwareScrollView style={{flex:1}} keyboardShouldPersistTaps={'handled'}>
                    <View style={[styles.logoSection,{marginTop:width*.20}]}>
                        <Image resizeMode="stretch" style={styles.logoImage} source={require("./../../assets/images/splash/logo.png")} />
                        {/*<Text style={styles.logoSlogan}>{this.props.global.slogan}</Text>*/}
                    </View>
                    <Swiper style={[styles.swiperContainer,{height:this.state.swiperHeight}]} showsButtons={false} loop={false} activeDot={activeDot} dot={dot} index={this.state.currentIndex}
                            paginationStyle={{flexDirection: 'row-reverse'}} onIndexChanged={this.changeIndex} ref={ref => this.swiper = ref}>
                        <View style={styles.slide}>
                            <View style={[styles.inputContainer,{marginTop:0}]}>
                                <Text style={styles.inputLabel}>نام</Text>
                                <TextInput underlineColorAndroid="rgba(0,0,0,0)" style={styles.inputMain} keyboardType="default"
                                           onChangeText={this.onNameChanged} />
                            </View>
                            <View style={[styles.inputContainer]}>
                                <Text style={styles.inputLabel}>نام خانوادگی</Text>
                                <TextInput underlineColorAndroid="rgba(0,0,0,0)" style={styles.inputMain} keyboardType="default"
                                           onChangeText={this.onFamilyChanged}/>
                            </View>
                            <View style={styles.inputContainer}>
                                <Text style={styles.inputLabel}>پست الکترونیک</Text>
                                <TextInput underlineColorAndroid="rgba(0,0,0,0)" style={styles.inputMain} keyboardType="email-address"
                                           onChangeText={this.onEmailChanged}/>
                            </View>
                            <View style={styles.inputContainer}>
                                <Text style={styles.inputLabel}>تلفن همراه (نام کاربری)</Text>
                                <TextInput underlineColorAndroid="rgba(0,0,0,0)" style={styles.inputMain} keyboardType="number-pad" maxLength={11}
                                           onChangeText={this.onPhoneNumberChanged}/>
                            </View>
                            <View style={styles.inputContainer}>
                                <Text style={styles.inputLabel}>رمز عبور</Text>
                                <TextInput underlineColorAndroid="rgba(0,0,0,0)" style={styles.inputMain}
                                           secureTextEntry={this.state.passOn}
                                           onChangeText={this.onPassChanged}/>
                                {this.state.passOn && <TouchableOpacity activeOpacity={.7} style={styles.eyeButton} onPress={() => this.setState({passOn:false})} hitSlop={{top: 10, left: 10, bottom: 10, right: 10}}>
                                    <Icon name='md-eye-off' style={styles.eyeButtonIcon}/>
                                </TouchableOpacity>}
                                {!this.state.passOn && <TouchableOpacity activeOpacity={.7} style={styles.eyeButton} onPress={() => this.setState({passOn:true})} hitSlop={{top: 10, left: 10, bottom: 10, right: 10}}>
                                    <Icon name='md-eye' style={styles.eyeButtonIcon}/>
                                </TouchableOpacity>}
                            </View>

                            <TouchableOpacity style={[styles.colorGrButton,{marginTop:15}]} activeOpacity={.8} onPress={this.registerRequest}>
                                <LinearGradient colors={[this.props.global.grColorOne,this.props.global.grColorTwo]} style={styles.grButtonGradient} start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}>
                                    <Text style={styles.grButtonText}>عضویت</Text>
                                </LinearGradient>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.slide}>
                            <View style={styles.inputContainer}>
                                <Text style={styles.inputLabel}>نام کاربری</Text>
                                <TextInput underlineColorAndroid="rgba(0,0,0,0)" style={styles.inputMain} keyboardType="default"
                                           onChangeText={this.onUserNameChanged}/>
                            </View>
                            <View style={styles.inputContainer}>
                                <Text style={styles.inputLabel}>رمز عبور</Text>
                                <TextInput underlineColorAndroid="rgba(0,0,0,0)" style={styles.inputMain}
                                           secureTextEntry={this.state.loginPassOn}
                                           onChangeText={this.onPasswordChanged}/>
                                {this.state.loginPassOn && <TouchableOpacity activeOpacity={.7} style={styles.eyeButton} onPress={() => this.setState({loginPassOn:false})} hitSlop={{top: 10, left: 10, bottom: 10, right: 10}}>
                                    <Icon name='md-eye-off' style={styles.eyeButtonIcon}/>
                                </TouchableOpacity>}
                                {!this.state.loginPassOn && <TouchableOpacity activeOpacity={.7} style={styles.eyeButton} onPress={() => this.setState({loginPassOn:true})} hitSlop={{top: 10, left: 10, bottom: 10, right: 10}}>
                                    <Icon name='md-eye' style={styles.eyeButtonIcon}/>
                                </TouchableOpacity>}
                            </View>

                            <TouchableOpacity style={[styles.colorGrButton,{marginTop:15}]} activeOpacity={.8} onPress={this.loginRequest}>
                                <LinearGradient colors={[this.props.global.grColorOne,this.props.global.grColorTwo]} style={styles.grButtonGradient} start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}>
                                    <Text style={styles.grButtonText}>ورود</Text>
                                </LinearGradient>
                            </TouchableOpacity>
                        </View>
                    </Swiper>
                    {this.state.currentIndex === 0 && <TouchableOpacity style={{marginBottom: 40}} activeOpacity={0.8} onPress={() => this.swiper.scrollBy(1,true)}>
                        <Text style={styles.buttonTransparentText}>حساب کاربری دارید؟ وارد شوید.</Text>
                    </TouchableOpacity>}

                    {this.state.currentIndex === 1 && <TouchableOpacity style={{marginBottom: 40}} activeOpacity={0.8} onPress={() => this.swiper.scrollBy(-1,true)}>
                        <Text style={styles.buttonTransparentText}>حساب کاربری ندارید؟ عضو شوید.</Text>
                    </TouchableOpacity>}
                </KeyboardAwareScrollView>

                <RequestModal showModal={this.state.showModal} setter={this} loading={this.state.loading} hasError={this.state.hasError} errorDesc={this.state.errorDesc}/>
            </View>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setUser : user => {
            dispatch(setUser(user))
        }
    }
};

const mapStateToProps =(state) =>  {
    return{
        user: state.user,
        global:state.global
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(Intro);