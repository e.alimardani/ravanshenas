import React from 'react';
import {ActivityIndicator, Text, View, StatusBar, WebView, Image} from 'react-native';

import { Container ,Header,Left,Body,Right} from 'native-base';
import {styles} from './Style';
import BackButton from "../Shared/BackButton";
import Helpers from "./Helper";

export default class Browser extends React.Component {

    constructor(props)
    {
        super(props);

        this.state = {
            url: '',
            loading: true
        };
    }

    async componentWillMount(){
        if(await Helpers.CheckNetInfo()){
            return;
        }
        this.setState({
            url: this.props.url,
        })
    }

    render() {


        return (
            <Container>
                <Header style={[styles.header,{overflow: 'hidden',zIndex:999}]} >
                <Image resizeMode='stretch' style={[styles.mainBackground,{height:1000}]} source={require("./../../assets/images/shared/menu-bg.png")} />
                    <Left style={{flex: .2,justifyContent:'center',alignItems:'flex-start'}}>
                        <BackButton/>
                    </Left>
                    <Body style={{flex: 1,justifyContent:'center',alignItems:'center'}}>
                        <Text style={[styles.notFoundText,{marginTop:0,color:'#fff'}]}>{this.props.title}</Text>
                    </Body>
                    <Right style={{flex: .2,justifyContent:'center',alignItems:'flex-end'}} >
                        {this.state.loading && <ActivityIndicator color="#ffffff"/>}
                    </Right>
                </Header>

                <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content" />

                <WebView
                    source={{uri: this.state.url , method : 'GET' }}
                    onLoadStart={() => { this.setState({loading:true}) } }
                    onLoadEnd={() => { this.setState({loading:false}) } }
                    style={{zIndex:-1}}
                />
            </Container>
        )
    }
}
