import React from 'react';
import { TouchableOpacity,View,Text,Platform } from 'react-native';
import { Actions } from 'react-native-router-flux';
import BasketIcon from "../../assets/icon/BasketIcon";
import {styles} from './Style';
import {connect} from "react-redux";
import Helpers from "./Helper";

class BasketButton extends React.Component {

    render() {
        return (
            <View style={{paddingHorizontal: 5,
                ...Platform.select({
                    ios: {
                        marginTop: -5
                    },
                    android:{
                        marginTop: -2
                    }
                }),
            }}>
                <TouchableOpacity onPress={() => Actions.push('cart')} activeOpacity={0.7} hitSlop={{top: 10, left: 10, bottom: 10, right: 10}}>
                    <BasketIcon/>
                    <View style={styles.basketButtonBadge}>
                        <Text style={styles.basketButtonBadgeText}>{Helpers.ToPersianNumber(this.props.app.cart.length.toString())}</Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
}

const mapStateToProps =(state) =>  {
    return{
        user: state.user,
        app:state.app
    }
};

export default connect(mapStateToProps,null)(BasketButton);
