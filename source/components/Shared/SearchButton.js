import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import { Icon } from 'native-base';
import {styles} from './Style';
import { Actions } from 'react-native-router-flux';

export default class SearchButton extends React.Component {

    render() {
        return (

            <View style={{padding:5,paddingHorizontal: 7}}>
                <TouchableOpacity activeOpacity={0.7} onPress={() => Actions.push('productSearch')} hitSlop={{top: 10, left: 10, bottom: 10, right: 10}}>
                    <Icon style={styles.backButton} name='ios-search'/>
                    {/*<Text style={styles.backButtonText}>بازگشت</Text>*/}
                </TouchableOpacity>
            </View>

        )
    }
}
