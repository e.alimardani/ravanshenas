import EStyleSheet from 'react-native-extended-stylesheet';
import {Platform, PixelRatio, Dimensions, StatusBar} from 'react-native';

export const styles = EStyleSheet.create({
    header : {
        backgroundColor : 'transparent',
        borderBottomWidth:0,
        elevation: 0,
        ...Platform.select({
            ios: {

            },
            android: {
                marginTop:StatusBar.currentHeight
            }
        }),
    },
    backButton:{
        color:'#ffffff',
        fontSize:30,
    },
    backButtonText:{
        textAlign:'right',
        writingDirection:'rtl',
        fontSize:12,
        color:'#ffffff',
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold'
            },
            android: {
                fontFamily: '$YekanBold'
            }
        }),
        width:50,
        marginTop:-20,
        backgroundColor:'transparent'
    },
    basketButtonBadge:{
        position: 'absolute',
        width:18,
        height:18,
        borderRadius:9,
        backgroundColor:'#ffb941',
        justifyContent:'center',
        alignItems:'center',
        bottom:0,
        right:-5,
        ...Platform.select({
            ios: {
                paddingTop: 2.5
            },
        }),
    },
    basketButtonBadgeText:{
        textAlign:'center',
        writingDirection:'rtl',
        fontSize:10,
        //lineHeight:12,
        color:'#ffffff',
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'normal'
            },
            android: {
                fontFamily: '$YekanRegular',
            }
        }),
    },
    headerRightText:{
        textAlign:'right',
        writingDirection:'rtl',
        fontSize:16,
        color:'#f5f4e8',
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold',
                //paddingTop:3
            },
            android: {
                fontFamily: '$YekanBold'
            }
        }),
        backgroundColor:'transparent'
    },
    mainContainer : {
        backgroundColor:'#fff',
        flex: 1
    },
    mainBackground:{
        position: 'absolute',
        width:Dimensions.get('window').width,
        height:Dimensions.get('window').height,
        //opacity:0.7,
    },
    drawerHeader:{
        backgroundColor:'transparent',
        paddingVertical: 56.5,
        paddingRight: 20
    },
    drawerHeaderText:{
        textAlign:'right',
        writingDirection:'rtl',
        fontSize:16,
        color:'#f5f4e8',
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold',
                //paddingTop:3
            },
            android: {
                fontFamily: '$YekanBold'
            }
        }),
        backgroundColor:'transparent'
    },
    drawerTitleRow:{
        flexDirection: 'row-reverse',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 15,
        paddingRight: 20,
        borderBottomWidth: 5/PixelRatio.get(),
        borderBottomColor:'#f4f7f9',
        marginLeft: 15
    },
    drawerTitleRowText:{
        textAlign:'right',
        writingDirection:'rtl',
        fontSize:14,
        color:'#393939',
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold',
                //paddingTop:3
            },
            android: {
                fontFamily: '$YekanBold'
            }
        }),
        backgroundColor:'transparent'
    },
    drawerTitleRowTextExpanded:{
        textAlign:'right',
        writingDirection:'rtl',
        fontSize:14,
        color:'$MainColor',
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold',
                //paddingTop:3
            },
            android: {
                fontFamily: '$YekanBold'
            }
        }),
        backgroundColor:'transparent'
    },
    drawerTitleRowExpandedIcon:{
        fontSize:22,
        color:'$MainColor'
    },
    drawerTitleRowUnexpandedIcon:{
        fontSize:22,
        color:'#dadadb'
    },
    drawerContent:{
        padding: 10,
        borderWidth: 5/PixelRatio.get(),
        borderColor:'#f4f7f9',
        //backgroundColor:'#cccccc',
        margin: 15,
        marginTop: 0,
        borderRadius: 15
    },
    drawerContentRow:{
        flexDirection: 'row-reverse',
        alignItems:'center',
        borderBottomWidth: 5/PixelRatio.get(),
        borderBottomColor:'#f4f7f9',
    },
    drawerContentIcon:{
        fontSize:18,
        color:'$MainColor'
    },
    drawerContentText:{
        textAlign:'right',
        writingDirection:'rtl',
        fontSize:12,
        color:'#393939',
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'normal',
                //paddingTop:3
            },
            android: {
                fontFamily: '$YekanRegular'
            }
        }),
        backgroundColor:'transparent',
        marginRight: 10,
        paddingVertical:10,
    },
    requestModalContainer:{
        width:'80%',
        minHeight:200,
        backgroundColor:'#ffffff',
        alignSelf:'center',
        borderRadius:5,
        justifyContent:'center',
        alignItems:'center',
        paddingVertical: 10
    },
    requestModalText:{
        textAlign:'center',
        writingDirection:'rtl',
        fontSize:12,
        color:'#333333',
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold',
                paddingTop:2
            },
            android: {
                fontFamily: '$YekanBold'
            }
        }),
        lineHeight:20,
        paddingHorizontal:10,
        marginTop:10
    },
    loadingModalContainer:{
        backgroundColor:'#ffffff',
        alignSelf:'center',
        borderRadius:5,
        justifyContent:'center',
        alignItems:'center',
        ...Platform.select({
            ios: {
                padding:10,
                paddingLeft:13,
                paddingTop:13
            },
            android: {
                padding:10
            }
        }),
    },
    notFoundText:{
        textAlign:'center',
        writingDirection:'rtl',
        fontSize:14,
        color:'#777777' ,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold',
                //paddingTop:3
            },
            android: {
                fontFamily: '$YekanBold',
            }
        }),
        marginTop:Dimensions.get('window').height * .1
    },
    notifModalContainer:{
        width:'90%',
        //height:'90%',
        backgroundColor:'#ffffff',
        alignSelf:'center',
        justifyContent:'center',
        alignItems:'center',
        borderRadius:5,
        paddingVertical:15,
        paddingHorizontal:10
    },
    modalColorGrButton:{
        alignSelf:'center',
        backgroundColor:'$ButtonBackgroundColor',
        width:null,
        height:30,
        justifyContent:'center',
        alignItems:'center',
        borderRadius:15,
        ...Platform.select({
            ios: {
                shadowColor: '$MainColor',
                shadowOffset: { width: 0, height: 0 },
                shadowOpacity: 0.6,
                shadowRadius: 7,
            },
            android: {
                elevation: 2
            }
        }),
    },
    modalGrButtonGradient:{
        width:null,
        height:30,
        borderRadius:15,
        paddingHorizontal:30,
        justifyContent:'center',
        alignItems:'center',
    },
    modalGrButtonText:{
        textAlign:'center',
        writingDirection:'rtl',
        fontSize:12,
        //lineHeight:14,
        color:'$MainFontColor',
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold'
            },
            android: {
                fontFamily: '$YekanBold',
            }
        }),
    },
    contactName:{
        textAlign:'center',
        writingDirection:'rtl',
        fontSize:14,
        marginTop:10,
        color:'#393939',
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold'
            },
            android: {
                fontFamily: '$YekanBold',
            }
        }),
    },
    contactValues:{
        textAlign:'center',
        writingDirection:'rtl',
        fontSize:12,
        color:'#393939',
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'normal'
            },
            android: {
                fontFamily: '$YekanRegular',
            }
        }),
    },
    textInput:{
        textAlign:'right',
        writingDirection:'rtl',
        fontSize:14,
        color:'#393939' ,
        paddingVertical:2,
        paddingHorizontal:10,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'normal',
                //paddingTop:10
            },
            android: {
                fontFamily: '$YekanRegular'
            }
        }),
        width: '100%',
        borderRadius:10,
        marginTop:10,
        height:50,
        backgroundColor:'#fff',
    },
});