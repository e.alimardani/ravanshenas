import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import { Icon } from 'native-base';
import {styles} from './Style';
import { Actions } from 'react-native-router-flux';

export default class HeaderRightLabel extends React.Component {

    render() {
        return (

            <View style={{padding:5}}>
                <Text style={styles.headerRightText} numberOfLines={1}>{this.props.title}</Text>
            </View>

        )
    }
}
