import React from 'react';
import {View, Text, Item, Icon, Content, Accordion, Left, Body, Right, Container} from 'native-base';
import {Image, TouchableOpacity, AsyncStorage, ActivityIndicator} from "react-native";
import { styles } from "./Style";
import { Actions } from 'react-native-router-flux';
import {setApp} from "../../redux/Actions/Index";
import {connect} from "react-redux";
import Helpers from "./Helper";

class MainDrawer extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            categories:[],
            loading:false
        };

        this.getCategoriesRequest = this.getCategoriesRequest.bind(this);
    }

    async componentWillMount(){
        if(this.props.app.categories.length === 0) {
            await this.getCategoriesRequest();
        }
    }

    async getCategoriesRequest(){
        if (await Helpers.CheckNetInfo()) {
            await this.setState({
                loading:false
            });
        }

        try {
            await this.setState({
                loading:true
            });

            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);

            let response = await fetch(this.props.global.baseApiUrl + '/product/getproductcats',
                {
                    method: "POST",
                    body: formData
                });

            //console.log(response);

            if (response.status !== 200) {
                await this.setState({
                    loading:false
                });
            }
            else {

                let json = await response.json();

                if(json.success){
                    await this.props.setApp({
                        ...this.props.app,
                        categories:json.data
                    });

                    await this.setState({
                        loading:false
                    });
                }
                else {
                    await this.setState({
                        loading:false
                    });
                }
            }
        } catch (error) {
            await this.setState({
                loading:false
            });
        }
    }

    renderHeader(item,expanded) {
        if(this.props.app.categories.filter(a => a.cat_parent === item.cat_id).length > 0){
            return (
                <View style={[styles.drawerTitleRow,{borderBottomColor : expanded ? '#ffffff' : '#f4f7f9'}]}>
                    <Text style={expanded ? styles.drawerTitleRowTextExpanded : styles.drawerTitleRowText}>
                        {item.cat_name}
                    </Text>
                    {expanded
                        ? <Icon style={styles.drawerTitleRowExpandedIcon} name="md-remove-circle" />
                        : <Icon style={styles.drawerTitleRowUnexpandedIcon} name="md-add-circle" />}
                </View>
            );
        }
        else {
            return (
                <TouchableOpacity style={[styles.drawerTitleRow,{borderBottomColor : expanded ? '#ffffff' : '#f4f7f9'}]}
                                  activeOpacity={.7} onPress={() => Actions.push('products',{title:item.cat_name,type:'category',categoryId:item.cat_id})}>
                    <Text style={expanded ? styles.drawerTitleRowTextExpanded : styles.drawerTitleRowText}>
                        {item.cat_name}
                    </Text>
                    {expanded
                        ? <Icon style={styles.drawerTitleRowExpandedIcon} name="md-remove-circle" />
                        : <Icon style={styles.drawerTitleRowUnexpandedIcon} name="md-add-circle" />}
                </TouchableOpacity>
            );
        }
    }
    renderContent(item) {

        return(
            <View style={styles.drawerContent}>
                <TouchableOpacity style={[styles.drawerContentRow,{borderBottomColor : '#ffffff' }]}
                                  activeOpacity={.7} onPress={() => Actions.push('products',{title:item.cat_name,type:'category',categoryId:item.cat_id})}>
                    <Icon name='ios-arrow-back' style={styles.drawerContentIcon} />
                    <Text style={styles.drawerContentText}>{item.cat_name}</Text>
                </TouchableOpacity>
                {this.props.app.categories.filter(a => a.cat_parent === item.cat_id).map((item2,index) => {
                    return(
                        <TouchableOpacity key={item2.cat_id} style={[styles.drawerContentRow,{borderBottomColor : index + 1 === this.props.app.categories.filter(a => a.cat_parent === item.cat_id).length  ? '#ffffff' : '#f4f7f9' }]}
                                          activeOpacity={.7} onPress={() => Actions.push('products',{title:item2.cat_name,type:'category',categoryId:item2.cat_id})}>
                            <Icon name='ios-arrow-back' style={styles.drawerContentIcon} />
                            <Text style={styles.drawerContentText}>{item2.cat_name}</Text>
                        </TouchableOpacity>
                    );
                })}
            </View>
        )

    }

    render() {

        return (
            <Container style={styles.mainContainer}>
                <Image resizeMode="cover" style={styles.mainBackground} source={require("./../../assets/images/shared/menu-bg.png")} />

                <View style={[styles.drawerHeader,{marginBottom:50}]}>
                    <Text style={styles.drawerHeaderText}>دسته بندی محصولات</Text>
                </View>
                {(!this.state.loading && this.props.app.categories.length > 0) && <View style={{flex:1,backgroundColor:'#fff'}}>
                    <Content style={{backgroundColor:'transparent'}}>
                        <Accordion
                            dataArray={this.props.app.categories.filter(a => a.cat_parent === 0)}
                            renderHeader={this.renderHeader.bind(this)}
                            renderContent={this.renderContent.bind(this)}
                            style={{borderWidth:0}}
                        />
                    </Content>
                </View>}

                {this.state.loading && <View style={{flex:1,backgroundColor:'#fff',justifyContent: 'center',alignItems: 'center'}}>
                    <ActivityIndicator color={this.props.global.grColorTwo} size='large'/>
                </View>}

                {(!this.state.loading && this.props.app.categories.length === 0) && <View style={{flex:1,backgroundColor:'#fff',justifyContent: 'center',alignItems: 'center'}}>
                    <Text style={styles.drawerTitleRowText}>دسته بندی وجود ندارد!</Text>
                </View>}

            </Container>
        )
    }
}

const mapStateToProps =(state) =>  {
    return{
        user: state.user,
        global:state.global,
        app:state.app
    }
};

const mapDispatchToProps = dispatch => {
    return {
        setApp : app => {
            dispatch(setApp(app))
        }
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(MainDrawer);