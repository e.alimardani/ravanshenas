// import React, { Component } from "react";
//
// import {Platform, TouchableOpacity, View,Text} from 'react-native';
//
// // import FCM, {FCMEvent, RemoteNotificationResult, WillPresentNotificationResult, NotificationType} from "react-native-fcm";
// import {connect} from "react-redux";
// import {styles} from './Style';
// import {Icon} from "native-base";
// import Modal from "react-native-modal";
// import LinearGradient from "react-native-linear-gradient";
// import Helpers from "./Helper";
//
// const buttonColors = [
//     '#DA4453',
//     '#DA4453',
//     '#ad1456',
// ];
//
// class PushNotificationController extends Component {
//     constructor(props) {
//         super(props);
//
//         this.state = {
//             showModal: false,
//             modalText: '',
//             modalTitle:'',
//             lastNotif:null,
//             initNotif:false,
//             hasRating:false,
//             ratingOrderId:0
//         };
//
//         //console.log(props);
//         this.registerToken = this.registerToken.bind(this);
//         this.showNotif = this.showNotif.bind(this);
//     }
//
//     async registerToken(token)
//     {
//         try {
//             let formData = new FormData();
//             formData.append("admintoken", this.props.global.adminToken);
//             formData.append("reg_id", token);
//             formData.append("d_type", Platform.OS === 'ios' ? 0 : 1);
//
//             if(this.props.user.apiToken !== null){
//                 formData.append("usertoken", this.props.user.apiToken);
//             }
//
//             let response = await fetch(this.props.global.baseApiUrl + '/notification/adddevice',
//                 {
//                     method: "POST",
//                     body: formData
//                 });
//
//             //console.log(Platform.OS);
//
//         } catch (error) {
//             console.log(error);
//         }
//     }
//
//     async componentDidMount() {
//         await FCM.requestPermissions();
//
//         FCM.setBadgeNumber(0);
//
//         FCM.createNotificationChannel({
//             id: 'default',
//             name: 'Default',
//             priority: 'high'
//         });
//
//         FCM.getFCMToken().then(async token => {
//             //console.log(token);
//             await this.registerToken(token);
//         });
//
//         this.refreshTokenListener = FCM.on(FCMEvent.RefreshToken,async token => {
//             await this.registerToken(token);
//         });
//
//         FCM.getInitialNotification().then(async notif => {
//             FCM.setBadgeNumber(0);
//
//             if(Platform.OS === "android")
//             {
//                 if(notif.body) {
//                     await this.showNotif(notif);
//                 }
//             }
//             else {
//                 if(notif.aps) {
//                     await this.showNotif(notif);
//                 }
//             }
//         });
//
//         this.notificationListner = FCM.on(FCMEvent.Notification, async notif => {
//
//             FCM.setBadgeNumber(0);
//
//             if(Platform.OS === 'android')
//             {
//                 if(notif.body){
//                     await this.showNotif(notif);
//                 }
//             }
//
//             if(Platform.OS ==='ios'){
//
//                 switch(notif._notificationType){
//                     case NotificationType.Remote:
//                         notif.finish(RemoteNotificationResult.NewData); //other types available: RemoteNotificationResult.NewData, RemoteNotificationResult.ResultFailed
//                         break;
//                     case NotificationType.NotificationResponse:
//                         notif.finish();
//                         break;
//                     case NotificationType.WillPresent:
//                         notif.finish(WillPresentNotificationResult.All); //other types available: WillPresentNotificationResult.None
//                         break;
//                 }
//
//                 if(notif.opened_from_tray) {
//                    if(notif.aps) {
//                         await this.showNotif(notif);
//                     }
//                 }
//             }
//             //this.showLocalNotification(notif);
//         });
//     }
//
//     async showNotif(notif){
//         if(Platform.OS === "android")
//         {
//             await this.setState({
//                 showModal: true,
//                 modalText: notif.body,
//                 modalTitle:notif.title
//             });
//         }
//         else {
//             await this.setState({
//                 showModal: true,
//                 modalText: notif.aps.alert.body,
//                 modalTitle:notif.aps.alert.title
//             });
//         }
//     }
//
//     render() {
//         return(
//             <Modal isVisible={this.state.showModal} backdropOpacity={0.5} animationIn="fadeIn" animationOut="fadeOut" backdropColor='#000000'
//                    onBackdropPress={() => this.setState({showModal: false})} onBackButtonPress={() => this.setState({showModal: false})}>
//                 <View style={styles.notifModalContainer} >
//                     <TouchableOpacity onPress={async () => await this.setState({showModal: false,hasRating:false,lastNotif:null})} style={{position: 'absolute', top: 3, left: 5, zIndex: 999999}}>
//                         <Icon name="ios-close-circle" style={{color: '#555555', fontSize: 20}}/>
//                     </TouchableOpacity>
//
//                     <Text style={[styles.requestModalText,{fontSize:12,color:this.props.global.grColorOne}]}>
//                         {this.state.modalTitle}
//                     </Text>
//
//                     <Text style={[styles.requestModalText,{fontSize:10}]}>
//                         {this.state.modalText}
//                     </Text>
//
//                     <TouchableOpacity style={[styles.modalColorGrButton,{marginTop:15}]} activeOpacity={.8} onPress={() => {this.setState({showModal:false})} }>
//                         <LinearGradient colors={[this.props.global.grColorOne,this.props.global.grColorTwo]} style={styles.modalGrButtonGradient} start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}>
//                             <Text style={styles.modalGrButtonText}>بستن</Text>
//                         </LinearGradient>
//                     </TouchableOpacity>
//
//                 </View>
//             </Modal>
//         );
//     }
// }
//
// const mapStateToProps =(state) =>  {
//     return{
//         user: state.user,
//         global:state.global
//     }
// };
//
// export default connect(mapStateToProps,null)(PushNotificationController);