import React from 'react';
import {Image, StatusBar, View} from 'react-native';
import Pdf from 'react-native-pdf';
import {Body, Container, Header, Left, Right} from "native-base";
import {styles} from './Style';
import BackButton from "../Shared/BackButton";
import HeaderRightLabel from "../Shared/HeaderRightLabel";

export default class PdfViewer extends React.Component {
    render() {
        const source = {uri:this.props.source,cache:false};

        return (
            <Container>
                <Header style={[styles.header,{overflow: 'hidden',zIndex:9999}]} >
                    <Image resizeMode="cover" style={styles.mainBackground} source={require("./../../assets/images/home/headerImage.png")} />
                    <Left style={{flex: 0.5}}>
                        <BackButton/>
                    </Left>
                    <Body style={{flex: 0.2, justifyContent: 'center', alignItems: 'center'}}/>
                    <Right style={{flex: 1}}>
                        <HeaderRightLabel title={this.props.title}/>
                    </Right>
                </Header>
                <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content" />
                <View style={{backgroundColor:'#ffffff',flex:1,zIndex:-1}}>
                    <Pdf
                        source={source}
                        style={{flex:1, width:'100%'}}/>
                </View>

            </Container>
        )
    }
}
