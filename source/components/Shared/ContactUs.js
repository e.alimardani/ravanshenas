import React from 'react';
import {
    ActivityIndicator,
    Text,
    View,
    StatusBar,
    Image,
    TouchableOpacity,
    Linking,
    PixelRatio,
    TextInput
} from 'react-native';

import {Container, Header, Left, Body, Right, Thumbnail,Icon} from 'native-base';
import {styles} from './Style';
import BackButton from "../Shared/BackButton";
import Helpers from "./Helper";
import {setUser} from "../../redux/Actions/Index";
import {connect} from "react-redux";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";

class ContactUs extends React.Component {

    constructor(props)
    {
        super(props);

        this.state = {
            info:null,
            loading: true,
            body:'',
            subject:'',
            phone:'',
            Mail:''
        };

        this.getContactRequest = this.getContactRequest.bind(this);
        this.onSubjectChanged = this.onSubjectChanged.bind(this);
        this.onBodyChanged = this.onBodyChanged.bind(this);
        this.onMailChanged = this.onMailChanged.bind(this);
        this.onPhoneChanged = this.onPhoneChanged.bind(this);

    }
    onSubjectChanged(text) {
        this.setState({
            subject:text
        });
    }
    onMailChanged(text) {
        this.setState({
            Mail:text
        });
    }
    onPhoneChanged(text) {
        this.setState({
            phone:text
        });
    }
    onBodyChanged(text) {
        this.setState({
            body:text
        });
    }

    async componentDidMount(){
        await this.getContactRequest();
    }

    async getContactRequest(){
        if (await Helpers.CheckNetInfo()) {
            await this.setState({
                loading:false
            });
        }

        try {

            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);

            let response = await fetch(this.props.global.baseApiUrl + '/user/contact_us_data',
                {
                    method: "POST",
                    body: formData
                });

            //console.log(response);

            if (response.status !== 200) {
                await this.setState({
                    loading:false
                });
            }
            else {

                let json = await response.json();

                console.log(json);

                if(json.success){

                    await this.setState({
                        info:json.data,
                        loading:false
                    });
                }
                else {
                    await this.setState({
                        loading:false
                    });
                }
            }
        } catch (error) {
            await this.setState({
                loading:false
            });
        }
    }

    render() {


        return (
            <Container>
                <Image resizeMode='stretch' style={[styles.mainBackground,{height:500}]} source={require("./../../assets/images/shared/menu-bg.png")} />
                <Header style={[styles.header]} >

                    <Left style={{flex: .2,justifyContent:'center',alignItems:'flex-start'}}>
                        <BackButton/>
                    </Left>
                    <Body style={{flex: 1,justifyContent:'center',alignItems:'center'}}>
                        <Text style={[styles.notFoundText,{marginTop:0,color:'#fff'}]}>تماس با ما</Text>
                    </Body>
                    <Right style={{flex: .2,justifyContent:'center',alignItems:'flex-end'}} >

                    </Right>
                </Header>

                <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content" />

                {!this.state.loading && this.state.info !== null && <KeyboardAwareScrollView style={{flex:1,backgroundColor:'#fafbff'}} keyboardShouldPersistTaps={'handled'}>
                    <View style={{width:'100%',justifyContent:'center',alignItems:'center',paddingVertical:20}}>
                        <Thumbnail source={{uri : this.state.info.profile_Image}} large />
                        <Text style={styles.contactName}>{this.state.info.name}</Text>
                    </View>
                    <View style={{width:'100%',flexDirection:'row',justifyContent:'flex-end',paddingHorizontal:10,alignItems:'center',paddingVertical:5}}>
                        <Text style={[styles.contactValues,{width:'90%',textAlign: 'right',marginRight:20}]}>{this.state.info.address}</Text>
                        <Icon name='md-pin' style={{fontSize:20}} />
                    </View>
                    <View style={{width:'100%',flexDirection:'row',justifyContent:'flex-end',paddingHorizontal:10,alignItems:'center',paddingVertical:5}}>
                        <Text style={[styles.contactValues,{width:'40%',textAlign: 'left',marginRight:20}]}>{Helpers.ToPersianNumber(this.state.info.workingTime)}</Text>
                        <Text style={[styles.contactValues,{width:'40%',textAlign: 'right',marginRight:20}]}>{Helpers.ToPersianNumber(this.state.info.phone)}</Text>
                        <Icon name='md-call' style={{fontSize:20}} />
                    </View>
                    <View style={{width:'100%',flexDirection:'row',justifyContent:'space-between',paddingHorizontal:20,alignItems:'center',paddingVertical:10}}>
                        <TouchableOpacity activeOpacity={.8} style={{flexDirection:'row',backgroundColor:'#fff',padding:10,borderRadius:10}} onPress={() => Linking.openURL(this.state.info.telegram)}>
                            <Image style={{width:20,height:20,resizeMode:'contain',marginRight:10}} source={require('./../../assets/images/shared/telegram.png')} />
                            <Text style={[styles.contactValues,{fontSize:12}]}>صفحه تلگرام ما</Text>
                        </TouchableOpacity>
                        <TouchableOpacity activeOpacity={.8} style={{flexDirection:'row',backgroundColor:'#fff',padding:10,borderRadius:10}} onPress={() => Linking.openURL(this.state.info.instegram)}>
                            <Image style={{width:20,height:20,resizeMode:'contain',marginRight:10}} source={require('./../../assets/images/shared/instagram.png')} />
                            <Text style={[styles.contactValues,{fontSize:12}]}>صفحه اینستاگرام ما</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{width:'100%',flexDirection:'row',justifyContent:'space-between',paddingHorizontal:20,alignItems:'center',paddingVertical:10}}>
                        <TouchableOpacity activeOpacity={.8} style={{flexDirection:'row',backgroundColor:'#fff',borderRadius:10,overflow:'hidden',width:'100%',height:150,borderWidth:5/PixelRatio.get(),borderColor:'#efefef'}} onPress={() => Linking.openURL(`geo:${this.state.info.lat_ip},${this.state.info.long_ip}`)}>
                            <Image style={{width:'100%',height:'100%',resizeMode:'cover'}} source={{uri:this.state.info.map_Image}} />
                        </TouchableOpacity>
                    </View>

                    <View style={{width:'100%',justifyContent:'center',paddingHorizontal:20,alignItems:'flex-end',paddingVertical:10}}>
                        <Text style={[styles.contactName,{fontSize:12,textAlign:'right'}]}>درخواست خود را برای ما ارسال کنید!</Text>

                        <TextInput underlineColorAndroid="rgba(0,0,0,0)" style={styles.textInput}
                                   placeholderTextColor="#cccccc" placeholder='عنوان پیام' value={this.state.subject}
                                   onChangeText={this.onSubjectChanged}/>
                        
                        <TextInput underlineColorAndroid="rgba(0,0,0,0)" style={styles.textInput}
                                   placeholderTextColor="#cccccc" placeholder='پست الکترونیکی' value={this.state.Mail}
                                   onChangeText={this.onMailChanged}/>
                        
                        <TextInput underlineColorAndroid="rgba(0,0,0,0)" style={styles.textInput}
                                   maxLength={11}
                                   placeholderTextColor="#cccccc" placeholder='شماره تماس' value={this.state.phone}
                                   onChangeText={this.onPhoneChanged}/>

                        <TextInput underlineColorAndroid="rgba(0,0,0,0)" style={[styles.textInput,{height:150}]}
                                   placeholderTextColor="#cccccc" placeholder='متن پیام' value={this.state.body} multiline={true}
                                   onChangeText={this.onBodyChanged}/>

                        <TouchableOpacity activeOpacity={.8} style={{justifyContent:'center',alignItems:'center',marginTop:10,backgroundColor:'#27ae60',borderRadius:10,overflow:'hidden',width:'100%',height:50}}
                                          onPress={() => Linking.openURL(`mailto:${this.state.info.email}?subject=${this.state.subject}&body=${this.state.body +'\n'+'\n'+ this.state.Mail +'\n'+ this.state.phone}`)}>
                            <Text style={[styles.contactValues,{fontSize:12,color:'#fff'}]}>ارسال پیام</Text>
                        </TouchableOpacity>
                    </View>
                </KeyboardAwareScrollView>}

                {this.state.loading && <View style={{flex:1,justifyContent:'center',alignItems:'center',backgroundColor:'#fafbff'}}>
                    <ActivityIndicator color="#0082f0" size='large'/>
                </View>}

            </Container>
        )
    }
}


const mapDispatchToProps = dispatch => {
    return {
        setUser : user => {
            dispatch(setUser(user))
        }
    }
};

const mapStateToProps =(state) =>  {
    return{
        user: state.user,
        global:state.global
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(ContactUs);
