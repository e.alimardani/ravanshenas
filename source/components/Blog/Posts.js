import React from 'react';
import {StatusBar, Image, ActivityIndicator, Text, TouchableOpacity, View, FlatList, PixelRatio} from 'react-native';
import {Header, Icon, Left, Right, Content, Container, Body} from 'native-base';
import styles from './Style';
import { Actions } from 'react-native-router-flux';
import {setApp, setUser} from "../../redux/Actions/Index";
import {connect} from "react-redux";
import HeaderRightLabel from "../Shared/HeaderRightLabel";
import BackButton from "../Shared/BackButton";
import Helpers from "../Shared/Helper";
import moment from 'jalali-moment';

class Posts extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            posts:[],
            page:1,
            hasMore:true,
            loading:true,
            refreshing:false,
            showLoadingModal:false
        };

        this.newDate = props.global.newDate;

        this.getPostsRequest = this.getPostsRequest.bind(this);

    }

    async componentWillMount(){
        await this.getPostsRequest();
    }

    async getPostsRequest(){
        //console.log(this.state);
        if(await Helpers.CheckNetInfo()){
            return;
        }

        try {
            let page = this.state.page;

            let url = this.props.type === 'category' ? '/post/getpostbycat' : '/post/all';

            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("perpage", '10');
            formData.append("offset", page);

            if(this.props.type === 'category'){
                formData.append("postcatid", this.props.categoryId);
            }
            let response = await fetch(this.props.global.baseApiUrl + url,
                {
                    method: "POST",
                    body: formData
                });

            if (response.status !== 200) {
                await this.setState({
                    loading:false,
                    refreshing:false,
                    hasMore:false
                });
            }
            else {

                let json = await response.json();

                if(json.success){

                    await this.setState(prevState => {
                        return {
                            posts: page === 1 ? json.data : [...prevState.posts, ...json.data],
                            loading: false,
                            refreshing:false,
                        }
                    });

                }
                else {
                    await this.setState({
                        loading:false,
                        hasMore:false,
                        refreshing:false
                    });
                }
            }

        } catch (error) {
            console.log(error);
            await this.setState({
                loading: false,
                refreshing:false,
                hasMore:false
            })
        }
    }

    renderItem({ item,index }) {
        return (
            <TouchableOpacity style={[styles.singlePost,{marginTop:index === 0 ? 10 : 10,marginBottom:index + 1 === this.state.posts.length ? 10 : 0}]} key={item.id} activeOpacity={.8}
                              onPress={() => Actions.push('post' , {title:item.title,url:item.permalink,post:item})}>
                <View style={styles.singlePostTop}>
                    <Image style={styles.singlePostImage} source={{uri: item.image + '?ref =' + this.newDate}}/>
                    <View style={styles.singlePostCategory}>
                        <Text style={styles.singlePostCategoryText}>{item.categories[0].category_name}</Text>
                    </View>
                </View>
                <View style={styles.singlePostBottom}>
                    <Text numberOfLines={1} style={styles.singlePostTitle}>{item.title}</Text>
                    <Text numberOfLines={1} style={styles.singlePostDate}>{Helpers.ToPersianNumber(moment(item.create_date, 'YYYY-MM-DD HH:mm').locale('fa').format('DD MMMM YYYY | HH:mm'))}</Text>
                </View>
            </TouchableOpacity>
        );
    }
    handleEmpty() {
        if(this.state.loading)
        {
            return(
                <ActivityIndicator style={styles.listLoadingIndicator} color={this.props.global.grColorTwo} size='large' />
            );
        }
        else
        {
            return(
                <Text style={styles.notFoundText}>مطلبی یافت نشد.</Text>
            );
        }
    }
    renderFooter() {
        if(this.state.loading && this.state.posts.length > 0)
        {
            return <ActivityIndicator style={{alignSelf: 'center',marginBottom: 20}} color={this.props.global.grColorTwo} />
        }
        else {
            return null;
        }
    }
    async handleRefresh() {
        if(await Helpers.CheckNetInfo()){
            return;
        }

        await this.setState({ page : 1 , refreshing : true ,hasMore:true} ,async () => {
            await this.getPostsRequest();
        });
    }
    async handleLoadMore() {
        if(this.state.hasMore) {
            await this.setState({page : this.state.page + 1 , loading : true}, async () => {
                await this.getPostsRequest()
            })
        }
    }

    render() {

        return (
            <Container style={styles.mainContainer}>
                <Image resizeMode='stretch' style={[styles.mainBackground,{height:2000}]} source={require("./../../assets/images/shared/menu-bg.png")} />
                <Header style={styles.header}>
                    <Left style={{flex:1,flexDirection:'row',alignItems:'center',justifyContent:'flex-start'}}>
                        <BackButton/>
                    </Left>
                    <Right style={{flex:2,alignItems:'center',justifyContent:'flex-end'}}>
                        <HeaderRightLabel title={this.props.title}/>
                    </Right>
                </Header>

                <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content" />

                <View style={{backgroundColor:'#ffffff',flex:1}}>
                    <FlatList
                        data={this.state.posts}
                        renderItem={this.renderItem.bind(this)}
                        ListEmptyComponent={this.handleEmpty.bind(this)}
                        ListFooterComponent={this.renderFooter.bind(this)}
                        keyExtractor={(item) => item.id.toString()}
                        style={{backgroundColor:'transparent'}}
                        showsVerticalScrollIndicator={false}
                        refreshing={this.state.refreshing}
                        onRefresh={this.handleRefresh.bind(this)}
                        onEndReached={this.handleLoadMore.bind(this)}
                        onEndReachedThreshold={2}
                        removeClippedSubviews={true}
                    />
                </View>

            </Container>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setApp : app => {
            dispatch(setApp(app))
        }
    }
};

const mapStateToProps =(state) =>  {
    return{
        user: state.user,
        global:state.global,
        app:state.app
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(Posts);