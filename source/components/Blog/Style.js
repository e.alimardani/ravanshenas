import EStyleSheet from 'react-native-extended-stylesheet';
import {Platform,Dimensions,PixelRatio,StatusBar} from "react-native";
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
export const styles = EStyleSheet.create({
    mainContainer : {
        backgroundColor:'$MainColor'
    },
    mainBackground:{
        position: 'absolute',
        width:Dimensions.get('window').width,
        height:Dimensions.get('window').height,
        //opacity:0.7
    },
    header : {
        backgroundColor : 'transparent',
        borderBottomWidth:0,
        elevation: 0,
        ...Platform.select({
            ios: {

            },
            android: {
                marginTop:StatusBar.currentHeight
            }
        }),
    },
    productDescInfoTitle:{
        textAlign:'right',
        writingDirection:'rtl',
        fontSize:16,
        //lineHeight:14,
        color:'#393939',
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold'
            },
            android: {
                fontFamily: '$YekanBold',
            }
        })
    },
    textInputComment:{
        flex:1,
        textAlign:'right',
        writingDirection:'rtl',
        fontSize:14,
        marginLeft:5,
        color:'#393939' ,
        paddingVertical:2,
        paddingHorizontal:10,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'normal',
                //paddingTop:10
            },
            android: {
                fontFamily: '$YekanRegular'
            }
        }),
        borderRadius:25,
        height:50,
        borderWidth:1,
        backgroundColor:'#fff',
    },
    singlePost:{
        backgroundColor:'#ffffff',
        margin:10,
        marginTop:0,
        borderRadius:7,
        borderWidth:5/PixelRatio.get(),
        borderColor:'#eceff1',
        width:Dimensions.get('window').width - 20,
    },
    loadingModalContainer:{
        backgroundColor:'#ffffff',
        alignSelf:'center',
        borderRadius:5,
        justifyContent:'center',
        alignItems:'center',
        ...Platform.select({
            ios: {
                padding:10,
                paddingLeft:13,
                paddingTop:13
            },
            android: {
                padding:10
            }
        }),
    },
    modalGrButtonGradient:{
        width:null,
        height:30,
        borderRadius:15,
        paddingHorizontal:30,
        justifyContent:'center',
        alignItems:'center',
    },
    modalGrButtonText:{
        textAlign:'center',
        writingDirection:'rtl',
        fontSize:12,
        //lineHeight:14,
        color:'$MainFontColor',
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold'
            },
            android: {
                fontFamily: '$YekanBold',
            }
        }),
    },
    productDescInfoContainer:{
        borderWidth: 5/PixelRatio.get(),
        borderColor:'#f1f4f7',
        marginHorizontal: 10,
        marginTop:30,
        borderRadius:20,
        paddingVertical:20,
        paddingHorizontal:10,
        // ...Platform.select({
        //     ios: {
        //         shadowColor: '#000000',
        //         shadowOffset: { width: 0, height: 0 },
        //         shadowOpacity: 0.15,
        //         shadowRadius: 10,
        //     },
        //     android: {
        //         elevation: 1
        //     }
        // }),
        backgroundColor:'#ffffff',
        width:Dimensions.get('window').width - 20
    },
    singlePostTop:{
        flex: 1,
        backgroundColor:'#ffffff',
        borderTopRightRadius:5,
        borderTopLeftRadius: 5,
        width:'100%',
        height:Dimensions.get('window').height * .2,
        overflow: 'hidden'
    },
    singlePostImage:{
        width:'100%',
        height:'100%',
        resizeMode:'cover',
        borderTopRightRadius: 5,
        borderTopLeftRadius: 5,
    },
    singlePostCategory:{
        backgroundColor:'#333333',
        position:'absolute',
        bottom:0,
        right:15,
        borderTopRightRadius:7,
        borderTopLeftRadius: 7,
        paddingVertical:2,
        paddingHorizontal:13
    },
    singlePostCategoryText:{
        textAlign:'center',
        writingDirection:'rtl',
        fontSize:12,
        color:'#ffffff' ,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'normal',
            },
            android: {
                fontFamily: '$YekanRegular',
            }
        }),
    },
    singlePostBottom:{
        paddingHorizontal:10,
        paddingVertical:10
    },
    singlePostTitle:{
        textAlign:'right',
        writingDirection:'rtl',
        fontSize:14,
        color:'#393939' ,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold',
            },
            android: {
                fontFamily: '$YekanBold',
            }
        }),
    },
    singlePostDate:{
        textAlign:'right',
        writingDirection:'rtl',
        fontSize:10,
        color:'#cccccc' ,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'normal',
            },
            android: {
                fontFamily: '$YekanRegular',
            }
        }),
        marginTop:5
    },
    listLoadingIndicator:{
        marginTop:Dimensions.get('window').height * .1
    },
    notFoundText:{
        textAlign:'center',
        writingDirection:'rtl',
        fontSize:14,
        color:'#777777' ,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold',
                //paddingTop:3
            },
            android: {
                fontFamily: '$YekanBold',
            }
        }),
        marginTop:Dimensions.get('window').height * .1
    },
    browserHeader : {
        backgroundColor : '$MainColor',
        borderBottomWidth:0,
        padding:0,
        elevation: 0,
        ...Platform.select({
            ios: {

            },
            android: {
                //marginTop:StatusBar.currentHeight,
                paddingTop:StatusBar.currentHeight,
                height:56 + StatusBar.currentHeight
            }
        }),
    },
    browserMainBackground:{
        position: 'absolute',
        width:Dimensions.get('window').width,
        height:Dimensions.get('window').height,
        //opacity:0.7,
    },
    singleVideo:{
        backgroundColor:'#17171c',
        margin:10,
        marginTop:0,
        borderRadius:7,
        width:Dimensions.get('window').width - 20,
        height:Dimensions.get('window').height * .3,
        overflow: 'hidden'
    },
    singleVideoImage:{
        width:'100%',
        height:'100%',
        resizeMode:'contain',
        borderRadius:7,
    },
    viewContainerProgress:{
        backgroundColor: 'rgba(0,0,0,0.5)',
        height: 48,
        left: 0,
        bottom: 0,
        right: 0,
        position: 'absolute',
        paddingHorizontal: 10,
        justifyContent:'center',
        alignItems:'center'
    },
    singleVideoTop1:{
        flex:1,
        position:'absolute',top:width*.15,
        backgroundColor:'transparent',
        borderTopRightRadius:5,
        borderTopLeftRadius: 5,
        justifyContent:'center',
        alignItems:'center'
    },
    singleVideoTop:{
        flex:1,
        backgroundColor:'transparent',
        borderTopRightRadius:5,
        borderTopLeftRadius: 5,
        justifyContent:'center',
        alignItems:'center'
    },
    singleVideoPlay:{
        width:60,
        height:60,
        backgroundColor:'$MainColorSingleVideoPlay',
        borderRadius:30,
        justifyContent:'center',
        alignItems:'center',
        opacity: .9
    },
    singleVideoPlayIcon:{
        fontSize:40,
        color:'#ffffff',
        marginLeft:5
    },
    singleVideoBottom:{
        width:'100%',
        position:'absolute',
        bottom:0
    },
    grButtonGradient:{
        width:'100%',
        height:'100%',
        paddingHorizontal:10,
        paddingVertical:15,
        justifyContent:'center'
    },
    singleVideoTitle:{
        textAlign:'right',
        writingDirection:'rtl',
        fontSize:14,
        color:'#ffffff' ,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold',
            },
            android: {
                fontFamily: '$YekanBold',
            }
        }),
    },
    videoTitleContainer:{
        paddingHorizontal:10,
        paddingVertical:20
    },
    videoTitleText:{
        textAlign:'right',
        writingDirection:'rtl',
        fontSize:16,
        color:'#ffffff' ,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold',
            },
            android: {
                fontFamily: '$YekanBold',
            }
        }),
    },
    videoSectionContainer:{
        borderTopWidth: 5/PixelRatio.get(),
        borderTopColor:'#858585',
        borderBottomWidth: 5/PixelRatio.get(),
        borderBottomColor:'#858585',
        backgroundColor:'#fff',
        paddingHorizontal:10,
        paddingVertical:15
    },
    videoSectionText:{
        textAlign:'right',
        writingDirection:'rtl',
        fontSize:14,
        color:'#888888' ,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'normal',
            },
            android: {
                fontFamily: '$YekanRegular',
            }
        }),
    },
    videoDescContainer:{
        padding:10
    },
    videoDescText:{
        textAlign:'right',
        writingDirection:'rtl',
        fontSize:12,
        color:'#dddddd' ,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'normal',
            },
            android: {
                fontFamily: '$YekanRegular',
            }
        }),
    },
    fileModal:{
        justifyContent:'center',
        alignItems:'center'
    },
    fileModalContainer:{
        width:'85%',
        height:180,
        backgroundColor:'#ffffff',
        borderRadius:10,
        //paddingVertical:7,
        //paddingHorizontal:7,
        justifyContent:'center',
        alignItems:'center'
    },
    fileModalRow:{
        flex:1,
        flexDirection:'row',
        width:'100%',
        justifyContent:'center',
        alignItems:'center',
        paddingHorizontal:0,
        paddingVertical:20
    },
    fileModalColumn:{
        flex:1,
        flexDirection:'column',
        borderLeftWidth:1/PixelRatio.get(),
        justifyContent:'center',
        alignItems:'center',
        height:'100%'
    },
    fileModalText:{
        textAlign:'center',
        writingDirection:'rtl',
        fontSize:12,
        color:'#555555',
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold'
            },
            android: {
                fontFamily: '$YekanBold'
            }
        }),
        marginTop:5
    },
    fileModalIcon:{
        color:'$MainColor',
        fontSize:50
    },
    renderedImage:{
        alignSelf:'center',
        marginVertical: 10,
        height:Dimensions.get('window').width / 2,
        maxWidth:Dimensions.get('window').width - 40,
        maxHeight:Dimensions.get('window').width / 2,
        minWidth:Dimensions.get('window').width - 40,
        resizeMode:'contain'
    }
});


export default styles;
