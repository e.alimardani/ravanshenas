import React from 'react';
import {
    StatusBar,
    Image,
    ActivityIndicator,
    Text,
    TouchableOpacity,
    View,
    FlatList,
    TouchableWithoutFeedback,
    Dimensions,
    ImageBackground,
    AsyncStorage, Linking, Platform
} from 'react-native';
import {Header, Icon, Left, Right, Content, Container, Body} from 'native-base';
import styles from './Style';
import {Actions} from 'react-native-router-flux';
import {setApp, setUser} from "../../redux/Actions/Index";
import {connect} from "react-redux";
import HeaderRightLabel from "../Shared/HeaderRightLabel";
import BackButton from "../Shared/BackButton";
import Helpers from "../Shared/Helper";
import LinearGradient from "react-native-linear-gradient";
import Modal from "react-native-modal";
import * as Progress from 'react-native-progress';
import RNFetchBlob from 'rn-fetch-blob';
import PlayVideo from 'react-native-video';

// import ProgressBar from "./ProgressBar";

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

class Video extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            videos: [],
            loading: true,
            //Files
            showFileModal: false,
            source: '',
            fileName: '',
            //posterSource:props.product.image !== null ? props.product.image : '',
            hasLocale: false,
            downloadPercent: 0,
            downloading: false,
            totalSize: 0,
            downloadedSize: 0,
            canceled: false,
            // state Video Play
            repeat: false,
            rate: 1,
            volume: 1,
            muted: false,
            duration: 0.0,
            currentTime: 0.0,
            paused: true,
            rateText: '1.0',
            pausedText: 'play',
            hideControls: false
        };
        this.video = props.video;

        this.newDate = props.global.newDate;

        this.getVideosRequest = this.getVideosRequest.bind(this);

        //Files
        this.showFile = this.showFile.bind(this);
        this.view = this.view.bind(this);
        this.downloadFile = this.downloadFile.bind(this);
        this.cancelDownload = this.cancelDownload.bind(this);
        this.deleteFile = this.deleteFile.bind(this);
        this.onLoadVideo = this.onLoadVideo.bind(this);
        this.onProgressVideo = this.onProgressVideo.bind(this);
        this.onEndVideo = this.onEndVideo.bind(this);
        this.handleProgressPress = this.handleProgressPress.bind(this);

    }

    async componentWillMount() {
        await this.getVideosRequest();
    }

    async getVideosRequest() {
        if (await Helpers.CheckNetInfo()) {
            await this.setState({
                loading: false
            });
        }

        try {

            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("perpage", 5);
            formData.append("offset", 1);

            let response = await fetch(this.props.global.baseApiUrl + '/view/allvideos',
                {
                    method: "POST",
                    body: formData
                });

            if (response.status !== 200) {
                await this.setState({
                    loading: false
                });
            } else {

                let json = await response.json();

                if (json.success) {

                    await this.setState({
                        videos: json.data,
                        loading: false
                    });
                } else {
                    await this.setState({
                        loading: false
                    });
                }
            }
        } catch (error) {
            await this.setState({
                loading: false
            });
        }
    }

    renderVideoItem({item, index}) {
        if (item.id === this.video.id) return null;
        return (
            <TouchableOpacity style={[styles.singleVideo, {
                margin: 0,
                width: Dimensions.get('window').width * .6,
                height: Dimensions.get('window').height * .2,
                marginRight: index === 1 ? 10 : 10,
                marginLeft: index + 1 === this.state.videos.length ? 10 : 0
            }]}
                              key={item.id} activeOpacity={.8} onPress={() => Actions.push('video', {video: item})}>
                <ImageBackground resizeMode='cover' style={styles.singleVideoImage}
                                 source={{uri: item.image + '?ref =' + this.newDate}}>
                    <View style={styles.singleVideoTop}>
                        <View style={[styles.singleVideoPlay, {width: 40, height: 40, borderRadius: 20}]}>
                            <Icon style={[styles.singleVideoPlayIcon, {fontSize: 30}]} name='ios-play'/>
                        </View>
                    </View>
                    <View style={styles.singleVideoBottom}>
                        <LinearGradient colors={['rgba(0,0,0,0)', 'rgba(0,0,0,.9)']} style={styles.grButtonGradient}>
                            <Text numberOfLines={1}
                                  style={[styles.singleVideoTitle, {fontSize: 12}]}>{item.title}</Text>
                        </LinearGradient>
                    </View>
                </ImageBackground>
            </TouchableOpacity>
        );
    }

    //Files
    async showFile() {
        let item = this.video;

        this.selectedFile = item;
        let fileName = 'video-' + item.id + '.mp4';
        let source = '';
        if (await AsyncStorage.getItem(fileName) !== null) {
            source = await AsyncStorage.getItem(fileName);
            await this.setState({
                hasLocale: true,
                fileName,
                source,
                downloadPercent: 0,
                downloading: false,
                totalSize: 0,
                downloadedSize: 0,
                canceled: false,
                showFileModal: true
            });
        } else {
            source = item.url;
            await this.setState({
                hasLocale: false,
                fileName,
                source,
                downloadPercent: 0,
                downloading: false,
                totalSize: 0,
                downloadedSize: 0,
                canceled: false,
                showFileModal: true
            });
        }

    }

    async view() {
        try {
            await this.setState({showFileModal: false});

            setTimeout(() => {
                if (this.state.hasLocale) {
                    if (Platform.OS === "ios") {
                        const ios = RNFetchBlob.ios;
                        ios.openDocument(this.state.source);
                    } else {
                        const android = RNFetchBlob.android;
                        android.actionViewIntent(this.state.source, 'video/mp4');
                    }
                } else {
                    // Linking.openURL(this.state.source);
                    this.setState({paused: !this.state.paused})
                }
            }, 500);

        } catch (error) {
            console.log(error);
        }
    }

    async downloadFile() {
        let dirs = RNFetchBlob.fs.dirs;

        await this.setState({
            downloading: true,
            canceled: false,
            downloadPercent: 0,
            totalSize: 0,
            downloadedSize: 0
        });

        this.downLoadTask = RNFetchBlob
            .config({
                fileCache: true,
                path: dirs.DocumentDir + "/" + this.state.fileName
            })
            .fetch('GET', this.state.source);


        this.downLoadTask.progress({interval: 500}, (received, total) => {
            this.setState({
                totalSize: total,
                downloadedSize: Math.round((received / 1024) / 1024),
                downloadPercent: received / total
            });
            //console.log(received,total);
        })
            .then(async (res) => {
                //console.log(res.path());
                if (!this.state.canceled) {
                    await AsyncStorage.setItem(this.state.fileName, res.path());
                    await this.setState({
                        source: res.path(),
                        hasLocale: true,
                        downloading: false
                    });
                } else {
                    this.setState({
                        downloading: false,
                        downloadPercent: 0,
                        totalSize: 0,
                        downloadedSize: 0
                    })
                }
            })
            .catch((errorMessage) => {
                console.log(errorMessage);
            });
    }

    async cancelDownload() {
        this.downLoadTask.cancel(async (err, taskId) => {
            await this.setState({
                canceled: true,
                downloading: false,
                downloadPercent: 0,
                totalSize: 0,
                downloadedSize: 0,
            });
            //console.log(`Cancel: taskId ${taskId}`);

            let localeAddress = await AsyncStorage.getItem(this.state.fileName);

            if (localeAddress !== null) {
                await RNFetchBlob.fs.unlink(localeAddress).then(async () => {
                    await AsyncStorage.removeItem(this.state.fileName);
                    await this.setState({
                        hasLocale: false,
                        source: this.selectedFile.url
                    });
                });
            }

        });
    }

    async deleteFile() {
        let localeAddress = await AsyncStorage.getItem(this.state.fileName);
        RNFetchBlob.fs.unlink(localeAddress).then(() => {
            AsyncStorage.removeItem(this.state.fileName);
            this.setState({
                hasLocale: false,
                source: this.selectedFile.url
            });
        });
    }

    onLoadVideo = (data) => {
        this.setState({duration: data.duration});
    };
    onProgressVideo = (data) => {
        this.setState({currentTime: data.currentTime / this.state.duration});
    };

    onEndVideo() {
        this.setState({pausedText: 'Play', paused: true});
        this.player.seek(0);
    }

    handleProgressPress = (e) => {
        const position = e.nativeEvent.locationX;
        const progress = (position / 350) * this.state.duration;
        this.player.seek(progress);
    };

    render() {
        let ModalContent = () => {
            if (this.state.hasLocale) {
                return (
                    <View style={styles.fileModalRow}>
                        <View style={[styles.fileModalColumn, {borderLeftWidth: 0}]}>
                            <TouchableOpacity onPress={this.deleteFile}
                                              style={{justifyContent: 'center', alignItems: 'center'}}>
                                <Icon name="ios-trash" style={styles.fileModalIcon}/>
                                <Text style={styles.fileModalText}>حذف از حافظه</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.fileModalColumn}>
                            <TouchableOpacity onPress={this.view}
                                              style={{justifyContent: 'center', alignItems: 'center'}}>
                                <Icon name="ios-cloud-done" style={styles.fileModalIcon}/>
                                <Text style={styles.fileModalText}>مشاهده آفلاین</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                );
            } else {

                let LocaleSection = () => {
                    if (this.state.downloading) {
                        return (
                            <View>
                                <Text style={[styles.fileModalText, {marginBottom: 10}]}>در حال دریافت</Text>
                                {this.state.totalSize > 0 &&
                                <Progress.Bar progress={this.state.downloadPercent} color={this.props.global.grColorTwo}
                                              borderColor="#dddddd" width={100}/>}
                                <Text style={[styles.fileModalText, {
                                    fontSize: 10,
                                    color: this.props.global.grColorTwo
                                }]}>{Helpers.ToPersianNumber(this.state.downloadedSize.toString())} مگابایت</Text>
                                <TouchableOpacity onPress={this.cancelDownload}
                                                  style={{justifyContent: 'center', alignItems: 'center'}}
                                                  hitSlop={{top: 10, left: 10, bottom: 10, right: 10}}>
                                    <Text
                                        style={[styles.fileModalText, {marginTop: 20, fontSize: 10, color: '#fd5459'}]}>لغو
                                        دانلود</Text>
                                </TouchableOpacity>
                            </View>
                        );
                    } else {
                        return (
                            <TouchableOpacity onPress={this.downloadFile}
                                              style={{justifyContent: 'center', alignItems: 'center'}}>
                                <Icon name="ios-cloud-download" style={styles.fileModalIcon}/>
                                <Text style={styles.fileModalText}>دانلود</Text>
                            </TouchableOpacity>
                        );
                    }
                };

                return (
                    <View style={styles.fileModalRow}>
                        <View style={[styles.fileModalColumn, {borderLeftWidth: 0}]}>
                            <LocaleSection/>
                        </View>
                        <View style={styles.fileModalColumn}>
                            <TouchableOpacity onPress={this.view}
                                              style={{justifyContent: 'center', alignItems: 'center'}}>
                                <Icon name="logo-youtube" style={styles.fileModalIcon}/>
                                <Text style={styles.fileModalText}>مشاهده آنلاین</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                );
            }
        };
        return (
            <Container style={styles.mainContainer}>
                <Image resizeMode='stretch' style={[styles.mainBackground,{height:2000}]} source={require("./../../assets/images/shared/menu-bg.png")} />
                <Header style={styles.header}>
                    <Left style={{flex: .2, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start'}}>
                        <BackButton/>
                    </Left>
                    <Right style={{flex: 2, alignItems: 'center', justifyContent: 'flex-end'}}>
                        <HeaderRightLabel title={this.props.video.title}/>
                    </Right>
                </Header>

                <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content"/>

                <Content style={{backgroundColor: '#fff', flex: 1}}>
                    <TouchableOpacity style={[styles.singleVideo, {
                        margin: 0,
                        width: Dimensions.get('window').width,
                        borderRadius: 0
                    }]} activeOpacity={.8} onPress={()=>  this.setState({paused: !this.state.paused})}>

                        <PlayVideo
                            style={{
                                position: 'absolute',
                                top: 0,
                                left: 0,
                                bottom: 0,
                                right: 0,
                                zIndex: 0,
                                width:width,
                            }}
                            ref={(ref) => {
                                this.player = ref
                            }}
                            source={{uri: this.video.url}}
                            repeat={this.state.repeat}
                            rate={this.state.rate}
                            volume={this.state.volume}
                            muted={this.state.muted}
                            resizeMode='stretch'
                            paused={this.state.paused}
                            onLoad={this.onLoadVideo}
                            onProgress={this.onProgressVideo}
                            onEnd={this.onEndVideo}
                        />
                        {(this.state.paused) &&
                        <View style={styles.singleVideoTop}>
                            <View style={styles.singleVideoPlay}>
                                <Icon style={styles.singleVideoPlayIcon}
                                      name={!this.state.paused ? "ios-pause" : "ios-play"} size={width * .05}/>
                            </View>
                        </View>
                        }
                        <View style={styles.viewContainerProgress}>
                            <TouchableWithoutFeedback onPress={this.handleProgressPress}>
                                 <View>
                                    <Progress.Bar
                                        progress={this.state.currentTime}
                                        color={'#fff'}
                                        unfilledColor={"rgba(255,255,255,.5)"}
                                        borderColor={'#fff'}
                                        width={350}
                                        height={20}
                                    />
                                </View>
                            </TouchableWithoutFeedback>
                        </View>
                        {/*<ImageBackground resizeMode='cover' style={styles.singleVideoImage} source={{uri: this.video.image + '?ref =' + this.newDate}}>*/}
                        {/*    <View style={styles.singleVideoTop}>*/}
                        {/*        <View style={styles.singleVideoPlay}>*/}
                        {/*            <Icon style={styles.singleVideoPlayIcon} name='ios-play'/>*/}
                        {/*        </View>*/}
                        {/*    </View>*/}
                        {/*</ImageBackground>*/}
                    </TouchableOpacity>
                    <View style={styles.videoTitleContainer}>
                        <Text style={styles.videoTitleText}>{this.video.title}</Text>
                    </View>
                    {this.video.desc !== null && <View>
                        <View style={styles.videoSectionContainer}>
                            <Text style={styles.videoSectionText}>توضیحات</Text>
                        </View>
                        <View style={styles.videoDescContainer}>
                            <Text style={styles.videoDescText}>{this.video.desc}</Text>
                        </View>
                    </View>}
                    {(!this.state.loading && this.state.videos.length > 0) && <View>
                        <View style={styles.videoSectionContainer}>
                            <Text style={styles.videoSectionText}>جدیدترین ویدئوها</Text>
                        </View>
                        <View style={{marginVertical: 10}}>
                            <FlatList
                                data={this.state.videos}
                                renderItem={this.renderVideoItem.bind(this)}
                                keyExtractor={(item) => item.id.toString()}
                                style={{backgroundColor: 'transparent'}}
                                horizontal={true}
                                inverted={true}
                                showsHorizontalScrollIndicator={false}
                            />
                        </View>
                    </View>}
                    {this.state.loading && <View>
                        <View style={styles.videoSectionContainer}>
                            <Text style={styles.videoSectionText}>جدیدترین ویدئوها</Text>
                        </View>
                        <View style={{marginVertical: 10}}>
                            <ActivityIndicator color={this.props.global.grColorTwo} size='large'/>
                        </View>
                    </View>}
                </Content>

                <Modal isVisible={this.state.showFileModal} backdropOpacity={0.3} animationIn="fadeIn"
                       animationOut="fadeOut"
                       onBackdropPress={() => this.setState({showFileModal: false})}
                       hitSlop={{top: 10, left: 10, bottom: 10, right: 10}}>
                    <View style={styles.fileModal}>
                        <View style={styles.fileModalContainer}>
                            <TouchableOpacity onPress={() => this.setState({showFileModal: false})}
                                              style={{position: 'absolute', top: 3, left: 5, zIndex: 999999}}>
                                <Icon name="ios-close-circle" style={{color: '#555555', fontSize: 20}}/>
                            </TouchableOpacity>
                            <ModalContent/>
                        </View>
                    </View>
                </Modal>

            </Container>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setApp: app => {
            dispatch(setApp(app))
        }
    }
};

const mapStateToProps = (state) => {
    return {
        user: state.user,
        global: state.global,
        app: state.app
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Video);