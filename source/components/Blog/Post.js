import React from 'react';
import {ActivityIndicator, Text, Dimensions, StatusBar, WebView, Image, Platform,View,TextInput,TouchableOpacity} from 'react-native';

import {Container, Header, Left, Body, Right, Content,Icon} from 'native-base';
import {styles} from './Style';
import BackButton from "../Shared/BackButton";
import Helpers from "../Shared/Helper";
import HTML from 'react-native-render-html';
import {connect} from "react-redux";
import LoadingModal from "../Shared/LoadingModal";
import Modal from "react-native-modal";
import LinearGradient from "react-native-linear-gradient";
const width = Dimensions.get('window').width;
import moment from 'jalali-moment';
import { Actions } from 'react-native-router-flux';

class Post extends React.Component {

    constructor(props)
    {
        super(props);

        //console.log(props);

        this.state = {
            url: '',
            loading: false,
            loadingComment:true,
            Comment:'',
            Comments:[],
            Done:false,
            ErrorAddComment:false
        };

        this.post_desc = props.post.post_desc !== null ? "<div style='font-family: Vazir;font-size:12px;direction: rtl;text-align: right;'>" + props.post.post_desc + "</div>" : "<div style='font-family: Vazir;font-size:12px;direction: rtl;text-align: right;'>توضیحی ثبت نشده است!</div>"; ;

        console.log(this.props);

        this.renderImage = this.renderImage.bind(this);
    }

    async componentWillMount(){
        if(await Helpers.CheckNetInfo()){
            return;
        }
        await this.GetComments()
        this.setState({
            url: this.props.url,
        })
    }
    async GetComments(){
        try {
            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("post_id", this.props.post.id);
            formData.append("offset", 3);
            formData.append("status", 'approve');

            //console.log(this.props.product)
            let response = await fetch(this.props.global.baseApiUrl + '/comment/get',
            
                {
                    method: "POST",
                    body: formData
                });
                let result = await response.json()
                console.log(result)

            if (!result.success) {
                await this.setState({
                    loadingComment:false
                });
            }
            else {
                await this.setState({
                    Comments:result.data,
                    loadingComment:false
                })
            }
            console.log(this.state.Comments)


        } catch (error) {
            console.log(error);
            await this.setState({
                loading:false
            });
        }
    }

    renderImage(prop){
        let width = prop.width && prop.width !== null ? parseInt(prop.width) : 0;
        let height = prop.height && prop.height !== null ? parseInt(prop.height) : 0;
        //console.log(prop);
        return(
            <Image key={12} source={{uri : prop.src}} style={[styles.renderedImage,{width:width}]} />
        );
    }
    async AddComment(){
        if(this.props.user.apiToken === null){
            this.setState({ErrorAddComment:true})
        }
        else{
            this.setState({showLoadingModal:true})
        try {
            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("usertoken", this.props.user.apiToken);
            formData.append("post_id", this.props.post.id);
            formData.append("content", this.state.Comment);
            formData.append("parent", "");

            let response = await fetch(this.props.global.baseApiUrl + '/comment/add',
                {
                    method: "POST",
                    body: formData
                });
                let result = await response.json()
                console.log(result)

            if (!result.success) {
                await this.setState({
                    
                    loadingComment:false,
                    showLoadingModal:false
                });
            }
            else {
                await this.setState({
                    Done:true,
                    loadingComment:false,
                    showLoadingModal:false,
                    Comment:''
                },()=>this.GetComments())
            }
            console.log(this.state.Comments)


        } catch (error) {
            console.log(error);
            await this.setState({
                loading:false
            });
        }

        }
    }
    sample(item){
        return(
            <View style={{padding:10,borderBottomColor:'#f1f4f7',borderBottomWidth:1,paddingBottom:20}}>
                <View style={{flexDirection:'row-reverse',alignItems:'center',marginVertical:5}}>
                    <Image resizeMode='contain' style={{height:60,width:60,borderRadius:30}} source={require("./../../assets/images/shared/avatar.png")} />
                    <View style={{marginRight:10,alignItems:'flex-end',justifyContent:'center',paddingBottom:5}}>
                        {this.state.Comments[item].comment_author == "" &&
                        <Text style={{fontFamily:'IRANSansMobileBold',fontSize:16,color:'#3399db'}}>{this.state.Comments[item].first_name+" "+this.state.Comments[item].last_name}</Text>
                        }
                        {this.state.Comments[item].comment_author != "" &&
                        <Text style={{fontFamily:'IRANSansMobileBold',fontSize:16,color:'#3399db'}}>{this.state.Comments[item].comment_author}</Text>
                        }                        
                        <Text style={{fontFamily:'IRANSansMobile',fontSize:10}}>{Helpers.ToPersianNumber(moment(this.state.Comments[item].comment_date, 'YYYY-MM-DD').locale('fa').format('DD MMMM YYYY'))}</Text>
                    </View>
                </View>
                <View>
                    <Text style={{fontFamily:'IRANSansMobileBold',fontSize:13,textAlign:'right',paddingBottom:10,color:'black'}}>{this.state.Comments[item].comment_content}</Text>
                </View>
            </View>
        )

    }

    render() {
        return (
            <Container>
                <Header style={[styles.browserHeader,{overflow: 'hidden',zIndex:999}]} >
                    <Image resizeMode='stretch' style={[styles.mainBackground,{height:2000}]} source={require("./../../assets/images/shared/menu-bg.png")} />
                    <Left style={{flex: .2,justifyContent:'center',alignItems:'flex-start'}}>
                        <BackButton/>
                    </Left>
                    <Body style={{flex: 1,justifyContent:'center',alignItems:'center'}}>
                    <Text numberOfLines={1} style={[styles.notFoundText,{marginTop:0,color:'#fff'}]}>{this.props.title}</Text>
                    </Body>
                    <Right style={{flex: .2,justifyContent:'center',alignItems:'flex-end'}} >
                        {this.state.loading && <ActivityIndicator color="#ffffff"/>}
                    </Right>
                </Header>

                <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content" />

                {/*<WebView*/}
                {/*source={{uri: this.state.url , method : 'GET' }}*/}
                {/*onLoadStart={() => { this.setState({loading:true}) } }*/}
                {/*onLoadEnd={() => { this.setState({loading:false}) } }*/}
                {/*style={{zIndex:-1}}*/}
                {/*/>*/}
                <Content style={{flex:1,backgroundColor:'#ffffff'}}>
                    <Image source={{uri:this.props.post.image}} style={{width: width,height:width *.50}}/>
                    <HTML html={this.post_desc}
                          containerStyle={{margin:10}}
                          tagsStyles={{
                              p : {justifyContent:Platform.OS === 'ios' ? 'flex-end' : 'flex-start',textAlign:Platform.OS === 'ios' ? 'justify' : 'right',writingDirection:'rtl'} ,
                              div : {textAlign:Platform.OS === 'ios' ? 'justify' : 'right',writingDirection:'rtl'},
                              ul : {textAlign:Platform.OS === 'ios' ? 'justify' : 'right',writingDirection:'rtl'},
                              li : {textAlign:Platform.OS === 'ios' ? 'justify' : 'right',writingDirection:'rtl'}
                          }}
                        //imagesMaxWidth={200} imagesInitialDimensions={{width:200,height:200}}
                          renderers = {{
                              img: this.renderImage
                          }}
                    />
                        <View style={[styles.productDescInfoContainer,{borderColor:'white'}]}>
                            <Text style={[styles.productDescInfoTitle,{borderBottomColor:'#f1f4f7',borderBottomWidth:1,paddingBottom:20}]}>
                                 دیدگاه ها
                            </Text>
                            <View>
                                {(!this.state.loadingComment && this.state.Comments.length==0)&&
                                    <Text style={{fontFamily:'IRANSansMobile',textAlign:'center',color:'#3399db',paddingVertical:15,borderBottomWidth:1,borderBottomColor:'#f1f4f7'}}>دیدگاهی وجود ندارد</Text>
                                }
                                {(!this.state.loadingComment && this.state.Comments.length>0)&& this.sample(0)}
                                {(!this.state.loadingComment && this.state.Comments.length>1)&& this.sample(1)}
                                {(!this.state.loadingComment && this.state.Comments.length>2)&& this.sample(2)}
                                {(!this.state.loadingComment && this.state.Comments.length>3)&&
                                <TouchableOpacity onPress={()=>Actions.push('allComments',{data:this.state.Comments})}>
                                    <Text style={{fontFamily:'IRANSansMobile',textAlign:'center',color:'#3399db',paddingVertical:15,borderBottomWidth:1,borderBottomColor:'#f1f4f7'}}>نمایش دیدگاه های بیشتر</Text>
                                </TouchableOpacity>
                                }
                            </View>
                            <View style={{flex:1,paddingTop:40,flexDirection:'row-reverse',alignSelf:'center',alignItems:'center',justifyContent:'center'}}>
                                <TextInput underlineColorAndroid="rgba(0,0,0,0)" style={styles.textInputComment}
                                   placeholderTextColor="#cccccc" placeholder='متن مورد نظر خود را تایپ کنید ...' value={this.state.Comment}
                                   onChangeText={(text)=>this.setState({Comment:text})}/>
                                   <TouchableOpacity onPress={()=>this.AddComment()} style={{borderRadius:25,elevation:5,backgroundColor:'#5f656a',width:50,height:50,alignItems:'center',justifyContent:'center'}}>
                                       <Icon name='paper-plane' style={{color:'white',fontFamily:'IRANSansMobile',padding:5,paddingHorizontal:10}}/>
                                   </TouchableOpacity>
                            </View>
                        </View>
                </Content>
                <LoadingModal show={this.state.showLoadingModal} />
                <Modal onBackdropPress={()=>this.setState({Done:false})} onBackButtonPress={()=>this.setState({Done:false})} isVisible={this.state.Done} backdropOpacity={0.5} animationIn="fadeIn" animationOut="fadeOut" backdropColor='#000000'>
                    <View style={styles.loadingModalContainer}>
                        <Text style={{fontFamily:'IRANSansMobile',textAlign:'center',color:'black',paddingVertical:15,}}>دیدگاه شما ثبت شد و در انتظار تایید میباشد</Text>
                        <TouchableOpacity style={[styles.modalColorGrButton,{marginLeft:15}]} activeOpacity={.8} onPress={()=>this.setState({Done:false})}>
                            <LinearGradient colors={[this.props.global.grColorOne,this.props.global.grColorTwo]} style={styles.modalGrButtonGradient} start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}>
                                <Text style={styles.modalGrButtonText}>تایید</Text>
                            </LinearGradient>
                        </TouchableOpacity>
                    </View>
                </Modal>
                <Modal onBackdropPress={()=>this.setState({ErrorAddComment:false})} onBackButtonPress={()=>this.setState({ErrorAddComment:false})} isVisible={this.state.ErrorAddComment} backdropOpacity={0.5} animationIn="fadeIn" animationOut="fadeOut" backdropColor='#000000'>
                    <View style={styles.loadingModalContainer}>
                        <Text style={{fontFamily:'IRANSansMobile',textAlign:'center',color:'black',paddingVertical:15,}}>برای ثبت دیدگاه ابتدا وارد شوید</Text>
                        <View style={{flexDirection:'row',justifyContent:'space-around'}}>
                            <TouchableOpacity style={[styles.modalColorGrButton,{marginLeft:15}]} activeOpacity={.8} onPress={()=>this.setState({ErrorAddComment:false})}>
                                <LinearGradient colors={[this.props.global.grColorOne,this.props.global.grColorTwo]} style={styles.modalGrButtonGradient} start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}>
                                    <Text style={styles.modalGrButtonText}>تایید</Text>
                                </LinearGradient>
                            </TouchableOpacity>
                            <TouchableOpacity style={[styles.modalColorGrButton,{marginLeft:15}]} activeOpacity={.8} onPress={()=>this.setState({ErrorAddComment:false},()=>Actions.push('intro',{initialIndex:1}))}>
                                <LinearGradient colors={[this.props.global.grColorOne,this.props.global.grColorTwo]} style={styles.modalGrButtonGradient} start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}>
                                    <Text style={styles.modalGrButtonText}>ورود</Text>
                                </LinearGradient>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>

            </Container>
        )
    }
}

const mapStateToProps =(state) =>  {
    return{
        user: state.user,
        global:state.global,
        app:state.app
    }
};

export default connect(mapStateToProps,null)(Post);
