import React from 'react';
import {
    StatusBar,
    Image,
    ActivityIndicator,
    Text,
    TouchableOpacity,
    View,
    FlatList,
    PixelRatio,
    ImageBackground
} from 'react-native';
import {Header, Icon, Left, Right, Content, Container, Body} from 'native-base';
import styles from './Style';
import { Actions } from 'react-native-router-flux';
import {setApp, setUser} from "../../redux/Actions/Index";
import {connect} from "react-redux";
import HeaderRightLabel from "../Shared/HeaderRightLabel";
import BackButton from "../Shared/BackButton";
import Helpers from "../Shared/Helper";
import moment from 'jalali-moment';
import LinearGradient from "react-native-linear-gradient";

class Videos extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            videos:[],
            page:1,
            hasMore:true,
            loading:true,
            refreshing:false,
            showLoadingModal:false
        };

        this.newDate = props.global.newDate;

        this.getVideosRequest = this.getVideosRequest.bind(this);

    }

    async componentWillMount(){
        await this.getVideosRequest();
    }

    async getVideosRequest(){
        //console.log(this.state);
        if(await Helpers.CheckNetInfo()){
            return;
        }

        try {
            let page = this.state.page;

            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("perpage", '10');
            formData.append("offset", page);

            let response = await fetch(this.props.global.baseApiUrl + '/view/allvideos',
                {
                    method: "POST",
                    body: formData
                });

            console.log(response);

            if (response.status !== 200) {
                await this.setState({
                    loading:false,
                    refreshing:false,
                    hasMore:false
                });
            }
            else {

                let json = await response.json();

                if(json.success){

                    await this.setState(prevState => {
                        return {
                            videos: page === 1 ? json.data : [...prevState.videos, ...json.data],
                            loading: false,
                            refreshing:false,
                        }
                    });

                }
                else {
                    await this.setState({
                        loading:false,
                        hasMore:false,
                        refreshing:false
                    });
                }
            }

        } catch (error) {
            //console.log(error);
            await this.setState({
                loading: false,
                refreshing:false,
                hasMore:false
            })
        }
    }

    renderItem({ item,index }) {
        return (
            <TouchableOpacity style={[styles.singleVideo,{marginTop:index === 0 ? 10 : 10,marginBottom:index + 1 === this.state.videos.length ? 10 : 0}]} key={item.id} activeOpacity={.8}
                              onPress={() => Actions.push('video',{video:item})}>
                <ImageBackground resizeMode='cover' style={styles.singleVideoImage} source={{uri: item.image + '?ref =' + this.newDate}}>
                    <View style={styles.singleVideoTop}>
                        <View style={styles.singleVideoPlay}>
                            <Icon style={styles.singleVideoPlayIcon} name='ios-play'/>
                        </View>
                    </View>
                    <View style={styles.singleVideoBottom}>
                        <LinearGradient colors={['rgba(0,0,0,0)','rgba(0,0,0,.9)']} style={styles.grButtonGradient}>
                            <Text numberOfLines={2} style={styles.singleVideoTitle}>{item.title}</Text>
                        </LinearGradient>
                    </View>
                </ImageBackground>
            </TouchableOpacity>
        );
    }
    handleEmpty() {
        if(this.state.loading)
        {
            return(
                <ActivityIndicator style={styles.listLoadingIndicator} color={this.props.global.grColorTwo} size='large' />
            );
        }
        else
        {
            return(
                <Text style={styles.notFoundText}>ویدئویی یافت نشد.</Text>
            );
        }
    }
    renderFooter() {
        if(this.state.loading && this.state.videos.length > 0)
        {
            return <ActivityIndicator style={{alignSelf: 'center',marginBottom: 20}} color={this.props.global.grColorTwo} />
        }
        else {
            return null;
        }
    }
    async handleRefresh() {
        if(await Helpers.CheckNetInfo()){
            return;
        }

        await this.setState({ page : 1 , refreshing : true ,hasMore:true} ,async () => {
            await this.getVideosRequest();
        });
    }
    async handleLoadMore() {
        if(this.state.hasMore) {
            await this.setState({page : this.state.page + 1 , loading : true}, async () => {
                await this.getVideosRequest()
            })
        }
    }

    render() {

        return (
            <Container style={stylesmainContainer}>
                <Image resizeMode='stretch' style={[styles.mainBackground,{height:2000}]} source={require("./../../assets/images/shared/menu-bg.png")} />
                <Header style={styles.header}>
                    <Left style={{flex:1,flexDirection:'row',alignItems:'center',justifyContent:'flex-start'}}>
                        <BackButton/>
                    </Left>
                    <Right style={{flex:2,alignItems:'center',justifyContent:'flex-end'}}>
                        <HeaderRightLabel title='ویدئو ها'/>
                    </Right>
                </Header>

                <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content" />

                <View style={{backgroundColor:'#fff',flex:1}}>
                    <FlatList
                        data={this.state.videos}
                        renderItem={this.renderItem.bind(this)}
                        ListEmptyComponent={this.handleEmpty.bind(this)}
                        ListFooterComponent={this.renderFooter.bind(this)}
                        keyExtractor={(item) => item.id.toString()}
                        style={{backgroundColor:'transparent'}}
                        showsVerticalScrollIndicator={false}
                        refreshing={this.state.refreshing}
                        onRefresh={this.handleRefresh.bind(this)}
                        onEndReached={this.handleLoadMore.bind(this)}
                        onEndReachedThreshold={2}
                        removeClippedSubviews={true}
                    />
                </View>

            </Container>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setApp : app => {
            dispatch(setApp(app))
        }
    }
};

const mapStateToProps =(state) =>  {
    return{
        user: state.user,
        global:state.global,
        app:state.app
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(Videos);