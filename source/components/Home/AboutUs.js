import React from 'react';
import {StatusBar, Image, TextInput, AsyncStorage,Text, Dimensions,TouchableOpacity,View,ScrollView,ActivityIndicator} from 'react-native';
import {Header, Icon, Left, Right, Content, Container, Body} from 'native-base';
import styles from './Style';
import { Actions } from 'react-native-router-flux';
import {setUser} from "../../redux/Actions/Index";
import {connect} from "react-redux";
import MenuButton from "../Shared/MenuButton";
import BasketButton from "../Shared/BasketButton";
import ProfileInfoIcon from "../../assets/icon/ProfileInfoIcon";
import ProfileHistoryIcon from "../../assets/icon/ProfileHistoryIcon";
import ProfileAddressIcon from "../../assets/icon/ProfileAddressIcon";
import ProfileEditIcon from "../../assets/icon/ProfileEditIcon";
import ProfileLogoutIcon from "../../assets/icon/ProfileLogoutIcon";
import HeaderRightLabel from "../Shared/HeaderRightLabel";
import Helpers from "../Shared/Helper";
import Modal from "react-native-modal";
import LinearGradient from "react-native-linear-gradient";
import OtherPasswordIcon from "../../assets/icon/OtherPasswordIcon";
import BackButton from "../Shared/BackButton";

class AboutUs extends React.Component {

    constructor(props)
    {
        super(props);
        this.state = {
            loading:true,
            AboutUs:''
        };
    }
    componentWillMount(){
        this.GetAboutUs()
    }

    async GetAboutUs(){
        try {
            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            let response = await fetch(this.props.global.baseApiUrl + '/user/contact_us_data',
            
                {
                    method: "POST",
                    body: formData
                });
                let result = await response.json()
                console.log(result)

            if (!result.success) {
                await this.setState({
                    loading:false
                });
            }
            else {
                await this.setState({
                    AboutUs:result.data.about_us,
                    loading:false
                })
            }


        } catch (error) {
            console.log(error);
            await this.setState({
                loading:false
            });
        }
    }

    render() {

        return (
            <Container style={styles.mainContainer}>
                <Image resizeMode='stretch' style={[styles.mainBackground,{height:1000}]} source={require("./../../assets/images/shared/menu-bg.png")} />
                <Header style={styles.header}>
                    <Left style={{flex:0.2,alignItems:'center',justifyContent:'center',flexDirection:'row-reverse'}}>
                        <BasketButton/>
                        <BackButton/>
                    </Left>
                    <Body style={{flex:1,alignItems:'flex-end',justifyContent:'center'}}>
                    <HeaderRightLabel title='درباره ما'/>
                    </Body>
                    <Right style={{flex:.2,alignItems:'center',justifyContent:'center'}}>
                        <MenuButton/>
                    </Right>
                </Header>

                <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content" />

                <Content style={{backgroundColor:'#fff'}}>
                    <View style={{backgroundColor:'#fff',padding:10}}>
                    {this.state.loading && <ActivityIndicator color={this.props.global.grColorTwo} size='large'/>}
                    {!this.state.loading &&
                        <Text style={{fontFamily:'IRANSansMobile'}}>{this.state.AboutUs}</Text>
                    }
                    </View>
                </Content>
            </Container>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setUser : user => {
            dispatch(setUser(user))
        }
    }
};

const mapStateToProps =(state) =>  {
    return{
        user: state.user,
        global:state.global
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(AboutUs);