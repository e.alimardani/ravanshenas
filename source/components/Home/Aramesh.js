import React from 'react';
import {
    StatusBar,
    Image,
    TextInput,
    ActivityIndicator,
    Text,
    RefreshControl,
    TouchableOpacity,
    View,
    FlatList,
    Dimensions, ImageBackground,Linking
} from 'react-native';
import {Header, Icon, Left, Right, Content, Container, Body, Accordion} from 'native-base';
import styles from './Style';
import { Actions } from 'react-native-router-flux';
import {setUser} from "../../redux/Actions/Index";
import {connect} from "react-redux";
import MenuButton from "../Shared/MenuButton";
import BasketButton from "../Shared/BasketButton";
import HeaderRightLabel from "../Shared/HeaderRightLabel";
import Helpers from "../Shared/Helper";
import Swiper from "react-native-swiper";
import moment from 'jalali-moment';
import NumberFormat from "react-number-format";
import Modal from "react-native-modal";
import LinearGradient from "react-native-linear-gradient";
import BackButton from "../Shared/BackButton";

class Aramesh extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            refreshing: false,
            sliders:[],
            sliderLoading:true,
            posts:[],
            postLoading:true,
            newProducts:[],
            newProductsLoading:true,
            videos:[],
            videoLoading:true,
            categories:[],
            categoryLoading:true,
            showModal:false,
            Ads:[],
            AdsLoading:true,
            cats:[
                {
                    id:25,
                    image:require('./../../assets/images/home/aramesh.png'),
                    title:'آرامش',
                    link:'http://ravanshenasesabz.com/aramesh'
                },
                {
                    id:26,
                    image:require('./../../assets/images/home/salamati.png'),
                    title:'سلامتی',
                    link:'http://ravanshenasesabz.com/salamati'
                },
                {
                    id:28,
                    image:require('./../../assets/images/home/movafaghiat.png'),
                    title:'موفقیت',
                    link:'http://ravanshenasesabz.com/movafaghiat'
                },
                {
                    id:27,
                    image:require('./../../assets/images/home/khoshbakhti.png'),
                    title:'خوشبختی',
                    link:'http://ravanshenasesabz.com/khoshbakhti'
                }
            ]
        };

        this.newDate = props.global.newDate;

        this.search = this.search.bind(this);
        this.onRefresh = this.onRefresh.bind(this);
        this.getSlidersRequest = this.getSlidersRequest.bind(this);
        this.getPostsRequest = this.getPostsRequest.bind(this);
        this.getCategoriesRequest = this.getCategoriesRequest.bind(this);
        this.getVideosRequest = this.getVideosRequest.bind(this);
        this.getNewProducts = this.getNewProducts.bind(this);

    }

    async componentWillMount(){
        await this.getCategoriesRequest();
        await this.getNewProducts();
        await this.getPostsRequest();
        await this.GetAds()
    }

    async GetAds(){

        try {

            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);

            let response = await fetch(this.props.global.baseApiUrl + '/user/get_advertise_banner',
                {
                    method: "POST",
                    body: formData
                });

            //console.log(response);

            if (response.status !== 200) {
                await this.setState({
                    AdsLoading:false
                });
            }
            else {

                let json = await response.json();
                console.log(json)
                if(json.success){

                    await this.setState({
                        Ads:json.data,
                        AdsLoading:false
                    });
                    //console.log(this.state.Ads)
                }
                else {
                    await this.setState({
                        AdsLoading:false
                    });
                }
            }
        } catch (error) {
            await this.setState({
                AdsLoading:false
            });
        }
    }

    async getSlidersRequest(){
        if (await Helpers.CheckNetInfo()) {
            await this.setState({
                sliderLoading:false
            });
        }

        try {

            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);

            let response = await fetch(this.props.global.baseApiUrl + '/user/get_sliders',
                {
                    method: "POST",
                    body: formData
                });

            let js = await response.json()
            //console.log(js);

            if (!js.success) {
                await this.setState({
                    sliderLoading:false
                });
            }
            else {
                if(js.success){
                    this.setState({
                        sliders:js.data,
                        sliderLoading:false
                    });
                    //console.log(this.state.sliders)
                }
                else {
                    await this.setState({
                        sliderLoading:false
                    });
                }
            }
        } catch (error) {
            await this.setState({
                sliderLoading:false
            });
        }
    }

    async getPostsRequest(){
        if (await Helpers.CheckNetInfo()) {
            await this.setState({
                postLoading:false
            });
        }

        try {

            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("postcatid", 25);

            let response = await fetch(this.props.global.baseApiUrl + '/post/getpostbycat',
                {
                    method: "POST",
                    body: formData
                });

            if (response.status !== 200) {
                await this.setState({
                    postLoading:false
                });
            }
            else {

                let json = await response.json();

                console.log(json);

                if(json.success){

                    await this.setState({
                        posts:json.data,
                        postLoading:false
                    });
                }
                else {
                    await this.setState({
                        postLoading:false
                    });
                }
            }
        } catch (error) {
            await this.setState({
                postLoading:false
            });
        }
    }
    renderPostItem({ item,index }) {
        return (
            <TouchableOpacity style={[styles.singlePost,{margin:0,borderRadius: 15,width : Dimensions.get('window').width * .35,marginRight:index === 1 ? 20 : 10 ,marginLeft:index + 1 === this.state.posts.length ? 20 : 0}]} key={item.id} activeOpacity={.8}
                              onPress={() => Actions.push('post' , {title:item.title,url:item.permalink,post:item})}>
                <View style={[styles.singlePostTop,{height:Dimensions.get('window').height * .15}]}>
                    <Image style={styles.singlePostImage} source={{uri: item.image + '?ref =' + this.newDate}}/>
                    {/*<View style={styles.singlePostCategory}>*/}
                        {/*<Text style={styles.singlePostCategoryText}>{item.categories[0].category_name}</Text>*/}
                    {/*</View>*/}
                </View>
                <View style={[styles.singlePostBottom,{height:60}]}>
                    <Text numberOfLines={2} style={[styles.singlePostTitle,{fontSize:10,textAlign: 'center'}]}>{item.title}</Text>
                    {/*<Text numberOfLines={1} style={styles.singlePostDate}>{Helpers.ToPersianNumber(moment(item.create_date, 'YYYY-MM-DD HH:mm').locale('fa').format('DD MMMM YYYY | HH:mm'))}</Text>*/}
                </View>
            </TouchableOpacity>
        );
    }

    renderCatsItem({ item,index }) {
        return (
            <TouchableOpacity style={[styles.singlePost,{margin:0,padding:5,borderRadius: 15,width : Dimensions.get('window').width * .35,marginRight:index === 1 ? 20 : 10 ,marginLeft:index + 1 === this.state.cats.length ? 20 : 0}]} key={item.id} activeOpacity={.8}
                              onPress={() => Actions.push('browser' , {title:item.title,url:item.link})}>
                <View style={[styles.singlePostTop,{height:Dimensions.get('window').height * .15}]}>
                    <Image style={styles.singlePostImage} source={item.image}/>
                    {/*<View style={styles.singlePostCategory}>*/}
                        {/*<Text style={styles.singlePostCategoryText}>{item.categories[0].category_name}</Text>*/}
                    {/*</View>*/}
                </View>
                <View style={[styles.singlePostBottom,{height:40}]}>
                    <Text numberOfLines={2} style={[styles.singlePostTitle,{fontSize:12,textAlign: 'center',color:'#0082f0'}]}>{item.title}</Text>
                    {/*<Text numberOfLines={1} style={styles.singlePostDate}>{Helpers.ToPersianNumber(moment(item.create_date, 'YYYY-MM-DD HH:mm').locale('fa').format('DD MMMM YYYY | HH:mm'))}</Text>*/}
                </View>
            </TouchableOpacity>
        );
    }

    async getVideosRequest(){
        if (await Helpers.CheckNetInfo()) {
            await this.setState({
                videoLoading:false
            });
        }

        try {
            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("perpage", 5);
            formData.append("offset", 1);

            let response = await fetch(this.props.global.baseApiUrl + '/view/allvideos',
                {
                    method: "POST",
                    body: formData
                });

            if (response.status !== 200) {
                await this.setState({
                    videoLoading:false
                });
            }
            else {

                let json = await response.json();

                if(json.success){

                    await this.setState({
                        videos:json.data,
                        videoLoading:false
                    });
                }
                else {
                    await this.setState({
                        videoLoading:false
                    });
                }
            }
        } catch (error) {
            await this.setState({
                videoLoading:false
            });
        }
    }
    renderVideoItem({ item,index }) {
        return (
            <TouchableOpacity style={[styles.singleVideo,{margin:0,width : Dimensions.get('window').width * .6,height:Dimensions.get('window').height * .2,marginRight:index === 1 ? 20 : 10 ,marginLeft:index + 1 === this.state.videos.length ? 20 : 0}]}
                              key={item.id} activeOpacity={.8} onPress={() => Actions.push('video',{video:item})}>
                <ImageBackground resizeMode='cover' style={styles.singleVideoImage} source={{uri: item.image + '?ref =' + this.newDate}}>
                    <View style={styles.singleVideoTop}>
                        <View style={[styles.singleVideoPlay,{width:40,height:40,borderRadius:20}]}>
                            <Icon style={[styles.singleVideoPlayIcon,{fontSize:30}]} name='ios-play'/>
                        </View>
                    </View>
                    <View style={styles.singleVideoBottom}>
                        <LinearGradient colors={['rgba(0,0,0,0)','rgba(0,0,0,.9)']} style={styles.grButtonGradient}>
                            <Text numberOfLines={1} style={[styles.singleVideoTitle,{fontSize:12}]}>{item.title}</Text>
                        </LinearGradient>
                    </View>
                </ImageBackground>
            </TouchableOpacity>
        );
    }

    async getCategoriesRequest(){
        if (await Helpers.CheckNetInfo()) {
            await this.setState({
                categoryLoading:false
            });
        }

        try {
            await this.setState({
                categoryLoading:true
            });

            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);

            let response = await fetch(this.props.global.baseApiUrl + '/post/getpostcats',
                {
                    method: "POST",
                    body: formData
                });

            //console.log(response);

            if (response.status !== 200) {
                await this.setState({
                    categoryLoading:false
                });
            }
            else {

                let json = await response.json();
                console.log(json)

                if(json.success){
                    await this.setState({
                        categories:json.data,
                        categoryLoading:false
                    });
                }
                else {
                    await this.setState({
                        categoryLoading:false
                    });
                }
            }
        } catch (error) {
            await this.setState({
                categoryLoading:false
            });
        }
    }

    async onRefresh(){
        await this.setState({
            sliders:[],
            sliderLoading:true,
            posts:[],
            postLoading:true,
            videos:[],
            videoLoading:true,
            newProducts:[],
            newProductsLoading:true,
            refreshing: true
        });
        await this.getSlidersRequest();
        await this.getNewProducts();
        await this.getPostsRequest();
        await this.getVideosRequest();
        await this.setState({
            refreshing: false
        });
    }

    async search(){
        //console.log(this.searchBox._lastNativeText);
        this.searchBox.blur();
        Actions.push('productSearch',{query:this.searchBox._lastNativeText});
    }

    async getNewProducts(){
        if (await Helpers.CheckNetInfo()) {
            await this.setState({
                newProductsLoading:false
            });
        }

        try {

            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("perpage", '10');
            formData.append("offset", '1');
            formData.append("pcatid", 48);
            formData.append("order", 'asc');
            formData.append("orderby", 'sale_price');

            let response = await fetch(this.props.global.baseApiUrl + '/product/getproductbycat',
                {
                    method: "POST",
                    body: formData
                });

            //console.log(response);

            if (response.status !== 200) {
                await this.setState({
                    newProductsLoading:false
                });
            }
            else {

                let json = await response.json();

                if(json.success){

                    await this.setState({
                        newProducts:json.data,
                        newProductsLoading:false
                    });
                }
                else {
                    await this.setState({
                        newProductsLoading:false
                    });
                }
            }
        } catch (error) {
            await this.setState({
                newProductsLoading:false
            });
        }
    }
    renderItemNew({ item,index }) {
        return (
            <TouchableOpacity style={[styles.productHorizontalSectionListItem,{marginRight:index === 0 ? 15 : 15,marginLeft:index + 1 === this.state.newProducts.length ? 15 : 0}]} key={item.id} activeOpacity={.8}
                              onPress={() => Actions.push('product',{product:item})}>
                <View style={styles.productHorizontalSectionListItemContainer}>
                    {item.image !== null && <Image resizeMode='contain' style={styles.productHorizontalSectionListItemImage} source={{uri:item.image + '?ref =' + this.newDate}}/>}
                    {item.image === null && <Image resizeMode='contain' style={styles.productHorizontalSectionListItemImage} source={require('./../../assets/images/shared/product.png')}/>}
                    {/*{item.oldPrice !== '' && <View style={styles.productHorizontalSectionListItemPercent}>*/}
                    {/*<Text style={styles.productHorizontalSectionListItemPercentText}>{item.percent}</Text>*/}
                    {/*</View>}*/}
                </View>
                <Text style={styles.productHorizontalSectionListItemName} numberOfLines={2}>{item.title}</Text>
                {(item.sale_price !== '') && <NumberFormat value={item.price} displayType={'text'} thousandSeparator={true} renderText={value =>
                    <Text style={styles.productHorizontalSectionListItemOldPrice}>{Helpers.ToPersianNumber(value)} تومان</Text>
                }/>}
                {(item.sale_price !== '0' && item.price !== '' && item.price !== '0') && <NumberFormat value={item.sale_price !== '' ? item.sale_price : item.price} displayType={'text'} thousandSeparator={true} renderText={value =>
                    <Text style={[styles.productHorizontalSectionListItemPrice,{marginTop:item.sale_price !== '' ? 0 : 0}]}>{Helpers.ToPersianNumber(value)} تومان</Text>
                }/>}
                {(item.price === '' || item.price === '0' || item.sale_price === '0') && <Text style={[styles.productHorizontalSectionListItemPrice,{marginTop:0}]}>رایگان</Text>}
            </TouchableOpacity>
        );
    }

    renderHeader(item,expanded) {
        if(this.state.categories.filter(a => a.parent === item.id).length > 0){
            return (
                <View style={[styles.drawerTitleRow,{borderBottomColor : expanded ? '#ffffff' : '#f4f7f9'}]}>
                    <Text style={expanded ? styles.drawerTitleRowTextExpanded : styles.drawerTitleRowText}>
                        {item.category_name}
                    </Text>
                    {expanded
                        ? <Icon style={styles.drawerTitleRowExpandedIcon} name="md-remove-circle" />
                        : <Icon style={styles.drawerTitleRowUnexpandedIcon} name="md-add-circle" />}
                </View>
            );
        }
        else {
            return (
                <TouchableOpacity style={[styles.drawerTitleRow,{borderBottomColor : expanded ? '#ffffff' : '#f4f7f9'}]}
                                  activeOpacity={.7} onPress={async() => { await this.setState({showModal:false});Actions.push('posts',{title:item.category_name,type:'category',categoryId:item.id}) } }>
                    <Text style={expanded ? styles.drawerTitleRowTextExpanded : styles.drawerTitleRowText}>
                        {item.category_name}
                    </Text>
                    {expanded
                        ? <Icon style={styles.drawerTitleRowExpandedIcon} name="md-remove-circle" />
                        : <Icon style={styles.drawerTitleRowUnexpandedIcon} name="md-add-circle" />}
                </TouchableOpacity>
            );
        }
    }
    renderContent(item) {
        return(
            <View style={styles.drawerContent}>
                <TouchableOpacity style={[styles.drawerContentRow,{borderBottomColor : '#ffffff' }]}
                                  activeOpacity={.7} onPress={async() => { await this.setState({showModal:false});Actions.push('posts',{title:item.category_name,type:'category',categoryId:item.id}) } }>
                    <Icon name='ios-arrow-back' style={styles.drawerContentIcon} />
                    <Text style={styles.drawerContentText}>{item.category_name}</Text>
                </TouchableOpacity>
                {this.state.categories.filter(a => a.parent === item.id).map((item2,index) => {
                    return(
                        <TouchableOpacity key={item2.id} style={[styles.drawerContentRow,{borderBottomColor : index + 1 === this.state.categories.filter(a => a.parent === item.id).length  ? '#ffffff' : '#f4f7f9' }]}
                                          activeOpacity={.7} onPress={async() => { await this.setState({showModal:false});Actions.push('posts',{title:item2.category_name,type:'category',categoryId:item2.id}) } }>
                            <Icon name='ios-arrow-back' style={styles.drawerContentIcon} />
                            <Text style={styles.drawerContentText}>{item2.category_name}</Text>
                        </TouchableOpacity>
                    );
                })}
            </View>
        )
    }

    render() {

        const dot = <View style={styles.dot} />;
        const activeDot = <View style={styles.activeDotContainer} >
            <View style={styles.activeDotInner} />
        </View>;

        let firstPost = this.state.posts.length > 0 ? this.state.posts[0] : null;
        let firstVideo = this.state.videos.length > 0 ? this.state.videos[0] : null;

        return (
            <Container style={styles.mainContainer}>
                <Image resizeMode='stretch' style={[styles.mainBackground,{height:1000}]} source={require("./../../assets/images/shared/menu-bg.png")} />
                <Header style={styles.header}>
                    <Left style={{flex:0.2,alignItems:'center',justifyContent:'center',flexDirection:'row'}}>
                        <BackButton/>
                        <BasketButton/>
                    </Left>
                    <Body style={{flex:1,alignItems:'flex-end',justifyContent:'center'}}>
                    {/*<Image style={styles.headerImage} source={require('./../../assets/images/splash/Splash-Logo.png')}/>*/}
                    <HeaderRightLabel title='آرامش'/>
                    </Body>
                    <Right style={{flex:.2,alignItems:'center',justifyContent:'center'}}>
                        <MenuButton/>
                    </Right>
                </Header>

                <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content" />

                <Content refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={this.onRefresh} tintColor='#ffffff'/>} >
                    <View style={styles.searchBoxContainer}>
                        <TextInput underlineColorAndroid="rgba(0,0,0,0)" style={styles.searchBoxInput}
                                   placeholderTextColor="#cccccc" onSubmitEditing={this.search} returnKeyType='search'
                                   placeholder='عبارت مورد نظر خود را تایپ کنید' ref={ref => this.searchBox = ref}/>

                        <TouchableOpacity style={styles.searchBoxButton} onPress={this.search}>
                            <Icon name="ios-search" style={styles.searchBoxIcon}/>
                        </TouchableOpacity>
                    </View>
                    {(!this.state.postLoading && this.state.posts.length > 0) && <View style={styles.blogContainer}>

                        <View style={styles.blogHeader}>
                            <TouchableOpacity style={styles.blogHeaderButton} activeOpacity={.8} onPress={() => this.setState({showModal:true})}>
                                <Text style={styles.blogHeaderButtonText}>
                                    دسته بندی
                                </Text>
                            </TouchableOpacity>
                            <Text style={styles.blogHeaderText}>
                                جدیدترین مقالات آموزشی
                            </Text>
                        </View>

                        <FlatList
                            data={this.state.posts}
                            renderItem={this.renderPostItem.bind(this)}
                            keyExtractor={(item) => item.id.toString()}
                            style={{backgroundColor:'transparent'}}
                            horizontal={true}
                            inverted={true}
                            showsHorizontalScrollIndicator={false}
                            removeClippedSubviews={true}
                        />

                        <TouchableOpacity activeOpacity={.7} style={styles.postMoreButton} onPress={() => Actions.push('posts' , {title:'مقالات',type:'category',categoryId:25})}>
                            {/*<Icon name='ios-arrow-back' style={{fontSize:20,color:'#393939',marginRight:10}}/>*/}
                            <Text style={styles.postMoreButtonText}>مشاهده همه مقالات آموزشی</Text>
                        </TouchableOpacity>

                    </View>}


                    {( !this.state.AdsLoading && this.state.Ads.length > 0 && this.state.Ads[0].position == '1') &&
                    <TouchableOpacity onPress={()=>{this.state.Ads[0].link !='' ? Linking.openURL(this.state.Ads[0].link) : Actions.push('product',{product:this.state.Ads[0].product})}} activeOpacity={.9} style={{backgroundColor:'#fff',padding:15,paddingTop:40,overflow:'hidden'}}>
                        <Image resizeMode='stretch' source={{uri:this.state.Ads[0].image_url}} style={[{width:Dimensions.get('window').width-30,height:120}]} />
                    </TouchableOpacity>
                    }
                    {(this.state.newProducts.length > 0 && !this.state.newProductsLoading) && <View style={styles.productHorizontalSection}>
                        <View style={styles.productHorizontalSectionHeader}>
                            <Text style={styles.productHorizontalSectionHeaderText}>جدیدترین محصولات</Text>
                            <TouchableOpacity activeOpacity={.7} style={{justifyContent:'center',alignItems:'center',flexDirection: 'row'}} onPress={() => Actions.push('products',{title:'جدیدترین محصولات',type:'category',categoryId:'48'})}>
                                <Icon name="ios-arrow-back" style={styles.productHorizontalSectionHeaderIcon}/>
                                <Text style={styles.productHorizontalSectionHeaderMoreText}>نمایش همه</Text>
                            </TouchableOpacity>
                        </View>
                        <FlatList
                            data={this.state.newProducts}
                            renderItem={this.renderItemNew.bind(this)}
                            keyExtractor={(item) => item.id.toString()}
                            style={styles.productHorizontalSectionList}
                            horizontal={true}
                            inverted={true}
                            showsHorizontalScrollIndicator={false}
                        />
                    </View>}
                    {(this.state.newProductsLoading) && <View style={styles.productHorizontalSection}>
                        <View style={styles.productHorizontalSectionHeader}>
                            <Text style={styles.productHorizontalSectionHeaderText}>جدیدترین محصولات</Text>
                            <TouchableOpacity activeOpacity={.7} style={{justifyContent:'center',alignItems:'center',flexDirection: 'row'}} onPress={() => Actions.push('products',{title:'جدیدترین محصولات',type:'category',categoryId:'48'})}>
                                <Icon name="ios-arrow-back" style={styles.productHorizontalSectionHeaderIcon}/>
                                <Text style={styles.productHorizontalSectionHeaderMoreText}>نمایش همه</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{height:150,justifyContent:'center',alignItems:'center'}}>
                            <ActivityIndicator color={this.props.global.grColorTwo} size='large'/>
                        </View>
                    </View>}
                    
                    {this.state.postLoading && <View style={{ height : Dimensions.get('window').height * .2 , backgroundColor:'#ffffff' , justifyContent:'center',alignItems:'center' }}>
                        <ActivityIndicator color={this.props.global.grColorTwo} size='large'/>
                    </View>}

                </Content>

                <Modal isVisible={this.state.showModal} backdropOpacity={0.5} animationIn="fadeIn" animationOut="fadeOut" backdropColor='#000000'
                       onBackdropPress={() => this.setState({showModal: false})} onBackButtonPress={() => this.setState({showModal: false})}>
                    <View style={styles.requestModalContainer} >
                        <TouchableOpacity onPress={() => this.setState({showModal: false})} style={{position: 'absolute', top: 10, left: 20, zIndex: 999999}}>
                            <Icon name="ios-close" style={{color: '#555555', fontSize: 50}}/>
                        </TouchableOpacity>
                        <Container style={{width:'100%',height:'100%'}}>
                            <View style={styles.drawerHeader}>
                                <Text style={styles.drawerHeaderText}>دسته بندی بلاگ</Text>
                            </View>
                            {(!this.state.categoryLoading && this.state.categories.length > 0) && <View style={{flex:1,backgroundColor:'#fff'}}>
                                <Content style={{backgroundColor:'transparent'}}>
                                    <Accordion
                                        dataArray={this.state.categories.filter(a => a.parent === 0)}
                                        renderHeader={this.renderHeader.bind(this)}
                                        renderContent={this.renderContent.bind(this)}
                                        style={{borderWidth:0}}
                                    />
                                </Content>
                            </View>}

                            {this.state.categoryLoading && <View style={{flex:1,backgroundColor:'#fff',justifyContent: 'center',alignItems: 'center'}}>
                                <ActivityIndicator color={this.props.global.grColorTwo} size='large'/>
                            </View>}

                            {(!this.state.categoryLoading && this.state.categories.length === 0) && <View style={{flex:1,backgroundColor:'#fff',justifyContent: 'center',alignItems: 'center'}}>
                                <Text style={styles.drawerTitleRowText}>دسته بندی وجود ندارد!</Text>
                            </View>}

                        </Container>
                    </View>
                </Modal>
            </Container>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setUser : user => {
            dispatch(setUser(user))
        }
    }
};

const mapStateToProps =(state) =>  {
    return{
        user: state.user,
        global:state.global
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(Aramesh);