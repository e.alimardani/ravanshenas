import React from 'react';
import {
    StatusBar,
    Image,
    TextInput,
    ActivityIndicator,
    Text,
    RefreshControl,
    TouchableOpacity,
    View,
    FlatList,
    Dimensions
} from 'react-native';
import {Header, Icon, Left, Right, Content, Container, Body} from 'native-base';
import styles from './Style';
import { Actions } from 'react-native-router-flux';
import {setUser} from "../../redux/Actions/Index";
import {connect} from "react-redux";
import MenuButton from "../Shared/MenuButton";
import BasketButton from "../Shared/BasketButton";
import HeaderRightLabel from "../Shared/HeaderRightLabel";
import Helpers from "../Shared/Helper";
import NumberFormat from "react-number-format";
import Swiper from "react-native-swiper";

class Product extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            sliders:[],
            sliderLoading:true,
            popularProducts:[],
            popularProductsLoading:true,
            onSaleProducts:[],
            onSaleProductsLoading:true,
            newProducts:[],
            newProductsLoading:true,
            refreshing: false,
        };

        this.newDate = props.global.newDate;

        //this.getSlidersRequest = this.getSlidersRequest.bind(this);
        this.getPopularProducts = this.getPopularProducts.bind(this);
        this.getOnSaleProducts = this.getOnSaleProducts.bind(this);
        this.getNewProducts = this.getNewProducts.bind(this);
        this.onRefresh = this.onRefresh.bind(this);

        this.search = this.search.bind(this);
    }

    async componentWillMount(){
        //await this.getSlidersRequest();
        await this.getPopularProducts();
        await this.getOnSaleProducts();
        await this.getNewProducts();
    }

    async getSlidersRequest(){
        if (await Helpers.CheckNetInfo()) {
            await this.setState({
                sliderLoading:false
            });
        }

        try {

            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);

            let response = await fetch(this.props.global.baseApiUrl + '/view/allsliders',
                {
                    method: "POST",
                    body: formData
                });

            //console.log(response);

            if (response.status !== 200) {
                await this.setState({
                    sliderLoading:false
                });
            }
            else {

                let json = await response.json();

                if(json.success){

                    await this.setState({
                        sliders:json.data,
                        sliderLoading:false
                    });
                }
                else {
                    await this.setState({
                        sliderLoading:false
                    });
                }
            }
        } catch (error) {
            await this.setState({
                sliderLoading:false
            });
        }
    }

    async getPopularProducts(){
        if (await Helpers.CheckNetInfo()) {
            await this.setState({
                popularProductsLoading:false
            });
        }

        try {

            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("perpage", '10');
            formData.append("offset", '1');
            formData.append("type", 'bestselling');

            let response = await fetch(this.props.global.baseApiUrl + '/product/all',
                {
                    method: "POST",
                    body: formData
                });

            //console.log(response);

            if (response.status !== 200) {
                await this.setState({
                    popularProductsLoading:false
                });
            }
            else {

                let json = await response.json();

                if(json.success){

                    await this.setState({
                        popularProducts:json.data,
                        popularProductsLoading:false
                    });
                }
                else {
                    await this.setState({
                        popularProductsLoading:false
                    });
                }
            }
        } catch (error) {
            await this.setState({
                popularProductsLoading:false
            });
        }
    }
    renderItemPopular({ item,index }) {
        return (
            <TouchableOpacity style={[styles.productHorizontalSectionListItem,{marginRight:index === 0 ? 15 : 15,marginLeft:index + 1 === this.state.popularProducts.length ? 15 : 0}]} key={item.id} activeOpacity={.8}
                              onPress={() => Actions.push('product',{product:item})}>
                <View style={styles.productHorizontalSectionListItemContainer}>
                    {item.image !== null && <Image resizeMode='contain' style={styles.productHorizontalSectionListItemImage} source={{uri:item.image + '?ref =' + this.newDate}}/>}
                    {item.image === null && <Image resizeMode='contain' style={styles.productHorizontalSectionListItemImage} source={require('./../../assets/images/shared/product.png')}/>}
                    {/*{item.oldPrice !== '' && <View style={styles.productHorizontalSectionListItemPercent}>*/}
                    {/*<Text style={styles.productHorizontalSectionListItemPercentText}>{item.percent}</Text>*/}
                    {/*</View>}*/}
                </View>
                <Text style={styles.productHorizontalSectionListItemName} numberOfLines={2}>{item.title}</Text>
                {(item.sale_price !== '') && <NumberFormat value={item.price} displayType={'text'} thousandSeparator={true} renderText={value =>
                    <Text style={styles.productHorizontalSectionListItemOldPrice}>{Helpers.ToPersianNumber(value)} تومان</Text>
                }/>}
                {(item.sale_price !== '0' && item.price !== '' && item.price !== '0') && <NumberFormat value={item.sale_price !== '' ? item.sale_price : item.price} displayType={'text'} thousandSeparator={true} renderText={value =>
                    <Text style={[styles.productHorizontalSectionListItemPrice,{marginTop:item.sale_price !== '' ? 0 : 0}]}>{Helpers.ToPersianNumber(value)} تومان</Text>
                }/>}
                {(item.price === '' || item.price === '0' || item.sale_price === '0') && <Text style={[styles.productHorizontalSectionListItemPrice,{marginTop:0}]}>رایگان</Text>}
            </TouchableOpacity>
        );
    }

    async getOnSaleProducts(){
        if (await Helpers.CheckNetInfo()) {
            await this.setState({
                onSaleProductsLoading:false
            });
        }

        try {

            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("perpage", '10');
            formData.append("offset", '1');
            formData.append("type", 'sale_price');

            let response = await fetch(this.props.global.baseApiUrl + '/product/all',
                {
                    method: "POST",
                    body: formData
                });

            //console.log(response);

            if (response.status !== 200) {
                await this.setState({
                    onSaleProductsLoading:false
                });
            }
            else {

                let json = await response.json();

                if(json.success){

                    await this.setState({
                        onSaleProducts:json.data,
                        onSaleProductsLoading:false
                    });
                }
                else {
                    await this.setState({
                        onSaleProductsLoading:false
                    });
                }
            }
        } catch (error) {
            await this.setState({
                onSaleProductsLoading:false
            });
        }
    }
    renderItemOnSale({ item,index }) {
        return (
            <TouchableOpacity style={[styles.productHorizontalSectionListItem,{marginRight:index === 0 ? 15 : 15,marginLeft:index + 1 === this.state.onSaleProducts.length ? 15 : 0}]} key={item.id} activeOpacity={.8}
                              onPress={() => Actions.push('product',{product:item})}>
                <View style={styles.productHorizontalSectionListItemContainer}>
                    {item.image !== null && <Image resizeMode='contain' style={styles.productHorizontalSectionListItemImage} source={{uri:item.image + '?ref =' + this.newDate}}/>}
                    {item.image === null && <Image resizeMode='contain' style={styles.productHorizontalSectionListItemImage} source={require('./../../assets/images/shared/product.png')}/>}
                    {/*{item.oldPrice !== '' && <View style={styles.productHorizontalSectionListItemPercent}>*/}
                    {/*<Text style={styles.productHorizontalSectionListItemPercentText}>{item.percent}</Text>*/}
                    {/*</View>}*/}
                </View>
                <Text style={styles.productHorizontalSectionListItemName} numberOfLines={2}>{item.title}</Text>
                {(item.sale_price !== '') && <NumberFormat value={item.price} displayType={'text'} thousandSeparator={true} renderText={value =>
                    <Text style={styles.productHorizontalSectionListItemOldPrice}>{Helpers.ToPersianNumber(value)} تومان</Text>
                }/>}
                {(item.sale_price !== '0' && item.price !== '' && item.price !== '0') && <NumberFormat value={item.sale_price !== '' ? item.sale_price : item.price} displayType={'text'} thousandSeparator={true} renderText={value =>
                    <Text style={[styles.productHorizontalSectionListItemPrice,{marginTop:item.sale_price !== '' ? 0 : 0}]}>{Helpers.ToPersianNumber(value)} تومان</Text>
                }/>}
                {(item.price === '' || item.price === '0' || item.sale_price === '0') && <Text style={[styles.productHorizontalSectionListItemPrice,{marginTop:0}]}>رایگان</Text>}
            </TouchableOpacity>
        );
    }

    async getNewProducts(){
        if (await Helpers.CheckNetInfo()) {
            await this.setState({
                newProductsLoading:false
            });
        }

        try {

            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("perpage", '10');
            formData.append("offset", '1');
            formData.append("type", 'newproduct');

            let response = await fetch(this.props.global.baseApiUrl + '/product/all',
                {
                    method: "POST",
                    body: formData
                });

            //console.log(response);

            if (response.status !== 200) {
                await this.setState({
                    newProductsLoading:false
                });
            }
            else {

                let json = await response.json();

                if(json.success){

                    await this.setState({
                        newProducts:json.data,
                        newProductsLoading:false
                    });
                }
                else {
                    await this.setState({
                        newProductsLoading:false
                    });
                }
            }
        } catch (error) {
            await this.setState({
                newProductsLoading:false
            });
        }
    }
    renderItemNew({ item,index }) {
        return (
            <TouchableOpacity style={[styles.productHorizontalSectionListItem,{marginRight:index === 0 ? 15 : 15,marginLeft:index + 1 === this.state.newProducts.length ? 15 : 0}]} key={item.id} activeOpacity={.8}
                              onPress={() => Actions.push('product',{product:item})}>
                <View style={styles.productHorizontalSectionListItemContainer}>
                    {item.image !== null && <Image resizeMode='contain' style={styles.productHorizontalSectionListItemImage} source={{uri:item.image + '?ref =' + this.newDate}}/>}
                    {item.image === null && <Image resizeMode='contain' style={styles.productHorizontalSectionListItemImage} source={require('./../../assets/images/shared/product.png')}/>}
                    {/*{item.oldPrice !== '' && <View style={styles.productHorizontalSectionListItemPercent}>*/}
                    {/*<Text style={styles.productHorizontalSectionListItemPercentText}>{item.percent}</Text>*/}
                    {/*</View>}*/}
                </View>
                <Text style={styles.productHorizontalSectionListItemName} numberOfLines={2}>{item.title}</Text>
                {(item.sale_price !== '') && <NumberFormat value={item.price} displayType={'text'} thousandSeparator={true} renderText={value =>
                    <Text style={styles.productHorizontalSectionListItemOldPrice}>{Helpers.ToPersianNumber(value)} تومان</Text>
                }/>}
                {(item.sale_price !== '0' && item.price !== '' && item.price !== '0') && <NumberFormat value={item.sale_price !== '' ? item.sale_price : item.price} displayType={'text'} thousandSeparator={true} renderText={value =>
                    <Text style={[styles.productHorizontalSectionListItemPrice,{marginTop:item.sale_price !== '' ? 0 : 0}]}>{Helpers.ToPersianNumber(value)} تومان</Text>
                }/>}
                {(item.price === '' || item.price === '0' || item.sale_price === '0') && <Text style={[styles.productHorizontalSectionListItemPrice,{marginTop:0}]}>رایگان</Text>}
            </TouchableOpacity>
        );
    }

    async onRefresh(){
        await this.setState({
            popularProducts:[],
            popularProductsLoading:true,
            onSaleProducts:[],
            onSaleProductsLoading:true,
            newProducts:[],
            newProductsLoading:true,
            refreshing: true
        });
        await this.getPopularProducts();
        await this.getOnSaleProducts();
        await this.getNewProducts();
        await this.setState({
            refreshing: false
        });
    }

    async search(){
        //console.log(this.searchBox._lastNativeText);
        this.searchBox.blur();
        Actions.push('productSearch',{query:this.searchBox._lastNativeText});
    }

    render() {

        const dot = <View style={styles.dot} />;
        const activeDot = <View style={styles.activeDotContainer} >
            <View style={styles.activeDotInner} />
        </View>;

        return (
            <Container style={styles.mainContainer}>
                <Image resizeMode='stretch' style={[styles.mainBackground,{height:1000}]} source={require("./../../assets/images/shared/menu-bg.png")} />
                <Header style={styles.header}>
                    <Left style={{flex:0.2,alignItems:'center',justifyContent:'center'}}>
                        <BasketButton/>
                    </Left>
                    <Body style={{flex:1,alignItems:'flex-end',justifyContent:'center'}}>
                    {/*<Image style={styles.headerImage} source={require('./../../assets/images/splash/Splash-Logo.png')}/>*/}
                    <HeaderRightLabel title='محصولات'/>
                    </Body>
                    <Right style={{flex:.2,alignItems:'center',justifyContent:'center'}}>
                        <MenuButton/>
                    </Right>
                </Header>

                <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content" />

                <Content refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={this.onRefresh} tintColor='#ffffff'/>} >
                    <View style={styles.searchBoxContainer}>
                        <TextInput underlineColorAndroid="rgba(0,0,0,0)" style={styles.searchBoxInput}
                                   placeholderTextColor="#cccccc" onSubmitEditing={this.search} returnKeyType='search'
                                   placeholder='عبارت مورد نظر خود را تایپ کنید' ref={ref => this.searchBox = ref}/>

                        <TouchableOpacity style={styles.searchBoxButton} onPress={this.search}>
                            <Icon name="ios-search" style={styles.searchBoxIcon}/>
                        </TouchableOpacity>
                    </View>
                    
                    {/*{(!this.state.sliderLoading && this.state.sliders.length > 0) && <Swiper style={styles.postSliderContainer} showsButtons={false} loop={false} activeDot={activeDot} dot={dot}*/}
                                                                                             {/*paginationStyle={{marginBottom: -20}}>*/}
                        {/*{this.state.sliders.map((item,index) => {*/}
                            {/*return(*/}
                                {/*<TouchableOpacity activeOpacity={.9} key={index} style={styles.postSliderSlide}>*/}
                                    {/*<Image style={styles.postSliderImage} source={{uri:item.post.image + '?ref =' + this.newDate}}/>*/}
                                    {/*/!*<View style={styles.postSliderTitleContainer}>*!/*/}
                                    {/*/!*<Text numberOfLines={1} style={styles.postSliderTitleText}>{item.post.title}</Text>*!/*/}
                                    {/*/!*</View>*!/*/}
                                {/*</TouchableOpacity>*/}
                            {/*);*/}
                        {/*})}*/}
                    {/*</Swiper>}*/}
                    {/*{this.state.sliderLoading && <View style={{ height : Dimensions.get('window').height * .30 , backgroundColor:'#ffffff' , justifyContent:'center',alignItems:'center' }}>*/}
                        {/*<ActivityIndicator color={this.props.global.grColorTwo} size='large'/>*/}
                    {/*</View>}*/}

                    {(this.state.popularProducts.length > 0 && !this.state.popularProductsLoading) && <View style={styles.productHorizontalSection}>
                        <View style={styles.productHorizontalSectionHeader}>
                            <Text style={styles.productHorizontalSectionHeaderText}>پرفروش‌ترین‌ها</Text>
                            <TouchableOpacity activeOpacity={.7} style={{justifyContent:'center',alignItems:'center',flexDirection: 'row'}} onPress={() => Actions.push('products',{title:'پرفروش ترین ها',type:'bestselling'})}>
                                <Icon name="ios-arrow-back" style={styles.productHorizontalSectionHeaderIcon}/>
                                <Text style={styles.productHorizontalSectionHeaderMoreText}>نمایش همه</Text>
                            </TouchableOpacity>
                        </View>
                        <FlatList
                            data={this.state.popularProducts}
                            renderItem={this.renderItemPopular.bind(this)}
                            keyExtractor={(item) => item.id.toString()}
                            style={styles.productHorizontalSectionList}
                            horizontal={true}
                            inverted={true}
                            showsHorizontalScrollIndicator={false}
                        />
                    </View>}
                    {(this.state.popularProductsLoading) && <View style={styles.productHorizontalSection}>
                        <View style={styles.productHorizontalSectionHeader}>
                            <Text style={styles.productHorizontalSectionHeaderText}>پرفروش‌ترین‌ها</Text>
                            <TouchableOpacity activeOpacity={.7} style={{justifyContent:'center',alignItems:'center',flexDirection: 'row'}} onPress={() => Actions.push('products',{title:'پرفروش ترین ها',type:'bestselling'})}>
                                <Icon name="ios-arrow-back" style={styles.productHorizontalSectionHeaderIcon}/>
                                <Text style={styles.productHorizontalSectionHeaderMoreText}>نمایش همه</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{height:150,justifyContent:'center',alignItems:'center'}}>
                            <ActivityIndicator color={this.props.global.grColorTwo} size='large'/>
                        </View>
                    </View>}

                    <TouchableOpacity activeOpacity={.9} onPress={() => Actions.drawerOpen()} style={{backgroundColor:'#fff',overflow:'hidden',justifyContent:'center',alignItems:'center'}}>
                        <Image resizeMode='cover' source={require('./../../assets/images/home/productBanner.jpg')} style={[styles.homeBanner,{width:Dimensions.get('window').width-30,resizeMode:'contain',borderRadius:30,height:120}]} />
                    </TouchableOpacity>

                    {(this.state.onSaleProducts.length > 0 && !this.state.onSaleProductsLoading) && <View style={[styles.productHorizontalSection,{backgroundColor:'#f4f6f9'}]}>
                        <View style={styles.productHorizontalSectionHeader}>
                            <Text style={styles.productHorizontalSectionHeaderText}>تخفیف ویژه</Text>
                            <TouchableOpacity activeOpacity={.7} style={{justifyContent:'center',alignItems:'center',flexDirection: 'row'}} onPress={() => Actions.push('products',{title:'تخفیف ویژه',type:'sale_price'})}>
                                <Icon name="ios-arrow-back" style={styles.productHorizontalSectionHeaderIcon}/>
                                <Text style={styles.productHorizontalSectionHeaderMoreText}>نمایش همه</Text>
                            </TouchableOpacity>
                        </View>
                        <FlatList
                            data={this.state.onSaleProducts}
                            renderItem={this.renderItemOnSale.bind(this)}
                            keyExtractor={(item) => item.id.toString()}
                            style={styles.productHorizontalSectionList}
                            horizontal={true}
                            inverted={true}
                            showsHorizontalScrollIndicator={false}
                        />
                    </View>}
                    {(this.state.onSaleProductsLoading) && <View style={[styles.productHorizontalSection,{backgroundColor:'#f4f6f9'}]}>
                        <View style={styles.productHorizontalSectionHeader}>
                            <Text style={styles.productHorizontalSectionHeaderText}>تخفیف ویژه</Text>
                            <TouchableOpacity activeOpacity={.7} style={{justifyContent:'center',alignItems:'center',flexDirection: 'row'}} onPress={() => Actions.push('products',{title:'تخفیف ویژه',type:'sale_price'})}>
                                <Icon name="ios-arrow-back" style={styles.productHorizontalSectionHeaderIcon}/>
                                <Text style={styles.productHorizontalSectionHeaderMoreText}>نمایش همه</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{height:150,justifyContent:'center',alignItems:'center'}}>
                            <ActivityIndicator color={this.props.global.grColorTwo} size='large'/>
                        </View>
                    </View>}

                    {(this.state.newProducts.length > 0 && !this.state.newProductsLoading) && <View style={styles.productHorizontalSection}>
                        <View style={styles.productHorizontalSectionHeader}>
                            <Text style={styles.productHorizontalSectionHeaderText}>جدیدترین محصولات</Text>
                            <TouchableOpacity activeOpacity={.7} style={{justifyContent:'center',alignItems:'center',flexDirection: 'row'}} onPress={() => Actions.push('products',{title:'جدیدترین محصولات',type:'newproduct'})}>
                                <Icon name="ios-arrow-back" style={styles.productHorizontalSectionHeaderIcon}/>
                                <Text style={styles.productHorizontalSectionHeaderMoreText}>نمایش همه</Text>
                            </TouchableOpacity>
                        </View>
                        <FlatList
                            data={this.state.newProducts}
                            renderItem={this.renderItemNew.bind(this)}
                            keyExtractor={(item) => item.id.toString()}
                            style={styles.productHorizontalSectionList}
                            horizontal={true}
                            inverted={true}
                            showsHorizontalScrollIndicator={false}
                        />
                    </View>}
                    {(this.state.newProductsLoading) && <View style={styles.productHorizontalSection}>
                        <View style={styles.productHorizontalSectionHeader}>
                            <Text style={styles.productHorizontalSectionHeaderText}>جدیدترین محصولات</Text>
                            <TouchableOpacity activeOpacity={.7} style={{justifyContent:'center',alignItems:'center',flexDirection: 'row'}} onPress={() => Actions.push('products',{title:'جدیدترین محصولات',type:'newproduct'})}>
                                <Icon name="ios-arrow-back" style={styles.productHorizontalSectionHeaderIcon}/>
                                <Text style={styles.productHorizontalSectionHeaderMoreText}>نمایش همه</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{height:150,justifyContent:'center',alignItems:'center'}}>
                            <ActivityIndicator color={this.props.global.grColorTwo} size='large'/>
                        </View>
                    </View>}

                </Content>
            </Container>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setUser : user => {
            dispatch(setUser(user))
        }
    }
};

const mapStateToProps =(state) =>  {
    return{
        user: state.user,
        global:state.global
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(Product);