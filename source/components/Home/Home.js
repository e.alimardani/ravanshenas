import React from 'react';
import {View,AsyncStorage} from 'react-native';
import {setApp} from "../../redux/Actions/Index";
import {connect} from "react-redux";
import HomeTabBar from "./HomeTabBar";
import Helpers from "../Shared/Helper";
//import PushNotificationController from "../Shared/PushNotificationController";


class Home extends React.Component {

    constructor(props) {
        super(props);
        this.getFavoriteProductsRequest = this.getFavoriteProductsRequest.bind(this);
    }

    async componentWillMount(){
        if(await AsyncStorage.getItem('cart') !== null){
            let cart = await AsyncStorage.getItem("cart");
            cart = JSON.parse(cart);

            await this.props.setApp({
                ...this.props.app,
                cart
            });
        }
        await this.getFavoriteProductsRequest();
    }

    async getFavoriteProductsRequest(){
        //console.log(this.state);
        if(await Helpers.CheckNetInfo()){
            return;
        }

        try {
            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("usertoken", this.props.user.apiToken);

            let response = await fetch(this.props.global.baseApiUrl + '/product/favpget',
                {
                    method: "POST",
                    body: formData
                });

            if (response.status !== 200) {
                // await this.setState({
                //     loading:false,
                //     products:false
                // });
            }
            else {

                let json = await response.json();

                if(json.success){

                    let favorite = [];

                    json.data.map(item => {
                       item.id = parseInt(item.id);
                       favorite.push(item);
                    });

                    await this.props.setApp({
                        ...this.props.app,
                        favorite:favorite
                    });
                }
                // else {
                //     await this.setState({
                //         loading:false
                //     });
                // }
            }

        } catch (error) {
            console.log(error);
            // await this.setState({
            //     loading: false
            // })
        }
    }

    render() {
        return (
            <View style={{flex:1}}>
                {/*<PushNotificationController/>*/}
                <HomeTabBar/>
            </View>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setApp : app => {
            dispatch(setApp(app))
        }
    }
};

const mapStateToProps =(state) =>  {
    return{
        user: state.user,
        global:state.global,
        app:state.app
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(Home);