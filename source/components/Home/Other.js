import React from 'react';
import {StatusBar, Image, TextInput, Switch,Text, Dimensions,TouchableOpacity,View,ScrollView,Share} from 'react-native';
import {Header, Icon, Left, Right, Content, Container, Body} from 'native-base';
import styles from './Style';
import { Actions } from 'react-native-router-flux';
import {setUser} from "../../redux/Actions/Index";
import {connect} from "react-redux";
import MenuButton from "../Shared/MenuButton";
import BasketButton from "../Shared/BasketButton";
import OtherInfoIcon from "../../assets/icon/OtherInfoIcon";
import OtherQuestionIcon from "../../assets/icon/OtherQuestionIcon";
import OtherTermsIcon from "../../assets/icon/OtherTermsIcon";
import OtherRefrenceIcon from "../../assets/icon/OtherRefrenceIcon";
import OtherContactUs from "../../assets/icon/OtherContactUs";
import OtherPasswordIcon from "../../assets/icon/OtherPasswordIcon";
import OtherNotificationIcon from "../../assets/icon/OtherNotificationIcon";
import OtherShareIcon from "../../assets/icon/OtherShareIcon";
import LoadingModal from "../Shared/LoadingModal";
import Helpers from "../Shared/Helper";
import HeaderRightLabel from "../Shared/HeaderRightLabel";

class Other extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            notification : false,
            showLoadingModal:false
        };

        this.toggleSwitch = this.toggleSwitch.bind(this);
        this.search = this.search.bind(this);
        this.getLink = this.getLink.bind(this);

        this.share = this.share.bind(this);
    }

    toggleSwitch(value){
        this.setState({notification: value})
    }

    async search(){
        //console.log(this.searchBox._lastNativeText);
        this.searchBox.blur();
        Actions.push('productSearch',{query:this.searchBox._lastNativeText});
    }

    async getLink(name){

        await this.setState({
            showLoadingModal:true
        });

        try {
            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("page", name);

            let response = await fetch(this.props.global.baseApiUrl + '/view/getpageurl',
                {
                    method: "POST",
                    body: formData
                });

            if (response.status !== 200) {
                await this.setState({
                    showLoadingModal:false
                });
            }
            else {

                let json = await response.json();

                if(json.success){
                    await this.setState({
                        showLoadingModal:false
                    });
                    Actions.push('browser',{url:json.data,title:name})
                }
                else {
                    await this.setState({
                        showLoadingModal:false
                    });
                }
            }

        } catch (error) {
            await this.setState({
                showLoadingModal:false
            });
        }
    }

    async share() {
        if (await Helpers.CheckNetInfo()) {
            return;
        }

        let inviteText = 'سلام\nاپلیکیشن "' + this.props.global.appName + '" رو از لینک زیر دانلود کن \n\n' + this.props.global.website;

        Share.share({
            message: inviteText,
            title: "به اشتراک گذاری " + this.props.global.appName
        }, {dialogTitle: "به اشتراک گذاری " + this.props.global.appName});

    }

    render() {

        return (
            <Container style={styles.mainContainer}>
                <Image resizeMode='stretch' style={[styles.mainBackground,{height:1000}]} source={require("./../../assets/images/shared/menu-bg.png")} />

                <Header style={styles.header}>
                    <Left style={{flex:0.2,alignItems:'center',justifyContent:'center'}}>
                        <BasketButton/>
                    </Left>
                    <Body style={{flex:1,alignItems:'flex-end',justifyContent:'center'}}>
                        <HeaderRightLabel title='سایر'/>
                        {/*<Image style={styles.headerImage} source={require('./../../assets/images/splash/Splash-Logo.png')}/>*/}
                    </Body>
                    <Right style={{flex:.2,alignItems:'center',justifyContent:'center'}}>
                        <MenuButton/>
                    </Right>
                </Header>

                <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content" />

                <Content>
                    <View style={styles.searchBoxContainer}>
                        <TextInput underlineColorAndroid="rgba(0,0,0,0)" style={styles.searchBoxInput}
                                   placeholderTextColor="#cccccc" onSubmitEditing={this.search} returnKeyType='search'
                                   placeholder='عبارت مورد نظر خود را تایپ کنید' ref={ref => this.searchBox = ref}/>

                        <TouchableOpacity style={styles.searchBoxButton} onPress={this.search}>
                            <Icon name="ios-search" style={styles.searchBoxIcon}/>
                        </TouchableOpacity>
                    </View>
                    <ScrollView style={{minHeight: Dimensions.get('window').height - 170,backgroundColor:'#f4f7f9'}}>
                        <View style={[styles.profileBottomSection,{marginTop:30}]}>
                            <TouchableOpacity style={styles.profileBottomSectionRow} activeOpacity={.7} onPress={() => Actions.push('aboutUs')}>
                                <Icon name='ios-arrow-back' style={styles.profileBottomSectionRowIcon}/>
                                <View style={styles.profileBottomSectionRowRight}>
                                    <Text style={styles.profileBottomSectionRowText}>درباره ما</Text>
                                    <OtherInfoIcon/>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.profileBottomSectionRow} activeOpacity={.7} onPress={() => this.getLink('سوالات متداول')}>
                                <Icon name='ios-arrow-back' style={styles.profileBottomSectionRowIcon}/>
                                <View style={styles.profileBottomSectionRowRight}>
                                    <Text style={styles.profileBottomSectionRowText}>سوالات متداول</Text>
                                    <OtherQuestionIcon/>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.profileBottomSectionRow} activeOpacity={.7} onPress={() => this.getLink('شرایط استفاده')}>
                                <Icon name='ios-arrow-back' style={styles.profileBottomSectionRowIcon}/>
                                <View style={styles.profileBottomSectionRowRight}>
                                    <Text style={styles.profileBottomSectionRowText}>شرایط استفاده</Text>
                                    <OtherTermsIcon/>
                                </View>
                            </TouchableOpacity>
                            {/*<TouchableOpacity style={styles.profileBottomSectionRow} activeOpacity={.7}>*/}
                                {/*<Icon name='ios-arrow-back' style={styles.profileBottomSectionRowIcon}/>*/}
                                {/*<View style={styles.profileBottomSectionRowRight}>*/}
                                    {/*<Text style={styles.profileBottomSectionRowText}>رهگیری سفارش</Text>*/}
                                    {/*<OtherRefrenceIcon/>*/}
                                {/*</View>*/}
                            {/*</TouchableOpacity>*/}
                            <TouchableOpacity style={[styles.profileBottomSectionRow,{borderBottomWidth:0}]} activeOpacity={.7} onPress={() => Actions.push('contact')}>
                                <Icon name='ios-arrow-back' style={styles.profileBottomSectionRowIcon}/>
                                <View style={styles.profileBottomSectionRowRight}>
                                    <Text style={styles.profileBottomSectionRowText}>تماس با ما</Text>
                                    <OtherContactUs/>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={[styles.profileBottomSection,{marginTop:5}]}>
                            {/*<TouchableOpacity style={styles.profileBottomSectionRow} activeOpacity={.7}>*/}
                                {/*<Icon name='ios-arrow-back' style={styles.profileBottomSectionRowIcon}/>*/}
                                {/*<View style={styles.profileBottomSectionRowRight}>*/}
                                    {/*<Text style={styles.profileBottomSectionRowText}>تغییر کلمه عبور</Text>*/}
                                    {/*<OtherPasswordIcon/>*/}
                                {/*</View>*/}
                            {/*</TouchableOpacity>*/}
                            {/*<View style={styles.profileBottomSectionRow} activeOpacity={.7}>*/}
                                {/*/!*<Icon name='ios-arrow-back' style={styles.profileBottomSectionRowIcon}/>*!/*/}
                                {/*<Switch value={this.state.notification} onValueChange ={this.toggleSwitch} style={{paddingVertical: 0}} />*/}
                                {/*<View style={styles.profileBottomSectionRowRight}>*/}
                                    {/*<Text style={styles.profileBottomSectionRowText}>اعلان ها</Text>*/}
                                    {/*<OtherNotificationIcon/>*/}
                                {/*</View>*/}
                            {/*</View>*/}
                            <TouchableOpacity style={[styles.profileBottomSectionRow,{borderBottomWidth:0}]} activeOpacity={.7} onPress={this.share}>
                                <Icon name='ios-arrow-back' style={styles.profileBottomSectionRowIcon}/>
                                <View style={styles.profileBottomSectionRowRight}>
                                    <Text style={styles.profileBottomSectionRowText}>به اشتراک بگذارید</Text>
                                    <OtherShareIcon/>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                </Content>

                <LoadingModal show={this.state.showLoadingModal} />
            </Container>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setUser : user => {
            dispatch(setUser(user))
        }
    }
};

const mapStateToProps =(state) =>  {
    return{
        user: state.user,
        global:state.global
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(Other);