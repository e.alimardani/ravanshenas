import React from 'react';
import { createBottomTabNavigator , createAppContainer} from 'react-navigation';
import EStyleSheet from 'react-native-extended-stylesheet';
import styles from './Style';
import Blog from "./Blog";

import HomeTabIcon from "../../assets/icon/HomeTabIcon";
import ProductTabIcon from "../../assets/icon/ProductTabIcon";
import ProfileTabIcon from "../../assets/icon/ProfileTabIcon";
import FavoriteTabIcon from "../../assets/icon/FavoriteTabIcon";
import OtherTabIcon from "../../assets/icon/OtherTabIcon";
import ProductsTabIcon from "../../assets/icon/ProductsTabIcon";
import {PixelRatio, Platform} from "react-native";
import Product from "./Product";
import Profile from "./Profile";
import Favorite from "./Favorite";
import Other from "./Other";
import Products from "../Product/Products";


const HomeTabBar = createBottomTabNavigator({
        Blog: {
            screen: props => <Blog />,
            navigationOptions: {
                tabBarLabel: 'خانه',
                tabBarIcon: ({tintColor}) => <HomeTabIcon color={tintColor}/>
            }
        },
        Product: {
            screen: props => <Product />,
            navigationOptions: {
                tabBarLabel: 'محصولات',
                tabBarIcon: ({tintColor}) => <ProductTabIcon color={tintColor}/>
            }
        },
        // Products: {
        //     screen: props => <Products />,
        //     navigationOptions: {
        //         tabBarLabel: 'محصولات',
        //         tabBarIcon: ({tintColor}) => <ProductsTabIcon color={tintColor}/>
        //     }
        // },
        Profile: {
            screen: props => <Profile />,
            navigationOptions: {
                tabBarLabel: 'پروفایل',
                tabBarIcon: ({tintColor}) => <ProfileTabIcon color={tintColor}/>
            }
        },
        Favorite: {
            screen: props => <Favorite />,
            navigationOptions: {
                tabBarLabel: 'علاقمندی ها',
                tabBarIcon: ({tintColor}) => <FavoriteTabIcon color={tintColor}/>
            }
        },
        Other: {
            screen: props => <Other />,
            navigationOptions: {
                tabBarLabel: 'سایر',
                tabBarIcon: ({tintColor}) => <OtherTabIcon color={tintColor}/>
            }
        },
    },
    {
        tabBarOptions: {
            activeTintColor: '#3b43ec' ,
            //activeTintColor: '#e82d62' ,
            inactiveTintColor: '#565D64',
            style: {
                borderBottomWidth:0,
                borderTopWidth:1/PixelRatio.get(),
                borderTopColor:'#dddddd',
                paddingVertical:2,
                height:60,
                backgroundColor:'#ffffff'
            },
            labelStyle:EStyleSheet.create({
                textAlign:'center',
                writingDirection:'rtl',
                fontSize:10,
                ...Platform.select({
                    ios: {
                        fontFamily:() => EStyleSheet.value('$YekanRegular'),
                        fontWeight: 'normal',
                        //paddingTop:10
                    },
                    android: {
                        fontFamily: () => EStyleSheet.value('$YekanRegular'),
                    }
                })
            })
        },
        order:['Other','Favorite','Profile','Product','Blog'],
        initialRouteName:'Blog',
        backBehavior:'none'
    });

export default createAppContainer(HomeTabBar);
