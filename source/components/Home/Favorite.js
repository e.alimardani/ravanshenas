import React from 'react';
import {
    StatusBar,
    Image,
    TextInput,
    ActivityIndicator,
    Text,
    Platform,
    TouchableOpacity,
    View,
    FlatList,
    Dimensions, ScrollView
} from 'react-native';
import {Header, Icon, Left, Right, Content, Container, Body} from 'native-base';
import styles from './Style';
import { Actions } from 'react-native-router-flux';
import {setApp} from "../../redux/Actions/Index";
import {connect} from "react-redux";
import MenuButton from "../Shared/MenuButton";
import BasketButton from "../Shared/BasketButton";
import HeaderRightLabel from "../Shared/HeaderRightLabel";
import Helpers from "../Shared/Helper";
import LoadingModal from "../Shared/LoadingModal";
import LinearGradient from "react-native-linear-gradient";

class Favorite extends React.Component {

    constructor(props) {
        super(props);

        this.newDate = props.global.newDate;

        this.state = {
            products:[],
            showLoadingModal:false,
            editable:false
        };

        this.deleteFavorite = this.deleteFavorite.bind(this);
        this.search = this.search.bind(this);
    }

    async deleteFavorite(id){
        await this.setState({
            showLoadingModal:true
        });

        try {
            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("usertoken", this.props.user.apiToken);
            formData.append("product_id", id);

            let response = await fetch(this.props.global.baseApiUrl + '/product/favpdel',
                {
                    method: "POST",
                    body: formData
                });

            if (response.status !== 200) {
                await this.setState({
                    showLoadingModal:false
                });
            }
            else {

                let json = await response.json();

                if(json.success){

                    if(json.data === "It's deleted")
                    {
                        await this.props.setApp({
                            ...this.props.app,
                            favorite:[]
                        });
                    }
                    else {
                        await this.props.setApp({
                            ...this.props.app,
                            favorite:json.data
                        });
                    }
                    await this.setState({
                        showLoadingModal:false
                    });
                }
                else {
                    await this.setState({
                        showLoadingModal:false
                    });
                }
            }

        } catch (error) {
            //console.log(error);
            await this.setState({
                showLoadingModal:false
            });
        }
    }

    handleEmpty() {
        return(
            <View style={{height:150,width:Dimensions.get('window').width,justifyContent:'center',alignItems:'center'}}>
                <Text style={[styles.notFoundText,{marginTop:0}]}>کالای مورد علاقه ای یافت نشد.</Text>
            </View>
        );
    }

    renderItem({ item,index }) {
        return (
            <TouchableOpacity style={[styles.favoriteHorizontalSectionListItem,{marginRight:index === 0 ? 15 : 5,marginLeft:index + 1 === this.props.app.favorite.length ? 15 : 0}]} key={item.id}
                              onPress={() => Actions.push('product',{product:item})} activeOpacity={.8}>
                <View style={styles.favoriteHorizontalSectionListItemContainer}>
                    {item.image !== null && <Image resizeMode='contain' style={styles.favoriteHorizontalSectionListItemImage} source={{uri:item.image + '?ref =' + this.newDate}}/>}
                    {item.image === null && <Image resizeMode='contain' style={styles.favoriteHorizontalSectionListItemImage} source={require('./../../assets/images/shared/product.png')}/>}
                    <Text style={styles.favoriteHorizontalSectionListItemName} numberOfLines={2}>{item.title}</Text>
                </View>
                {this.state.editable && <TouchableOpacity style={styles.favoriteDeleteButton} hitSlop={{top: 10, left: 10, bottom: 10, right: 10}}
                                  onPress={() => this.deleteFavorite(item.id)} activeOpacity={.8}>
                    <Text style={styles.favoriteDeleteButtonText}>حذف</Text>
                </TouchableOpacity>}
            </TouchableOpacity>
        );
    }

    async search(){
        //console.log(this.searchBox._lastNativeText);
        this.searchBox.blur();
        Actions.push('productSearch',{query:this.searchBox._lastNativeText});
    }

    render() {

        return (
            <Container style={styles.mainContainer}>
                <Image resizeMode='stretch' style={[styles.mainBackground,{height:1000}]} source={require("./../../assets/images/shared/menu-bg.png")} />

                <Header style={styles.header}>
                    <Left style={{flex:0.2,alignItems:'center',justifyContent:'center'}}>
                        <BasketButton/>
                    </Left>
                    <Body style={{flex:1,alignItems:'flex-end',justifyContent:'center'}}>
                        <HeaderRightLabel title='علاقمندی ها'/>
                    </Body>
                    <Right style={{flex:.2,alignItems:'center',justifyContent:'center'}}>
                        <MenuButton/>
                    </Right>
                </Header>

                <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content" />

                <Content>
                    <View style={styles.searchBoxContainer}>
                        <TextInput underlineColorAndroid="rgba(0,0,0,0)" style={styles.searchBoxInput}
                                   placeholderTextColor="#cccccc" onSubmitEditing={this.search} returnKeyType='search'
                                   placeholder='عبارت مورد نظر خود را تایپ کنید' ref={ref => this.searchBox = ref}/>

                        <TouchableOpacity style={styles.searchBoxButton} onPress={this.search}>
                            <Icon name="ios-search" style={styles.searchBoxIcon}/>
                        </TouchableOpacity>
                    </View>
                    <ScrollView style={{minHeight: Dimensions.get('window').height - 170,backgroundColor:'#f4f7f9'}}>
                        {this.props.user.apiToken !== null && <View style={[styles.productHorizontalSectionHeader,{paddingVertical:30}]}>
                            <Text style={styles.productHorizontalSectionHeaderText}>علاقمندی ها</Text>
                            {!this.state.editable && <TouchableOpacity activeOpacity={.7} style={styles.favoriteEditButton} onPress={() => this.setState({editable:true})}>
                                <Text style={[styles.productHorizontalSectionHeaderMoreText,{fontSize:12,color:'#393939'}]}>ویرایش</Text>
                            </TouchableOpacity>}
                            {this.state.editable && <TouchableOpacity activeOpacity={.7} style={styles.favoriteEditButton} onPress={() => this.setState({editable:false})}>
                                <Text style={[styles.productHorizontalSectionHeaderMoreText,{fontSize:12,color:'#393939'}]}>اتمام ویرایش</Text>
                            </TouchableOpacity>}
                        </View>}
                        {this.props.user.apiToken !== null && <View style={[styles.productHorizontalSection,{paddingVertical:20}]}>
                            <FlatList
                                data={this.props.app.favorite}
                                ListEmptyComponent={this.handleEmpty.bind(this)}
                                renderItem={this.renderItem.bind(this)}
                                keyExtractor={(item) => item.id.toString()}
                                style={styles.favoriteHorizontalSectionList}
                                horizontal={true}
                                inverted={this.props.app.favorite.length > 0}
                                showsHorizontalScrollIndicator={false}
                            />
                        </View>}
                        {this.props.user.apiToken === null && <View style={styles.splashBottomSection}>
                            <TouchableOpacity style={styles.loginColorGrButton} activeOpacity={0.8} onPress={() => Actions.push('intro',{initialIndex:1})}>
                                <LinearGradient colors={[this.props.global.grColorOne,this.props.global.grColorTwo]} style={styles.loginGrButtonGradient} start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}>
                                    <Text style={styles.loginGrButtonText}>ورود</Text>
                                </LinearGradient>
                            </TouchableOpacity>

                            <TouchableOpacity style={{marginTop: 30}} activeOpacity={0.8} onPress={() => Actions.push('intro',{initialIndex:0})}>
                                <Text style={styles.buttonTransparentText}>حساب کاربری ندارید؟ عضو شوید.</Text>
                            </TouchableOpacity>
                        </View>}
                    </ScrollView>
                </Content>
                <LoadingModal show={this.state.showLoadingModal} />
            </Container>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setApp : app => {
            dispatch(setApp(app))
        }
    }
};

const mapStateToProps =(state) =>  {
    return{
        user: state.user,
        global:state.global,
        app:state.app
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(Favorite);