import EStyleSheet from 'react-native-extended-stylesheet';
import {Platform,Dimensions,PixelRatio,StatusBar} from "react-native";

export const styles = EStyleSheet.create({
    mainContainer : {
        backgroundColor:'$MainColor'
    },
    mainBackground:{
        position: 'absolute',
        width:Dimensions.get('window').width,
        height:Dimensions.get('window').height,
        //opacity:0.7
    },
    header : {
        backgroundColor : 'transparent',
        borderBottomWidth:0,
        padding:0,
        elevation: 0,
        ...Platform.select({
            ios: {

            },
            android: {
                marginTop:StatusBar.currentHeight
            }
        }),
    },
    headerImage:{
        ...Platform.select({
            ios: {
                width:60
            },
            android: {
                width:70
            }
        }),
        height:40,
        resizeMode:'contain'
    },
    searchBoxContainer:{
        width:Dimensions.get('window').width,
        height: 60,
        //borderBottomWidth: 2/PixelRatio.get(),
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 10
    },
    searchBoxInput:{
        width:'90%',
        textAlign:'right',
        writingDirection:'rtl',
        fontSize:12,
        //lineHeight:14,
        color:'#444444' ,
        paddingVertical:2,
        paddingHorizontal:10,
        paddingLeft:40,
        height:45,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'normal',
                //paddingTop:10
            },
            android: {
                fontFamily: '$YekanRegular'
            }
        }),
        backgroundColor:'#ffffff',
        borderRadius:10
    },
    searchBoxButton:{
        position: 'absolute',
        left:'7%'
    },
    searchBoxIcon:{
        fontSize: 30,
        color: '#000000'
    },
    mainTabStyle:{
        borderBottomWidth:0,
        borderTopWidth:1/PixelRatio.get(),
        borderTopColor:'#dddddd',
        paddingVertical:2,
        height:60,
        backgroundColor:'#ffffff'
    },
    tabLabelStyle:{
        textAlign:'center',
        writingDirection:'rtl',
        fontSize:10,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'normal',
                //paddingTop:10
            },
            android: {
                fontFamily: '$YekanRegular'
            }
        }),
    },
    productHorizontalSection:{
        backgroundColor:'#ffffff',
        paddingVertical:25
    },
    productHorizontalSectionHeader:{
        flexDirection: 'row-reverse',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 20
    },
    productHorizontalSectionHeaderText:{
        textAlign:'right',
        color:'#393939',
        writingDirection:'rtl',
        fontSize:14,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold',
                //paddingTop:10
            },
            android: {
                fontFamily: '$YekanBold'
            }
        }),
    },
    productHorizontalSectionHeaderMoreText:{
        textAlign:'center',
        color:'#0082f0',
        writingDirection:'rtl',
        fontSize:12,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'normal',
                //paddingTop:10
            },
            android: {
                fontFamily: '$YekanRegular'
            }
        }),
    },
    productHorizontalSectionHeaderIcon:{
        fontSize:20,
        color:'#0082f0',
        marginRight: 7
    },
    productHorizontalSectionList:{
        backgroundColor:'transparent',
        width:Dimensions.get('window').width,
        marginTop: 25
    },
    productHorizontalSectionListItem:{
        width:Dimensions.get('window').width / 3.2,
        //backgroundColor:'#cccccc',
        marginRight: 15
    },
    productHorizontalSectionListItemContainer:{
        padding:5,
        margin:2,
        backgroundColor:'#ffffff',
        borderRadius: 10,
        ...Platform.select({
            ios: {
                shadowColor: 'rgb(0, 0, 0)',
                shadowOffset: { width: 0, height: 0 },
                shadowOpacity: 0.05,
                shadowRadius: 3,
            },
            android: {
                elevation: 1
            }
        }),
        borderWidth:2/PixelRatio.get(),
        borderColor:'#eceff1',
    },
    productHorizontalSectionListItemImage:{
        width:'100%',
        height:Dimensions.get('window').width / 3.4,
        resizeMode: 'contain'
    },
    productHorizontalSectionListItemName:{
        textAlign:'center',
        color:'#333333',
        writingDirection:'rtl',
        fontSize:10,
        lineHeight:20,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold',
                //paddingTop:10
            },
            android: {
                fontFamily: '$YekanBold'
            }
        }),
        marginTop:10
    },
    productHorizontalSectionListItemPrice:{
        textAlign:'center',
        color:'#2fcc71',
        writingDirection:'rtl',
        fontSize:10,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold',
                //paddingTop:10
            },
            android: {
                fontFamily: '$YekanBold'
            }
        }),
        marginTop:0
    },
    productHorizontalSectionListItemOldPrice: {
        textAlign: 'center',
        color: '#db1623',
        writingDirection: 'rtl',
        fontSize: 10,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold',
                //paddingTop:10
            },
            android: {
                fontFamily: '$YekanBold'
            }
        }),
        marginTop: 0,
        textDecorationLine:'line-through'
    },
    productHorizontalSectionListItemPercent:{
        position:'absolute',
        top:10,
        backgroundColor:'$MainColor',
        left: 0,
        width:35,
        height:25,
        borderTopRightRadius:12.5,
        borderBottomRightRadius:12.5,
        justifyContent:'center',
        alignItems:'center'
    },
    productHorizontalSectionListItemPercentText:{
        textAlign: 'center',
        color: '#ffffff',
        writingDirection: 'rtl',
        fontSize: 12,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'normal',
                //paddingTop:10
            },
            android: {
                fontFamily: '$YekanRegular'
            }
        }),
    },
    homeBanner:{
        width:Dimensions.get('window').width,
        height:120
    },
    profileTopSection:{
        //backgroundColor:'#800',
        paddingVertical:20,
        justifyContent:'center',
        alignItems:'center'
    },
    profileThumbContainer:{
        width:100,
        height:100,
        backgroundColor:'#d4dce1',
        borderRadius:50,
        ...Platform.select({
            ios: {
                shadowColor: 'rgb(0, 0, 0)',
                shadowOffset: { width: 0, height: 0 },
                shadowOpacity: 0.2,
                shadowRadius: 5,
            },
            android: {
                elevation: 3
            }
        })
    },
    profileName:{
        textAlign:'center',
        writingDirection:'rtl',
        fontSize:20,
        color:'#393939',
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold',
                //paddingTop:3
            },
            android: {
                fontFamily: '$YekanBold'
            }
        }),
        backgroundColor:'transparent',
        marginTop:10
    },
    profileNumber:{
        textAlign:'center',
        writingDirection:'rtl',
        fontSize:16,
        color:'$MainColor',
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'normal',
                //paddingTop:3
            },
            android: {
                fontFamily: '$YekanRegular'
            }
        }),
        backgroundColor:'transparent',
        marginTop:5
    },
    profileBottomSection:{
        marginHorizontal: 20,
        borderWidth: 5/PixelRatio.get(),
        borderColor:'#eceff1',
        backgroundColor:'#ffffff',
        borderRadius:20,
        marginBottom: 20,
        paddingVertical:5
    },
    profileBottomSectionRow:{
        flexDirection: 'row',
        justifyContent:'space-between',
        alignItems:'center',
        paddingVertical:10,
        borderBottomWidth:5/PixelRatio.get(),
        borderBottomColor:'#eceff1',
        marginHorizontal: 15
    },
    profileBottomSectionRowIcon:{
        fontSize:18,
        color:'#b7c4cb'
    },
    profileBottomSectionRowRight:{
        flexDirection:'row',
        alignItems:'center'
    },
    profileBottomSectionRowText:{
        textAlign:'right',
        writingDirection:'rtl',
        fontSize:12,
        color:'#393939',
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'normal',
                //paddingTop:3
            },
            android: {
                fontFamily: '$YekanRegular'
            }
        }),
        backgroundColor:'transparent',
        marginRight:10
    },
    favoriteEditButton:{
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'#ffffff',
        paddingVertical:7,
        paddingHorizontal:20,
        borderWidth:5/PixelRatio.get(),
        borderColor:'#eceff1',
        borderRadius:30
    },
    favoriteHorizontalSectionList:{
        backgroundColor:'transparent',
        width:Dimensions.get('window').width,
        //marginTop: 25
    },
    favoriteHorizontalSectionListItem:{
        width:Dimensions.get('window').width / 2.5,
        marginRight: 15,
        paddingBottom: 5
    },
    favoriteHorizontalSectionListItemContainer:{
        padding:5,
        margin:2,
        backgroundColor:'#ffffff',
        borderWidth:5/PixelRatio.get(),
        borderColor:'#eceff1',
        borderRadius:15,
        paddingTop:10,
        paddingBottom: 20
    },
    favoriteHorizontalSectionListItemImage:{
        width:'100%',
        height:Dimensions.get('window').width / 3,
        resizeMode: 'contain'
    },
    favoriteHorizontalSectionListItemName:{
        textAlign:'center',
        color:'#333333',
        writingDirection:'rtl',
        fontSize:12,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'normal',
                //paddingTop:10
            },
            android: {
                fontFamily: '$YekanRegular'
            }
        }),
        marginTop:10
    },
    favoriteDeleteButton:{
        position:'absolute',
        backgroundColor:'#fd5459',
        bottom:0,
        alignSelf: 'center',
        paddingVertical:2,
        paddingHorizontal:10,
        borderRadius:15
    },
    favoriteDeleteButtonText:{
        textAlign:'center',
        color:'#ffffff',
        writingDirection:'rtl',
        fontSize:10,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'normal',
                //paddingTop:10
            },
            android: {
                fontFamily: '$YekanRegular'
            }
        }),
    },
    logOutModal:{
        backgroundColor:'#ffffff',
        alignSelf:'center',
        borderRadius:5,
        justifyContent:'center',
        alignItems:'center',
        paddingVertical:10,
        paddingHorizontal:5
    },
    modalLoadingText:{
        textAlign:'right',
        writingDirection:'rtl',
        fontSize:13,
        color:'#333333',
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold'
            },
            android: {
                fontFamily: '$YekanBold'
            }
        }),
        marginTop:15,
        paddingHorizontal:15
    },
    modalColorGrButton:{
        alignSelf:'center',
        backgroundColor:'$ButtonBackgroundColor',
        width:null,
        height:30,
        justifyContent:'center',
        alignItems:'center',
        borderRadius:15,
        ...Platform.select({
            ios: {
                shadowColor: '$MainColor',
                shadowOffset: { width: 0, height: 0 },
                shadowOpacity: 0.6,
                shadowRadius: 7,
            },
            android: {
                elevation: 2
            }
        }),
    },
    modalGrButtonGradient:{
        width:null,
        height:30,
        borderRadius:15,
        paddingHorizontal:30,
        justifyContent:'center',
        alignItems:'center',
    },
    modalGrButtonText:{
        textAlign:'center',
        writingDirection:'rtl',
        fontSize:12,
        //lineHeight:14,
        color:'$MainFontColor',
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold'
            },
            android: {
                fontFamily: '$YekanBold',
            }
        }),
    },
    splashBottomSection:{
        backgroundColor:'transparent',
        flex:1,
        height:Dimensions.get('window').height - 170,
        justifyContent:'center',
        alignItems:'center'
    },
    loginColorGrButton:{
        alignSelf:'center',
        backgroundColor:'$ButtonBackgroundColor',
        width:200,
        height:40,
        justifyContent:'center',
        alignItems:'center',
        borderRadius:20,
        ...Platform.select({
            ios: {
                shadowColor: '$MainColor',
                shadowOffset: { width: 0, height: 0 },
                shadowOpacity: 0.6,
                shadowRadius: 7,
            },
            android: {
                elevation: 2
            }
        }),
    },
    loginGrButtonGradient:{
        width:200,
        height:40,
        borderRadius:20,
        //paddingHorizontal:30,
        justifyContent:'center',
        alignItems:'center',
    },
    loginGrButtonText:{
        textAlign:'center',
        writingDirection:'rtl',
        fontSize:14,
        //lineHeight:14,
        color:'$MainFontColor',
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold'
            },
            android: {
                fontFamily: '$YekanBold',
            }
        }),
    },
    buttonTransparentText:{
        textAlign:'center',
        writingDirection:'rtl',
        fontSize:14,
        //lineHeight:14,
        color:'$MainColor',
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold'
            },
            android: {
                fontFamily: '$YekanBold'
            }
        }),
    },
    listLoadingIndicator:{
        marginTop:Dimensions.get('window').height * .1
    },
    notFoundText:{
        textAlign:'center',
        writingDirection:'rtl',
        fontSize:14,
        color:'#777777' ,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold',
                //paddingTop:3
            },
            android: {
                fontFamily: '$YekanBold',
            }
        }),
        marginTop:Dimensions.get('window').height * .1
    },
    postSliderContainer:{
        backgroundColor:'#ffffff',
        ...Platform.select({
            android: {
                width: Dimensions.get('window').width
            }
        }),
        justifyContent:'center',
        alignItems:'center',
        height:Dimensions.get('window').height * .30,
    },
    postSliderSlide:{
        width: Dimensions.get('window').width - 30,
        marginHorizontal:15,
        marginVertical:10,
        height: Dimensions.get('window').height * .30 - 20,
        backgroundColor:'#eeeeee',
        borderRadius:15,
        overflow:'hidden'
    },
    postSliderImage:{
        width: '100%',
        height: '100%',
    },
    postSliderTitleContainer:{
        backgroundColor:'rgba(0,0,0,.85)',
        position:'absolute',
        bottom: 0,
        alignSelf:'center',
        width:Dimensions.get('window').width * .8,
        height: 70,
        borderTopRightRadius: 10,
        borderTopLeftRadius:10,
        paddingTop: 10
    },
    postSliderTitleText:{
        textAlign:'center',
        writingDirection:'rtl',
        fontSize:14,
        color:'#ffffff' ,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold',
                //paddingTop:3
            },
            android: {
                fontFamily: '$YekanBold',
            }
        }),
        paddingHorizontal:10
    },
    dot:{
        backgroundColor:'#d5dbe1',
        width: 8,
        height: 8,
        borderRadius: 4,
        marginLeft: 3,
        marginRight: 3,
        marginTop: 3,
        marginBottom: 3
    },
    activeDotContainer:{
        backgroundColor:'#d5dbe1',
        width: 20,
        height: 20,
        borderRadius: 10,
        marginLeft: 3,
        marginRight: 3,
        marginTop: 3,
        marginBottom: 3,
        justifyContent: 'center',
        alignItems: 'center'
    },
    activeDotInner:{
        backgroundColor: '#ffffff',
        width: 8,
        height: 8,
        borderRadius: 4,
        marginLeft: 3,
        marginRight: 3,
        marginTop: 3,
        marginBottom: 3
    },
    blogHeader:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        paddingHorizontal:20,
        backgroundColor:'transparent',
        height:60
    },
    blogHeaderText:{
        textAlign:'center',
        writingDirection:'rtl',
        fontSize:14,
        color:'#323232' ,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold',
            },
            android: {
                fontFamily: '$YekanBold',
            }
        }),
    },
    blogHeaderButton:{
        backgroundColor:'#f4f7f9',
        paddingHorizontal:15,
        paddingVertical:7,
        borderRadius:7
    },
    blogHeaderButtonText:{
        textAlign:'center',
        writingDirection:'rtl',
        fontSize:12,
        color:'#0082f0' ,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold',
            },
            android: {
                fontFamily: '$YekanBold',
            }
        }),
    },
    blogContainer:{
        backgroundColor:'#ffffff'
    },
    singlePost:{
        backgroundColor:'#ffffff',
        margin:20,
        marginTop:0,
        borderRadius:7,
        borderWidth:5/PixelRatio.get(),
        borderColor:'#eceff1',
        width:Dimensions.get('window').width - 40,
    },
    singlePostTop:{
        flex: 1,
        backgroundColor:'#ffffff',
        borderTopRightRadius:5,
        borderTopLeftRadius: 5,
        width:'100%',
        height:Dimensions.get('window').height * .2,
        overflow: 'hidden'
    },
    singlePostImage:{
        width:'100%',
        height:'100%',
        resizeMode:'cover',
        borderTopRightRadius: 5,
        borderTopLeftRadius: 5,
    },
    singlePostCategory:{
        backgroundColor:'#333333',
        position:'absolute',
        bottom:0,
        right:15,
        borderTopRightRadius:7,
        borderTopLeftRadius: 7,
        paddingVertical:2,
        paddingHorizontal:13
    },
    singlePostCategoryText:{
        textAlign:'center',
        writingDirection:'rtl',
        fontSize:12,
        color:'#ffffff' ,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'normal',
            },
            android: {
                fontFamily: '$YekanRegular',
            }
        }),
    },
    singlePostBottom:{
        paddingHorizontal:10,
        paddingVertical:10
    },
    singlePostTitle:{
        textAlign:'right',
        writingDirection:'rtl',
        fontSize:14,
        color:'#393939' ,
        lineHeight:20,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold',
            },
            android: {
                fontFamily: '$YekanBold',
            }
        }),
    },
    singlePostDate:{
        textAlign:'right',
        writingDirection:'rtl',
        fontSize:10,
        color:'#cccccc' ,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'normal',
            },
            android: {
                fontFamily: '$YekanRegular',
            }
        }),
        marginTop:5
    },
    postMoreButton:{
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'#0082f0',
        paddingVertical:10,
        // borderWidth:5/PixelRatio.get(),
        // borderColor:'#eceff1',
        borderRadius:15,
        width:Dimensions.get('window').width * .85,
        alignSelf:'center',
        marginVertical: 20,
        flexDirection:'row'
    },
    postMoreButtonText:{
        textAlign:'right',
        writingDirection:'rtl',
        fontSize:12,
        color:'#ffffff' ,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold',
            },
            android: {
                fontFamily: '$YekanBold',
            }
        }),
    },
    requestModalContainer:{
        width:'90%',
        minHeight:'90%',
        backgroundColor:'#ffffff',
        alignSelf:'center',
        borderRadius:10,
        justifyContent:'center',
        alignItems:'center',
        paddingVertical: 10
    },
    drawerHeader:{
        backgroundColor:'transparent',
        paddingVertical: 10,
        paddingRight: 20,
        borderBottomWidth:10/PixelRatio.get(),
        borderBottomColor:'#f4f7f9'
    },
    drawerHeaderText:{
        textAlign:'right',
        writingDirection:'rtl',
        fontSize:16,
        color:'$MainColor',
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold',
                //paddingTop:3
            },
            android: {
                fontFamily: '$YekanBold'
            }
        }),
        backgroundColor:'transparent'
    },
    drawerTitleRow:{
        flexDirection: 'row-reverse',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 15,
        paddingRight: 7,
        borderBottomWidth: 2/PixelRatio.get(),
        borderBottomColor:'#f4f7f9',
        marginHorizontal: 15
    },
    drawerTitleRowText:{
        textAlign:'right',
        writingDirection:'rtl',
        fontSize:14,
        color:'#393939',
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold',
                //paddingTop:3
            },
            android: {
                fontFamily: '$YekanBold'
            }
        }),
        backgroundColor:'transparent'
    },
    drawerTitleRowTextExpanded:{
        textAlign:'right',
        writingDirection:'rtl',
        fontSize:14,
        color:'$MainColor',
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold',
                //paddingTop:3
            },
            android: {
                fontFamily: '$YekanBold'
            }
        }),
        backgroundColor:'transparent'
    },
    drawerTitleRowExpandedIcon:{
        fontSize:22,
        color:'$MainColor'
    },
    drawerTitleRowUnexpandedIcon:{
        fontSize:22,
        color:'#dadadb'
    },
    drawerContent:{
        padding: 10,
        borderWidth: 5/PixelRatio.get(),
        borderColor:'#f4f7f9',
        //backgroundColor:'#cccccc',
        margin: 15,
        marginTop: 0,
        borderRadius: 15
    },
    drawerContentRow:{
        flexDirection: 'row-reverse',
        alignItems:'center',
        borderBottomWidth: 5/PixelRatio.get(),
        borderBottomColor:'#f4f7f9',
    },
    drawerContentIcon:{
        fontSize:18,
        color:'$MainColor'
    },
    drawerContentText:{
        textAlign:'right',
        writingDirection:'rtl',
        fontSize:12,
        color:'#393939',
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'normal',
                //paddingTop:3
            },
            android: {
                fontFamily: '$YekanRegular'
            }
        }),
        backgroundColor:'transparent',
        marginRight: 10,
        paddingVertical:10,
    },
    videoHeader:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        paddingHorizontal:20,
        backgroundColor:'#222328',
        height:60
    },
    videoHeaderText:{
        textAlign:'center',
        writingDirection:'rtl',
        fontSize:18,
        color:'#ffffff' ,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold',
            },
            android: {
                fontFamily: '$YekanBold',
            }
        }),
    },
    singleVideo:{
        backgroundColor:'#17171c',
        margin:20,
        marginTop:0,
        borderRadius:7,
        width:Dimensions.get('window').width - 40,
        height:Dimensions.get('window').height * .3,
        overflow: 'hidden'
    },
    singleVideoImage:{
        width:'100%',
        height:'100%',
        resizeMode:'contain',
        borderRadius:7,
    },
    singleVideoTop:{
        flex:1,
        backgroundColor:'transparent',
        borderTopRightRadius:5,
        borderTopLeftRadius: 5,
        justifyContent:'center',
        alignItems:'center'
    },
    singleVideoPlay:{
        width:60,
        height:60,
        backgroundColor:'#0082f0',
        borderRadius:30,
        justifyContent:'center',
        alignItems:'center',
        opacity: .9
    },
    singleVideoPlayIcon:{
        fontSize:40,
        color:'#ffffff',
        marginLeft:5
    },
    singleVideoBottom:{
        width:'100%',
        position:'absolute',
        bottom:0
    },
    grButtonGradient:{
        width:'100%',
        height:'100%',
        paddingHorizontal:10,
        paddingVertical:15,
        justifyContent:'center'
    },
    singleVideoTitle:{
        textAlign:'right',
        writingDirection:'rtl',
        fontSize:14,
        color:'#ffffff' ,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold',
            },
            android: {
                fontFamily: '$YekanBold',
            }
        }),
    },
    videoMoreButton:{
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'#3b3c45',
        paddingVertical:10,
        borderWidth:5/PixelRatio.get(),
        borderColor:'#5e5f6e',
        borderRadius:30,
        width:150,
        alignSelf:'center',
        marginVertical: 20,
        flexDirection:'row'
    },
    videoMoreButtonText:{
        textAlign:'right',
        writingDirection:'rtl',
        fontSize:12,
        color:'#ffffff' ,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold',
            },
            android: {
                fontFamily: '$YekanBold',
            }
        }),
    },
});

export default styles;
