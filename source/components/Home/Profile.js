import React from 'react';
import {StatusBar, Image, TextInput, AsyncStorage,Text, Dimensions,TouchableOpacity,View,ScrollView} from 'react-native';
import {Header, Icon, Left, Right, Content, Container, Body} from 'native-base';
import styles from './Style';
import { Actions } from 'react-native-router-flux';
import {setUser} from "../../redux/Actions/Index";
import {connect} from "react-redux";
import MenuButton from "../Shared/MenuButton";
import BasketButton from "../Shared/BasketButton";
import ProfileInfoIcon from "../../assets/icon/ProfileInfoIcon";
import ProfileHistoryIcon from "../../assets/icon/ProfileHistoryIcon";
import ProfileAddressIcon from "../../assets/icon/ProfileAddressIcon";
import ProfileEditIcon from "../../assets/icon/ProfileEditIcon";
import ProfileLogoutIcon from "../../assets/icon/ProfileLogoutIcon";
import HeaderRightLabel from "../Shared/HeaderRightLabel";
import Helpers from "../Shared/Helper";
import Modal from "react-native-modal";
import LinearGradient from "react-native-linear-gradient";
import OtherPasswordIcon from "../../assets/icon/OtherPasswordIcon";

class Profile extends React.Component {

    constructor(props)
    {
        super(props);
        this.state = {
            showModal:false
        };

        this.logOutUser = this.logOutUser.bind(this);
        this.search = this.search.bind(this);
    }

    logOutUser(){
        AsyncStorage.removeItem("apiToken");
        AsyncStorage.removeItem("cart");

        this.props.setUser({
            fullName: null,
            name:null,
            family:null,
            phoneNumber:null,
            apiToken: null,
            id:null,
            email:null
        });

        this.setState({
            showModal:false
        });
    }

    async search(){
        //console.log(this.searchBox._lastNativeText);
        this.searchBox.blur();
        Actions.push('productSearch',{query:this.searchBox._lastNativeText});
    }

    render() {

        return (
            <Container style={styles.mainContainer}>
                <Image resizeMode='stretch' style={[styles.mainBackground,{height:1000}]} source={require("./../../assets/images/shared/menu-bg.png")} />
                <Header style={styles.header}>
                    <Left style={{flex:0.2,alignItems:'center',justifyContent:'center'}}>
                        <BasketButton/>
                    </Left>
                    <Body style={{flex:1,alignItems:'flex-end',justifyContent:'center'}}>
                    <HeaderRightLabel title='پروفایل'/>
                    </Body>
                    <Right style={{flex:.2,alignItems:'center',justifyContent:'center'}}>
                        <MenuButton/>
                    </Right>
                </Header>

                <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content" />

                <Content>
                    <View style={styles.searchBoxContainer}>
                        <TextInput underlineColorAndroid="rgba(0,0,0,0)" style={styles.searchBoxInput}
                                   placeholderTextColor="#cccccc" onSubmitEditing={this.search} returnKeyType='search'
                                   placeholder='عبارت مورد نظر خود را تایپ کنید' ref={ref => this.searchBox = ref}/>

                        <TouchableOpacity style={styles.searchBoxButton} onPress={this.search}>
                            <Icon name="ios-search" style={styles.searchBoxIcon}/>
                        </TouchableOpacity>
                    </View>
                    <ScrollView style={{minHeight: Dimensions.get('window').height - 170,backgroundColor:'#f4f7f9'}}>
                        {this.props.user.apiToken !== null && <View style={styles.profileTopSection}>
                            <View style={styles.profileThumbContainer}>
                                <Image resizeMode='contain' style={{width:'100%',height:'100%'}} source={require('../../assets/images/shared/avatar.png')}/>
                            </View>
                            <Text style={styles.profileName}>{this.props.user.fullName}</Text>
                            <Text style={styles.profileNumber}>{Helpers.ToPersianNumber(this.props.user.phoneNumber)}</Text>
                        </View>}
                        {this.props.user.apiToken !== null && <View style={styles.profileBottomSection}>
                            <TouchableOpacity style={styles.profileBottomSectionRow} activeOpacity={.7} onPress={() => Actions.push('editProfile')}>
                                <Icon name='ios-arrow-back' style={styles.profileBottomSectionRowIcon}/>
                                <View style={styles.profileBottomSectionRowRight}>
                                    <Text style={styles.profileBottomSectionRowText}>ویرایش مشخصات</Text>
                                    <ProfileEditIcon/>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.profileBottomSectionRow} activeOpacity={.7} onPress={() => Actions.push('editPassword')}>
                                <Icon name='ios-arrow-back' style={styles.profileBottomSectionRowIcon}/>
                                <View style={styles.profileBottomSectionRowRight}>
                                    <Text style={styles.profileBottomSectionRowText}>تغییر رمز عبور</Text>
                                    <OtherPasswordIcon/>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.profileBottomSectionRow} activeOpacity={.7} onPress={() => Actions.push('myOrder')}>
                                <Icon name='ios-arrow-back' style={styles.profileBottomSectionRowIcon}/>
                                <View style={styles.profileBottomSectionRowRight}>
                                    <Text style={styles.profileBottomSectionRowText}>سوابق خرید</Text>
                                    <ProfileHistoryIcon/>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.profileBottomSectionRow} activeOpacity={.7} onPress={() => Actions.push('myAddress')}>
                                <Icon name='ios-arrow-back' style={styles.profileBottomSectionRowIcon}/>
                                <View style={styles.profileBottomSectionRowRight}>
                                    <Text style={styles.profileBottomSectionRowText}>آدرس های من</Text>
                                    <ProfileAddressIcon/>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity style={[styles.profileBottomSectionRow,{justifyContent:'flex-end',borderBottomWidth:0}]} activeOpacity={.7} onPress={() => { this.setState({showModal:true}); }}>
                                <View style={styles.profileBottomSectionRowRight}>
                                    <Text style={styles.profileBottomSectionRowText}>خروج از حساب کاربری</Text>
                                    <ProfileLogoutIcon/>
                                </View>
                            </TouchableOpacity>
                        </View>}
                        {this.props.user.apiToken === null && <View style={styles.splashBottomSection}>
                            <TouchableOpacity style={styles.loginColorGrButton} activeOpacity={0.8} onPress={() => Actions.push('intro',{initialIndex:1})}>
                                <LinearGradient colors={[this.props.global.grColorOne,this.props.global.grColorTwo]} style={styles.loginGrButtonGradient} start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}>
                                    <Text style={styles.loginGrButtonText}>ورود</Text>
                                </LinearGradient>
                            </TouchableOpacity>

                            <TouchableOpacity style={{marginTop: 30}} activeOpacity={0.8} onPress={() => Actions.push('intro',{initialIndex:0})}>
                                <Text style={styles.buttonTransparentText}>حساب کاربری ندارید؟ عضو شوید.</Text>
                            </TouchableOpacity>
                        </View>}
                    </ScrollView>

                    <Modal isVisible={this.state.showModal} backdropOpacity={0.5} animationIn="fadeIn" animationOut="fadeOut">
                        <View style={styles.logOutModal}>
                            <Icon name="md-alert" style={{color:'#ffb941',fontSize:50}} />
                            <Text style={[styles.modalLoadingText,{textAlign:'center'}]}>
                                خروج از حساب کاربری را تایید میکنید؟
                            </Text>
                            <View style={{flexDirection:'row',marginTop:25,justifyContent:'center',alignItems:'center'}}>

                                <TouchableOpacity style={[styles.modalColorGrButton]} activeOpacity={.8} onPress={() => { this.setState({showModal:false}); }}>
                                    <LinearGradient colors={[this.props.global.grColorOne,this.props.global.grColorTwo]} style={styles.modalGrButtonGradient} start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}>
                                        <Text style={styles.modalGrButtonText}>خیر</Text>
                                    </LinearGradient>
                                </TouchableOpacity>

                                <TouchableOpacity style={[styles.modalColorGrButton,{marginLeft:15}]} activeOpacity={.8} onPress={this.logOutUser}>
                                    <LinearGradient colors={[this.props.global.grColorOne,this.props.global.grColorTwo]} style={styles.modalGrButtonGradient} start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}>
                                        <Text style={styles.modalGrButtonText}>بلی</Text>
                                    </LinearGradient>
                                </TouchableOpacity>

                            </View>
                        </View>
                    </Modal>

                </Content>
            </Container>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setUser : user => {
            dispatch(setUser(user))
        }
    }
};

const mapStateToProps =(state) =>  {
    return{
        user: state.user,
        global:state.global
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(Profile);