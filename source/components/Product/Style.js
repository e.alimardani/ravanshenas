import EStyleSheet from 'react-native-extended-stylesheet';
import {Platform,Dimensions,PixelRatio,StatusBar} from "react-native";

export const styles = EStyleSheet.create({
    mainContainer : {
        backgroundColor:'$MainColor'
    },
    loadingModalContainer:{
        backgroundColor:'#ffffff',
        alignSelf:'center',
        borderRadius:5,
        justifyContent:'center',
        alignItems:'center',
        ...Platform.select({
            ios: {
                padding:10,
                paddingLeft:13,
                paddingTop:13
            },
            android: {
                padding:10
            }
        }),
    },
    textInputComment:{
        flex:1,
        textAlign:'right',
        writingDirection:'rtl',
        fontSize:14,
        marginLeft:5,
        color:'#393939' ,
        paddingVertical:2,
        paddingHorizontal:10,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'normal',
                //paddingTop:10
            },
            android: {
                fontFamily: '$YekanRegular'
            }
        }),
        borderRadius:25,
        height:50,
        borderWidth:1,
        backgroundColor:'#fff',
    },
    mainBackground:{
        position: 'absolute',
        width:Dimensions.get('window').width,
        height:Dimensions.get('window').height,
        //opacity:0.7
    },
    header : {
        backgroundColor : 'transparent',
        borderBottomWidth:0,
        elevation: 0,
        ...Platform.select({
            ios: {

            },
            android: {
                marginTop:StatusBar.currentHeight
            }
        }),
    },
    productSliderContainer:{
        backgroundColor:'#ffffff',
        ...Platform.select({
            android: {
                width: Dimensions.get('window').width
            }
        }),
        justifyContent:'center',
        alignItems:'center',
        height:Dimensions.get('window').height * .35,
        //borderBottomWidth: 5/PixelRatio.get(),
        //borderBottomColor:'#800'
        //paddingBottom: 50
    },
    productSliderSlide:{
        width: '100%',
        height: Dimensions.get('window').height * .30,
        backgroundColor:'#ffffff'
    },
    productSliderImage:{
        resizeMode: 'contain',
        width: '100%',
        height: '100%',
    },
    dot:{
        backgroundColor:'#d5dbe1',
        width: 8,
        height: 8,
        borderRadius: 4,
        marginLeft: 3,
        marginRight: 3,
        marginTop: 3,
        marginBottom: 3
    },
    activeDotContainer:{
        backgroundColor:'#d5dbe1',
        width: 20,
        height: 20,
        borderRadius: 10,
        marginLeft: 3,
        marginRight: 3,
        marginTop: 3,
        marginBottom: 3,
        justifyContent: 'center',
        alignItems: 'center'
    },
    activeDotInner:{
        backgroundColor: '#ffffff',
        width: 8,
        height: 8,
        borderRadius: 4,
        marginLeft: 3,
        marginRight: 3,
        marginTop: 3,
        marginBottom: 3
    },
    productTitleContainer:{
        backgroundColor:'#ffffff',
        justifyContent:'center',
        alignItems:'center',
        paddingVertical: 30,
        paddingHorizontal: 10
    },
    productTitleCategoryContainer:{
        width:null,
        height:35,
        backgroundColor:'#f3f6f9',
        justifyContent:'center',
        alignItems:'center',
        borderRadius:17.5,
        paddingHorizontal: 20
    },
    productTitleCategoryText:{
        textAlign:'center',
        writingDirection:'rtl',
        fontSize:12,
        color:'$MainColor',
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'normal'
            },
            android: {
                fontFamily: '$YekanRegular'
            }
        }),
    },
    productTitleTextOne:{
        textAlign:'center',
        writingDirection:'rtl',
        fontSize:16,
        color:'#393939',
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold'
            },
            android: {
                fontFamily: '$YekanBold'
            }
        }),
        marginTop:20
    },
    productTitleTextTwo:{
        textAlign:'center',
        writingDirection:'rtl',
        fontSize:14,
        color:'#9fa9b3',
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'normal'
            },
            android: {
                fontFamily: '$YekanRegular'
            }
        }),
        marginTop:5
    },
    productDescContainer:{
        backgroundColor:'#ffffff',
        justifyContent:'center',
        alignItems:'center',
        paddingVertical: 20,
        paddingHorizontal: 10,
        borderTopWidth: 5/PixelRatio.get(),
        borderTopColor:'#f1f4f7'
    },
    productPriceTitle:{
        textAlign:'center',
        writingDirection:'rtl',
        fontSize:14,
        color:'#393939',
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'normal'
            },
            android: {
                fontFamily: '$YekanRegular'
            }
        }),
        marginTop:5
    },
    productPrice:{
        textAlign:'center',
        writingDirection:'rtl',
        fontSize:18,
        color:'#2fcc71',
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold'
            },
            android: {
                fontFamily: '$YekanBold'
            }
        }),
        marginTop:5
    },
    colorGrButton:{
        alignSelf:'center',
        backgroundColor:'$ButtonBackgroundColor',
        width:null,
        height:40,
        justifyContent:'center',
        alignItems:'center',
        borderRadius:20,
        ...Platform.select({
            ios: {
                shadowColor: '$MainColor',
                shadowOffset: { width: 0, height: 0 },
                shadowOpacity: 0.6,
                shadowRadius: 7,
            },
            android: {
                elevation: 2
            }
        }),
    },
    grButtonGradient:{
        width:null,
        height:40,
        borderRadius:20,
        justifyContent:'center',
        alignItems:'center',
        paddingHorizontal:40
    },
    grButtonText:{
        textAlign:'center',
        writingDirection:'rtl',
        fontSize:14,
        //lineHeight:14,
        color:'$MainFontColor',
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold'
            },
            android: {
                fontFamily: '$YekanBold',
            }
        }),
    },
    productDescInfoContainer:{
        borderWidth: 5/PixelRatio.get(),
        borderColor:'#f1f4f7',
        marginHorizontal: 10,
        marginTop:30,
        borderRadius:20,
        paddingVertical:20,
        paddingHorizontal:10,
        // ...Platform.select({
        //     ios: {
        //         shadowColor: '#000000',
        //         shadowOffset: { width: 0, height: 0 },
        //         shadowOpacity: 0.15,
        //         shadowRadius: 10,
        //     },
        //     android: {
        //         elevation: 1
        //     }
        // }),
        backgroundColor:'#ffffff',
        width:Dimensions.get('window').width - 20
    },
    productDescInfoTitle:{
        textAlign:'right',
        writingDirection:'rtl',
        fontSize:16,
        //lineHeight:14,
        color:'#393939',
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold'
            },
            android: {
                fontFamily: '$YekanBold',
            }
        })
    },
    productDescInfoText:{
        textAlign:'right',
        writingDirection:'rtl',
        fontSize:14,
        //lineHeight:14,
        color:'#393939',
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'normal'
            },
            android: {
                fontFamily: '$YekanRegular',
            }
        }),
        marginTop:15
    },
    productDescInfoViewMore:{
        borderWidth: 5/PixelRatio.get(),
        borderColor:'#f1f4f7',
        width:130,
        height:50,
        justifyContent:'center',
        alignItems:'center',
        alignSelf: 'center',
        marginTop:30,
        borderRadius:25,
        flexDirection: 'row',
        ...Platform.select({
            ios: {
                shadowColor: '#rgba(0,0,0,0)',
                shadowOffset: { width: 0, height: 0 },
                shadowOpacity: 0,
                shadowRadius: 0,
            }
        }),
    },
    productDescInfoViewMoreText:{
        textAlign:'center',
        writingDirection:'rtl',
        fontSize:12,
        //lineHeight:14,
        color:'$MainColor',
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold'
            },
            android: {
                fontFamily: '$YekanBold',
            }
        }),
    },
    productDescInfoViewMoreIcon:{
        fontSize:18,
        marginLeft:10,
        color:'#999999'
    },
    productFilesContainer:{
        backgroundColor:'#ffffff',
        justifyContent:'center',
        alignItems:'center',
        paddingVertical: 10,
        paddingHorizontal: 10,
        borderTopWidth: 5/PixelRatio.get(),
        borderTopColor:'#f1f4f7'
    },
    filesListItem:{
        width:Dimensions.get('window').width - 20,
        //backgroundColor:'#cccccc',
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        paddingHorizontal:5,
        paddingVertical:5,
        paddingBottom:10,
        borderBottomWidth: 5/PixelRatio.get(),
        borderBottomColor:'#f1f4f7'
    },
    fileListItemName:{
        textAlign:'center',
        color:'#333333',
        writingDirection:'rtl',
        fontSize:12,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold',
                //paddingTop:10
            },
            android: {
                fontFamily: '$YekanBold'
            }
        }),
    },
    fileShowButtonText:{
        textAlign:'center',
        color:'$MainColor',
        writingDirection:'rtl',
        fontSize:10,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'normal',
                //paddingTop:10
            },
            android: {
                fontFamily: '$YekanRegular'
            }
        }),
    },
    productHorizontalSection:{
        backgroundColor:'#ffffff',
        paddingVertical:25,
        paddingTop: 10
    },
    productHorizontalSectionHeader:{
        flexDirection: 'row-reverse',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 20
    },
    productHorizontalSectionHeaderText:{
        textAlign:'right',
        color:'#393939',
        writingDirection:'rtl',
        fontSize:14,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold',
                //paddingTop:10
            },
            android: {
                fontFamily: '$YekanBold'
            }
        }),
    },
    productHorizontalSectionHeaderMoreText:{
        textAlign:'center',
        color:'$MainColor',
        writingDirection:'rtl',
        fontSize:12,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'normal',
                //paddingTop:10
            },
            android: {
                fontFamily: '$YekanRegular'
            }
        }),
    },
    productHorizontalSectionHeaderIcon:{
        fontSize:20,
        color:'$MainColor',
        marginRight: 7
    },
    productHorizontalSectionList:{
        backgroundColor:'transparent',
        width:Dimensions.get('window').width,
        marginTop: 25
    },
    productHorizontalSectionListItem:{
        width:Dimensions.get('window').width / 3.2,
        //backgroundColor:'#cccccc',
        marginRight: 15
    },
    productHorizontalSectionListItemContainer:{
        padding:5,
        margin:2,
        backgroundColor:'#ffffff',
        borderRadius: 10,
        ...Platform.select({
            ios: {
                shadowColor: 'rgb(0, 0, 0)',
                shadowOffset: { width: 0, height: 0 },
                shadowOpacity: 0.05,
                shadowRadius: 3,
            },
            android: {
                elevation: 1
            }
        }),
        borderWidth:2/PixelRatio.get(),
        borderColor:'#eceff1',
    },
    productHorizontalSectionListItemImage:{
        width:'100%',
        height:Dimensions.get('window').width / 3.4,
        resizeMode: 'contain'
    },
    productHorizontalSectionListItemName:{
        textAlign:'center',
        color:'#333333',
        writingDirection:'rtl',
        fontSize:12,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'normal',
                //paddingTop:10
            },
            android: {
                fontFamily: '$YekanRegular'
            }
        }),
        marginTop:10
    },
    productHorizontalSectionListItemPrice:{
        textAlign:'center',
        color:'#2fcc71',
        writingDirection:'rtl',
        fontSize:12,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'normal',
                //paddingTop:10
            },
            android: {
                fontFamily: '$YekanRegular'
            }
        }),
        marginTop:0
    },
    productHorizontalSectionListItemOldPrice: {
        textAlign: 'center',
        color: '#db1623',
        writingDirection: 'rtl',
        fontSize: 12,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'normal',
                //paddingTop:10
            },
            android: {
                fontFamily: '$YekanRegular'
            }
        }),
        marginTop: 10,
        textDecorationLine:'line-through'
    },
    productHorizontalSectionListItemPercent:{
        position:'absolute',
        top:10,
        backgroundColor:'$MainColor',
        left: 0,
        width:35,
        height:25,
        borderTopRightRadius:12.5,
        borderBottomRightRadius:12.5,
        justifyContent:'center',
        alignItems:'center'
    },
    productHorizontalSectionListItemPercentText:{
        textAlign: 'center',
        color: '#ffffff',
        writingDirection: 'rtl',
        fontSize: 12,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'normal',
                //paddingTop:10
            },
            android: {
                fontFamily: '$YekanRegular'
            }
        }),
    },
    favoriteEditButton:{
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'#ffffff',
        paddingVertical:7,
        paddingHorizontal:20,
        borderWidth:5/PixelRatio.get(),
        borderColor:'#eceff1',
        borderRadius:30
    },
    addressList: {
        backgroundColor: 'transparent',
        //width:Dimensions.get('window').width,
    },
    addressListItem:{
        borderWidth:5/PixelRatio.get(),
        borderColor:'#eceff1',
        marginHorizontal: 10,
        borderRadius:20
    },
    addressListItemHeader:{
        backgroundColor:'#f7f8fa',
        borderBottomWidth:5/PixelRatio.get(),
        borderBottomColor:'#eceff1',
        borderTopRightRadius: 20,
        borderTopLeftRadius:20,
        height:50,
        flexDirection:'row-reverse',
        alignItems:'center',
        justifyContent:'space-between',
        paddingHorizontal:10
    },
    addressListItemHeaderText:{
        textAlign: 'center',
        color: '$MainColor',
        writingDirection: 'rtl',
        fontSize: 14,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold',
                //paddingTop:10
            },
            android: {
                fontFamily: '$YekanBold'
            }
        })
    },
    addressListItemHeaderIcon:{
        fontSize:24,
        color:'#1fc792'
    },
    addressListItemAddress:{
        //backgroundColor:'#800',
        marginHorizontal:10,
        paddingVertical:10,
        borderBottomWidth:5/PixelRatio.get(),
        borderBottomColor:'#eceff1'
    },
    addressListItemAddressText:{
        textAlign: 'right',
        color: '#393939',
        writingDirection: 'rtl',
        fontSize: 12,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'normal',
                //paddingTop:10
            },
            android: {
                fontFamily: '$YekanRegular'
            }
        })
    },
    addressListItemPhone:{
        flexDirection:'row',
        paddingHorizontal:10,
        paddingVertical:10,
    },
    addressListItemButtonSection:{
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
        paddingVertical:10,
        paddingHorizontal:5
    },
    addressListItemButton:{
        flex: 1,
        justifyContent:'center',
        alignItems:'center',
        borderWidth:5/PixelRatio.get(),
        borderColor:'#eceff1',
        borderRadius:20,
        marginHorizontal:5,
        paddingVertical:10
    },
    addressListItemButtonText:{
        textAlign: 'right',
        color: '#393939',
        writingDirection: 'rtl',
        fontSize: 10,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold',
                //paddingTop:10
            },
            android: {
                fontFamily: '$YekanBold'
            }
        })
    },
    orderListStatus:{
        flex: 1,
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
        borderWidth:5/PixelRatio.get(),
        borderColor:'#eceff1',
        borderRadius:20,
        marginHorizontal:3,
        paddingVertical:5
    },
    orderListStatusText:{
        textAlign: 'center',
        color: '#1fc792',
        writingDirection: 'rtl',
        fontSize: 14,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'normal',
                //paddingTop:10
            },
            android: {
                fontFamily: '$YekanRegular'
            }
        })
    },
    orderListStatusIcon:{
        fontSize:26,
        color:'#1fc792'
    },
    orderListProductImage:{
        resizeMode:'contain',
        width:Dimensions.get('window').width * .25,
        height:Dimensions.get('window').width * .25
    },
    orderListProductText:{
        textAlign: 'right',
        color: '#393939',
        writingDirection: 'rtl',
        fontSize: 14,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold',
                //paddingTop:10
            },
            android: {
                fontFamily: '$YekanBold'
            }
        })
    },
    orderListProductCount:{
        textAlign: 'right',
        color: '#393939',
        writingDirection: 'rtl',
        fontSize: 10,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'normal',
                //paddingTop:10
            },
            android: {
                fontFamily: '$YekanRegular'
            }
        }),
        marginTop:5
    },
    orderListProductPrice:{
        textAlign: 'right',
        color: '$MainColor',
        writingDirection: 'rtl',
        fontSize: 10,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold',
                //paddingTop:10
            },
            android: {
                fontFamily: '$YekanBold'
            }
        }),
        marginTop:5
    },
    searchBoxContainer:{
        width:Dimensions.get('window').width,
        height: 60,
        //borderBottomWidth: 2/PixelRatio.get(),
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 10
    },
    searchBoxInput:{
        width:'90%',
        textAlign:'right',
        writingDirection:'rtl',
        fontSize:12,
        //lineHeight:14,
        color:'#444444' ,
        paddingVertical:2,
        paddingHorizontal:10,
        paddingLeft:40,
        height:45,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'normal',
                //paddingTop:10
            },
            android: {
                fontFamily: '$YekanRegular'
            }
        }),
        backgroundColor:'#ffffff',
        borderRadius:10
    },
    searchBoxButton:{
        position: 'absolute',
        left:'7%'
    },
    searchBoxIcon:{
        fontSize: 30,
        color: '#000000'
    },
    cartListItem:{
        marginHorizontal: 15,
        borderRadius:20,
        flexDirection:'row',
        paddingVertical:10,
        ...Platform.select({
            ios: {
                shadowColor: 'rgb(0, 0, 0)',
                shadowOffset: { width: 0, height: 0 },
                shadowOpacity: 0.15,
                shadowRadius: 7,
            },
            android: {
                borderWidth:5/PixelRatio.get(),
                borderColor:'#eceff1',
            }
        }),
        backgroundColor:'#ffffff'
    },
    cartListItemLeft:{
        flex:1,
        justifyContent:'center',
        alignItems:'center'
    },
    cartListItemLeftButton:{
        width:30,
        height:30,
        borderRadius:15,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'#e6e6e6',
        // ...Platform.select({
        //     ios: {
        //         paddingTop:5
        //     }
        // }),
    },
    cartListItemLeftButtonIcon:{
        fontSize:20,
        color:'#a6b6bf',
    },
    cartListItemCenter:{
        flex:3,
        justifyContent:'center',
        alignItems:'flex-end'
    },
    cartListItemCenterName:{
        textAlign:'right',
        writingDirection:'rtl',
        fontSize:14,
        color:'#393939' ,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold',
                //paddingTop:10
            },
            android: {
                fontFamily: '$YekanBold'
            }
        }),
    },
    cartListItemCenterCount:{
        textAlign:'right',
        writingDirection:'rtl',
        fontSize:12,
        color:'#393939' ,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'normal',
                //paddingTop:10
            },
            android: {
                fontFamily: '$YekanRegular'
            }
        }),
        marginTop:10,
        textDecorationLine:'underline',
        textDecorationColor:'$MainColor'
    },
    cartListItemCenterPrice:{
        textAlign:'right',
        writingDirection:'rtl',
        fontSize:16,
        color:'#2fcc71' ,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'normal',
                //paddingTop:10
            },
            android: {
                fontFamily: '$YekanRegular'
            }
        }),
        marginTop:10
    },
    cartListItemRight:{
        flex:2,
        justifyContent:'center',
        alignItems:'center'
    },
    cartListItemRightImage:{
        width:Dimensions.get('window').width * .2,
        height:Dimensions.get('window').width * .2
    },
    cartListFooter:{
        backgroundColor:'transparent',
        paddingHorizontal:10,
        paddingVertical:15,
        paddingBottom:30,
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center'
    },
    cartListFooterButton:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'$MainColor',
        height:40,
        borderRadius:20,
        marginHorizontal:10,
        ...Platform.select({
            ios: {
                shadowColor: '#000000',
                shadowOffset: { width: 0, height: 0 },
                shadowOpacity: 0.6,
                shadowRadius: 7,
            },
            android: {
                elevation: 2
            }
        }),
    },
    cartListFooterButtonText:{
        textAlign:'right',
        writingDirection:'rtl',
        fontSize:14,
        color:'#ffffff' ,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold',
                //paddingTop:10
            },
            android: {
                fontFamily: '$YekanBold'
            }
        }),
    },
    cartCoupon:{
        backgroundColor:'#f3f6f9',
        marginHorizontal: 15,
        borderRadius:20,
        paddingVertical:10,
        justifyContent:'center',
        alignItems:'center'
    },
    inputMain:{
        borderColor:'#eeeeee' ,
        borderWidth:4 /PixelRatio.get(),
        width:'50%',
        textAlign:'center',
        writingDirection:'rtl',
        fontSize:14,
        borderRadius:20,
        color:'#444444' ,
        paddingVertical:2,
        paddingHorizontal:10,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'normal',
                paddingVertical:5,
            },
            android: {
                fontFamily: '$YekanRegular'
            }
        }),
        backgroundColor:'#ffffff'
    },
    productListItem:{
        marginHorizontal: 15,
        borderRadius:20,
        flexDirection:'row',
        paddingVertical:10,
        ...Platform.select({
            ios: {
                shadowColor: 'rgb(0, 0, 0)',
                shadowOffset: { width: 0, height: 0 },
                shadowOpacity: 0.15,
                shadowRadius: 7,
            },
            android: {
                borderWidth:5/PixelRatio.get(),
                borderColor:'#eceff1',
            }
        }),
        backgroundColor:'#ffffff'
    },
    productListItemLeft:{
        flex:1,
        justifyContent:'flex-end',
        alignItems:'center'
    },
    productListItemLeftButton:{
        width:30,
        height:30,
        borderRadius:15,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'#e6e6e6'
    },
    productListItemLeftButtonIcon:{
        fontSize:20,
        color:'#a6b6bf'
    },
    productListItemCenter:{
        flex:3,
        justifyContent:'center',
        alignItems:'flex-end',
        paddingLeft: 10
    },
    productListItemCenterName:{
        textAlign:'right',
        writingDirection:'rtl',
        fontSize:14,
        color:'#393939' ,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold',
                //paddingTop:10
            },
            android: {
                fontFamily: '$YekanBold'
            }
        }),
    },
    productListItemCenterCount:{
        textAlign:'right',
        writingDirection:'rtl',
        fontSize:12,
        color:'#393939' ,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'normal',
                //paddingTop:10
            },
            android: {
                fontFamily: '$YekanRegular'
            }
        }),
        marginTop:10
    },
    productListItemCenterOldPrice:{
        textAlign:'right',
        writingDirection:'rtl',
        fontSize:12,
        color:'#db1623' ,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'normal',
                //paddingTop:10
            },
            android: {
                fontFamily: '$YekanRegular'
            }
        }),
        marginTop:10,
        textDecorationLine:'line-through'
    },
    productListItemCenterPrice:{
        textAlign:'right',
        writingDirection:'rtl',
        fontSize:14,
        color:'#2fcc71' ,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'normal',
                //paddingTop:10
            },
            android: {
                fontFamily: '$YekanRegular'
            }
        }),
        marginTop:10
    },
    productListItemRight:{
        flex:2,
        justifyContent:'center',
        alignItems:'center'
    },
    productListItemRightImage:{
        width:Dimensions.get('window').width * .2,
        height:Dimensions.get('window').width * .2
    },
    notFoundText:{
        textAlign:'center',
        writingDirection:'rtl',
        fontSize:14,
        color:'#777777' ,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold',
                //paddingTop:3
            },
            android: {
                fontFamily: '$YekanBold',
            }
        }),
        marginTop:Dimensions.get('window').height * .1
    },
    listLoadingIndicator:{
        marginTop:Dimensions.get('window').height * .1
    },
    emptyCartBadge:{
        width:Dimensions.get('window').width * .12,
        height:Dimensions.get('window').width * .12,
        backgroundColor:'#ffba00',
        position:'absolute',
        right:0,
        borderRadius:Dimensions.get('window').width * .06,
        justifyContent:'center',
        alignItems:'center'
    },
    emptyCartBadgeText:{
        textAlign:'center',
        writingDirection:'rtl',
        fontSize:24,
        color:'#ffffff' ,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold',
                //paddingTop:3
            },
            android: {
                fontFamily: '$YekanBold',
            }
        }),
    },
    emptyCartTitleText:{
        textAlign:'center',
        writingDirection:'rtl',
        fontSize:24,
        color:'#393939' ,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold',
                //paddingTop:3
            },
            android: {
                fontFamily: '$YekanBold',
            }
        }),
        marginTop:10
    },
    emptyCartDescText:{
        textAlign:'center',
        writingDirection:'rtl',
        fontSize:14,
        color:'#999999' ,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'normal',
                //paddingTop:3
            },
            android: {
                fontFamily: '$YekanRegular',
            }
        }),
        marginTop:15
    },
    profileSectionTitle:{
        textAlign: 'right',
        color: '#393939',
        writingDirection: 'rtl',
        fontSize: 16,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold',
                //paddingTop:10
            },
            android: {
                fontFamily: '$YekanBold'
            }
        }),
        paddingHorizontal: 15,
        marginTop: 20
    },
    profileFormContainer:{
        borderWidth:5/PixelRatio.get(),
        borderColor:'#eceff1',
        margin:20,
        borderRadius:20,
        marginTop:20,
        marginBottom: 0,
        paddingBottom: 20
    },
    formRow:{
        flexDirection: 'row',
        justifyContent:'center',
        alignItems:'center',
        borderBottomWidth:5/PixelRatio.get(),
        borderBottomColor:'#eceff1',
        marginHorizontal: 10,
        paddingVertical: 10
    },
    textInput:{
        textAlign:'right',
        writingDirection:'rtl',
        fontSize:14,
        color:'#393939' ,
        paddingVertical:2,
        paddingHorizontal:0,
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'normal',
                //paddingTop:10
            },
            android: {
                fontFamily: '$YekanRegular'
            }
        }),
        width: '90%',
        marginRight:10
        //backgroundColor:'#800',
    },
    logOutModal:{
        backgroundColor:'#ffffff',
        alignSelf:'center',
        borderRadius:5,
        justifyContent:'center',
        alignItems:'center',
        paddingVertical:10,
        paddingHorizontal:5
    },
    modalLoadingText:{
        textAlign:'right',
        writingDirection:'rtl',
        fontSize:13,
        color:'#333333',
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold'
            },
            android: {
                fontFamily: '$YekanBold'
            }
        }),
        marginTop:15,
        paddingHorizontal:15
    },
    modalColorGrButton:{
        alignSelf:'center',
        backgroundColor:'$ButtonBackgroundColor',
        width:null,
        height:30,
        justifyContent:'center',
        alignItems:'center',
        borderRadius:15,
        ...Platform.select({
            ios: {
                shadowColor: '$MainColor',
                shadowOffset: { width: 0, height: 0 },
                shadowOpacity: 0.6,
                shadowRadius: 7,
            },
            android: {
                elevation: 2
            }
        }),
    },
    modalGrButtonGradient:{
        width:null,
        height:30,
        borderRadius:15,
        paddingHorizontal:30,
        justifyContent:'center',
        alignItems:'center',
    },
    modalGrButtonText:{
        textAlign:'center',
        writingDirection:'rtl',
        fontSize:12,
        //lineHeight:14,
        color:'$MainFontColor',
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold'
            },
            android: {
                fontFamily: '$YekanBold',
            }
        }),
    },
    productFavoriteButton:{
        width:40,
        height:40,
        borderRadius:20,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'#e6e6e6',
        marginTop:10,
        ...Platform.select({
            ios: {
                paddingTop: 5
            },
        }),
    },
    productFavoriteButtonIcon:{
        fontSize:30,
        color:'#a6b6bf'
    },
    fileModal:{
        justifyContent:'center',
        alignItems:'center'
    },
    fileModalContainer:{
        width:'85%',
        height:180,
        backgroundColor:'#ffffff',
        borderRadius:10,
        //paddingVertical:7,
        //paddingHorizontal:7,
        justifyContent:'center',
        alignItems:'center'
    },
    fileModalRow:{
        flex:1,
        flexDirection:'row',
        width:'100%',
        justifyContent:'center',
        alignItems:'center',
        paddingHorizontal:0,
        paddingVertical:20,
    },
    fileModalColumn:{
        flex:1,
        flexDirection:'column',
        borderLeftWidth:1/PixelRatio.get(),
        justifyContent:'center',
        alignItems:'center',
        height:'100%'
    },
    fileModalText:{
        textAlign:'center',
        writingDirection:'rtl',
        fontSize:12,
        color:'#555555',
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold'
            },
            android: {
                fontFamily: '$YekanBold'
            }
        }),
        marginTop:5
    },
    fileModalIcon:{
        color:'$MainColor',
        fontSize:50
    },
    renderedImage:{
        alignSelf:'center',
        marginVertical: 10,
        height:Dimensions.get('window').width / 2,
        maxWidth:Dimensions.get('window').width - 40,
        maxHeight:Dimensions.get('window').width / 2,
        resizeMode:'contain'
    }
});


export default styles;
