import React from 'react';
import {
    StatusBar,
    Image,
    Text,
    TouchableOpacity,
    View,
    FlatList,
    AsyncStorage,
    Linking,
    TextInput,
    ActivityIndicator
} from 'react-native';
import {Header, Icon, Left, Right, Content, Container, Body} from 'native-base';
import styles from './Style';
import { Actions } from 'react-native-router-flux';
import {setApp} from "../../redux/Actions/Index";
import {connect} from "react-redux";
import Picker from 'react-native-picker';

import HeaderRightLabel from "../Shared/HeaderRightLabel";
import BasketButton from "../Shared/BasketButton";
import BackButton from "../Shared/BackButton";
import SearchButton from "../Shared/SearchButton";
import LinearGradient from "react-native-linear-gradient";
import NumberFormat from "react-number-format";
import Helpers from "../Shared/Helper";
import BasketEmptyIcon from "../../assets/icon/BasketEmptyIcon";
import LoadingModal from "../Shared/LoadingModal";

class Cart extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            showLoadingModal:false,
            coupon:'',
            couponLoading:false,
            couponMessage:'',
            couponColor:''
        };

        this.newDate = props.global.newDate;

        this.renderFooter = this.renderFooter.bind(this);
        this.deleteFromCart = this.deleteFromCart.bind(this);
        this.updateCart = this.updateCart.bind(this);
        this.showPicker = this.showPicker.bind(this);

        this.addFavorite = this.addFavorite.bind(this);
        this.deleteFavorite = this.deleteFavorite.bind(this);

        this.closeCart = this.closeCart.bind(this);
        this.pay = this.pay.bind(this);

        this.onCouponChanged = this.onCouponChanged.bind(this);

        this.validateCoupon = this.validateCoupon.bind(this);
    }

    onCouponChanged(text) {
        this.setState({
            coupon:text
        });
    }

    renderItem({ item,index }) {
        return (
            <TouchableOpacity style={[styles.cartListItem,{marginTop:index === 0 ? 15 : 15,marginBottom:index + 1 === this.props.app.cart.length ? 15 : 0}]}
                  onPress={() => Actions.push('product',{product:item})} activeOpacity={.8} key={item.id} >
                <View style={styles.cartListItemLeft}>
                    {this.props.app.favorite.findIndex(a => a.id === item.id) === -1 && <TouchableOpacity activeOpacity={.7} style={styles.cartListItemLeftButton} onPress={() => this.addFavorite(item.id)}>
                        <Icon name='ios-heart' style={[styles.cartListItemLeftButtonIcon,{color:'#999999'}]}/>
                    </TouchableOpacity>}
                    {this.props.app.favorite.findIndex(a => a.id === item.id) > -1 && <TouchableOpacity activeOpacity={.7} style={styles.cartListItemLeftButton} onPress={() => this.deleteFavorite(item.id)}>
                        <Icon name='ios-heart' style={[styles.cartListItemLeftButtonIcon,{color:'#ff4141'}]}/>
                    </TouchableOpacity>}
                    <TouchableOpacity activeOpacity={.7} style={[styles.cartListItemLeftButton,{marginTop:10}]} onPress={() => this.deleteFromCart(item.id)}>
                        <Icon name='ios-close' style={[styles.cartListItemLeftButtonIcon,{fontSize:32}]}/>
                    </TouchableOpacity>
                </View>
                <View style={styles.cartListItemCenter}>
                    <Text style={styles.cartListItemCenterName} numberOfLines={2}>{item.title}</Text>

                    <TouchableOpacity onPress={() => this.showPicker(item.id)} activeOpacity={.7}>
                        <Text style={styles.cartListItemCenterCount} numberOfLines={1}>تعداد : {Helpers.ToPersianNumber(item.count.toString())} عدد</Text>
                    </TouchableOpacity>

                    {(item.sale_price !== '0' && item.price !== '' && item.price !== '0') && <NumberFormat value={item.sale_price !== '' ? item.sale_price * item.count : item.price * item.count} displayType={'text'} thousandSeparator={true} renderText={value =>
                        <Text style={styles.cartListItemCenterPrice}>{Helpers.ToPersianNumber(value)} تومان</Text>
                    }/>}
                    {(item.price === '' || item.price === '0' || item.sale_price === '0') && <Text style={styles.cartListItemCenterPrice}>رایگان</Text>}
                </View>
                <View style={styles.cartListItemRight}>
                    {item.image !== null && <Image resizeMode='contain' style={styles.cartListItemRightImage} source={{uri:item.image + '?ref =' + this.newDate}}/>}
                    {item.image === null && <Image resizeMode='contain' style={styles.cartListItemRightImage} source={require('./../../assets/images/shared/product.png')}/>}
                </View>
            </TouchableOpacity>
        );
    }

    async deleteFromCart(id){
        let cart = this.props.app.cart;

        let index = cart.findIndex(a => a.id === id);

        cart.splice(index,1);

        if(cart.length === 0){
            await AsyncStorage.removeItem("cart");
        }
        else {
            await AsyncStorage.setItem("cart",JSON.stringify(cart));
        }

        await this.props.setApp({
            ...this.props.app,
            cart
        });
    }
    async updateCart(data,index){

        let cart = this.props.app.cart;

        cart[index].count = parseInt(Helpers.ToEnglishNumber(data[0]));

        await AsyncStorage.setItem("cart",JSON.stringify(cart));

        await this.props.setApp({
            ...this.props.app,
            cart
        });
    }
    async showPicker(id) {
        let data = [];

        let cart = this.props.app.cart;
        let index = cart.findIndex(a => a.id === id);
        let product = cart[index];

        if(product.downloadable || (product.price === '' || product.price === '0' || product.sale_price === '0'))
        {
            data = ['۱'];
        }
        else {
            data = ['۱','۲','۳','۴','۵'];
        }

        Picker.init({
            pickerData: data,
            pickerConfirmBtnText:'تایید',
            pickerCancelBtnText:'لغو',
            pickerTitleText:'تعداد را انتخاب کنید',
            pickerConfirmBtnColor:[51, 153, 102,1],
            pickerCancelBtnColor:[218, 68, 83,1],
            pickerToolBarBg:[245, 244, 232,1],
            pickerFontFamily:'Vazir',
            pickerFontSize:18,
            selectedValue: [Helpers.ToPersianNumber(this.props.app.cart[index].count.toString())],
            onPickerConfirm: async data => {
                await this.updateCart(data,index);
            }
        });
        Picker.show();
    }

    async validateCoupon(){

        if(this.state.coupon === ''){
            await this.setState({
                couponMessage:'وارد کردن کد تخفیف الزامیست!',
                couponColor:'#db1623'
            });
            return;
        }

        await this.setState({
            couponLoading:true,
            couponMessage:''
        });

        let formData = new FormData();
        formData.append("admintoken", this.props.global.adminToken);
        formData.append("coupon", this.state.coupon);

        let response = await fetch(this.props.global.baseApiUrl + '/order/couponvalid',
            {
                method: "POST",
                body: formData
            });

        let json = await response.json();

        if(json.success){
            await this.setState({
                couponLoading:false,
                couponMessage:'کد تخفیف معتبر است.',
                couponColor:'#2fcc71'
            });
        }
        else {
            await this.setState({
                couponLoading:false,
                couponMessage:'کد تخفیف معتبر نیست.',
                couponColor:'#db1623'
            });
        }
    }

    renderFooter(){
        return(
            <View>
                <View style={styles.cartCoupon}>
                    <Text style={[styles.cartListFooterButtonText,{color:'#393939'}]}>کد یا کوپن تخفیف :</Text>
                    <View style={{flexDirection:'row',justifyContent:'center',alignItems:'center',marginTop:10}}>
                        {this.state.couponLoading && <ActivityIndicator color={this.props.global.grColorTwo} style={{marginRight:20}} />}
                        {!this.state.couponLoading && <TouchableOpacity style={{marginRight:20}} activeOpacity={.6} onPress={this.validateCoupon} hitSlop={{top: 10, left: 10, bottom: 10, right: 10}}>
                            <Text style={[styles.cartListFooterButtonText,{color:this.props.global.grColorOne,fontSize: 12}]}>اعتبارسنجی کد</Text>
                        </TouchableOpacity>}
                        <TextInput underlineColorAndroid="rgba(0,0,0,0)" style={styles.inputMain} onChangeText={this.onCouponChanged}/>
                    </View>
                    {this.state.couponMessage !== '' && <Text style={[styles.cartListFooterButtonText,{color:this.state.couponColor,marginTop:10,fontSize:12}]}>{this.state.couponMessage}</Text>}
                </View>
                <View style={styles.cartListFooter}>
                    <TouchableOpacity style={styles.colorGrButton} activeOpacity={.8} onPress={() => Actions.pop()}>
                        <LinearGradient colors={[this.props.global.grColorOne,this.props.global.grColorTwo]} style={styles.grButtonGradient} start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}>
                            <Text style={styles.cartListFooterButtonText}>ادامه خرید</Text>
                        </LinearGradient>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.colorGrButton,{marginLeft:20,shadowColor:'#2ecc71'}]} activeOpacity={.8} onPress={this.pay}>
                        <LinearGradient colors={['#11d462','#0ca64c']} style={styles.grButtonGradient} start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}>
                            <Text style={styles.cartListFooterButtonText}>پرداخت</Text>
                        </LinearGradient>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }

    async pay(){

        if(this.props.user.apiToken === null){
            Actions.push('intro',{initialIndex:1});
            return;
        }

        if(this.props.app.cart.filter(a => a.downloadable === false).length > 0){
            Actions.push('selectAddress',{coupon:this.state.coupon});
        }
        else {
            this.closeCart();
        }
    }

    async closeCart(){
        await this.setState({
            showLoadingModal:true
        });

        try {
            let notFreeItemIndex = this.props.app.cart.findIndex(a => a.sale_price !== '0' && a.price !== '' && a.price !== '0');

            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("usertoken", this.props.user.apiToken);
            //formData.append("add_id", id);

            this.props.app.cart.map(item => {
                formData.append("product_item[" + item.id + "]", item.count);
            });

            if(this.state.coupon !== ''){
                formData.append("coupon", this.state.coupon);
            }

            let response = await fetch(this.props.global.baseApiUrl + '/order/create',
                {
                    method: "POST",
                    body: formData
                });

            let json = await response.json();

            if(response.status === 200 && json.success && notFreeItemIndex > -1)
            {
                let formData2 = new FormData();
                formData2.append("admintoken", this.props.global.adminToken);
                formData2.append("usertoken", this.props.user.apiToken);
                formData2.append("order_id", json.data.order_id);

                let response2 = await fetch(this.props.global.baseApiUrl + '/order/payorder',
                    {
                        method: "POST",
                        body: formData2
                    });

                let json2 = await response2.json();

                if(response2.status === 200 && json2.success)
                {
                    await this.props.setApp({
                        ...this.props.app,
                        cart:[]
                    });
                    await AsyncStorage.removeItem("cart");

                    this.setState({
                        showLoadingModal: false
                    });
                    Linking.openURL(this.props.global.baseApiUrl + "/order/paybyurl/?payurl=" + json2.data.payurl);
                    Actions.reset('root');
                }
                else {
                    this.setState({
                        showLoadingModal: false
                    });
                }

            }
            else {
                if(notFreeItemIndex === -1){
                    await this.props.setApp({
                        ...this.props.app,
                        cart:[]
                    });
                    await AsyncStorage.removeItem("cart");
                    this.setState({
                        showLoadingModal: false
                    });
                    Actions.reset('root');
                }
                else {
                    this.setState({
                        showLoadingModal: false
                    });
                }
            }

        } catch (error) {
            console.log(error);
            this.setState({
                showLoadingModal: false
            });
        }
    }

    async addFavorite(id){

        await this.setState({
            showLoadingModal:true
        });

        try {
            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("usertoken", this.props.user.apiToken);
            formData.append("product_id", id);

            let response = await fetch(this.props.global.baseApiUrl + '/product/favpadd',
                {
                    method: "POST",
                    body: formData
                });

            if (response.status !== 200) {
                await this.setState({
                    showLoadingModal:false
                });
            }
            else {

                let json = await response.json();

                if(json.success){

                    let favorite = [];

                    json.data.map(item => {
                        item.id = parseInt(item.id);
                        favorite.push(item);
                    });

                    await this.props.setApp({
                        ...this.props.app,
                        favorite:favorite
                    });
                    await this.setState({
                        showLoadingModal:false
                    });
                }
                else {
                    await this.setState({
                        showLoadingModal:false
                    });
                }
            }

        } catch (error) {
            //console.log(error);
            await this.setState({
                showLoadingModal:false
            });
        }
    }
    async deleteFavorite(id){
        await this.setState({
            showLoadingModal:true
        });

        try {
            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("usertoken", this.props.user.apiToken);
            formData.append("product_id", id);

            let response = await fetch(this.props.global.baseApiUrl + '/product/favpdel',
                {
                    method: "POST",
                    body: formData
                });

            if (response.status !== 200) {
                await this.setState({
                    showLoadingModal:false
                });
            }
            else {

                let json = await response.json();

                if(json.success){

                    if(json.data === "It's deleted")
                    {
                        await this.props.setApp({
                            ...this.props.app,
                            favorite:[]
                        });
                    }
                    else {
                        let favorite = [];

                        json.data.map(item => {
                            item.id = parseInt(item.id);
                            favorite.push(item);
                        });

                        await this.props.setApp({
                            ...this.props.app,
                            favorite:favorite
                        });
                    }
                    await this.setState({
                        showLoadingModal:false
                    });
                }
                else {
                    await this.setState({
                        showLoadingModal:false
                    });
                }
            }

        } catch (error) {
            //console.log(error);
            await this.setState({
                showLoadingModal:false
            });
        }
    }

    render() {

        return (
            <Container style={styles.mainContainer}>
                <Image resizeMode='stretch' style={[styles.mainBackground,{height:1000}]} source={require("./../../assets/images/shared/menu-bg.png")} />

                <Header style={styles.header}>
                    <Left style={{flex:1,flexDirection:'row',alignItems:'center',justifyContent:'flex-start'}}>
                        <BackButton/>
                        <SearchButton/>
                    </Left>
                    <Right style={{flex:2,alignItems:'center',justifyContent:'flex-end'}}>
                        <HeaderRightLabel title='سبد خرید'/>
                    </Right>
                </Header>

                <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content" />

                {this.props.app.cart.length > 0 && <View style={{backgroundColor:'#ffffff',flex:1}}>
                    <FlatList
                        data={this.props.app.cart}
                        renderItem={this.renderItem.bind(this)}
                        ListFooterComponent={this.renderFooter}
                        keyExtractor={(item) => item.id.toString()}
                        style={{backgroundColor:'transparent'}}
                        showsVerticalScrollIndicator={false}
                    />
                </View>}

                {this.props.app.cart.length === 0 && <View style={{backgroundColor:'#ffffff',flex:1,justifyContent:'center',alignItems:'center'}}>
                    <View>
                        <BasketEmptyIcon/>
                        <View style={styles.emptyCartBadge}>
                            <Text style={styles.emptyCartBadgeText}>۰</Text>
                        </View>
                    </View>
                    <Text style={styles.emptyCartTitleText}>سبد خرید خالی است</Text>
                    <Text style={styles.emptyCartDescText}>یک محصول را به سبد خرید اضافه کنید.</Text>

                    <TouchableOpacity style={[styles.colorGrButton,{marginTop:80}]} activeOpacity={.8} onPress={() => Actions.push('home')}>
                        <LinearGradient colors={[this.props.global.grColorOne,this.props.global.grColorTwo]} style={styles.grButtonGradient} start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}>
                            <Text style={styles.grButtonText}>شروع به خرید کنید</Text>
                        </LinearGradient>
                    </TouchableOpacity>
                </View>}

                <LoadingModal show={this.state.showLoadingModal} />
            </Container>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setApp : app => {
            dispatch(setApp(app))
        }
    }
};

const mapStateToProps =(state) =>  {
    return{
        user: state.user,
        global:state.global,
        app:state.app
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(Cart);