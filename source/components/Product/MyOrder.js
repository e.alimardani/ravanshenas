import React from 'react';
import {
    StatusBar,
    Image,
    Text,
    View,
    FlatList,
    PixelRatio,
    TouchableOpacity,
    ActivityIndicator,
    AsyncStorage, Linking
} from 'react-native';
import {Header, Icon, Left, Right, Content, Container, Body} from 'native-base';
import styles from './Style';
import { Actions } from 'react-native-router-flux';
import {setUser} from "../../redux/Actions/Index";
import {connect} from "react-redux";
import HeaderRightLabel from "../Shared/HeaderRightLabel";
import BasketButton from "../Shared/BasketButton";
import BackButton from "../Shared/BackButton";
import SearchButton from "../Shared/SearchButton";
import Helpers from "../Shared/Helper";
import NumberFormat from "react-number-format";
import LoadingModal from "../Shared/LoadingModal";
import moment from 'jalali-moment';

class MyOrder extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            orders:[],
            loading:true,
            refreshing:false,
            showLoadingModal:false
        };

        this.newDate = props.global.newDate;

        this.getOrdersRequest = this.getOrdersRequest.bind(this);
        this.pay = this.pay.bind(this);
        this.goToProduct = this.goToProduct.bind(this);
    }

    async componentWillMount(){
        await this.getOrdersRequest();
    }

    async getOrdersRequest(){
        //console.log(this.state);
        if(await Helpers.CheckNetInfo()){
            return;
        }

        try {

            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("usertoken", this.props.user.apiToken);

            let response = await fetch(this.props.global.baseApiUrl + '/order/customerorders',
                {
                    method: "POST",
                    body: formData
                });

            if (response.status !== 200) {
                await this.setState({
                    loading:false,
                    refreshing:false
                });
            }
            else {

                let json = await response.json();

                if(json.success){

                    await this.setState({
                        orders: json.data,
                        loading: false,
                        refreshing: false,
                    });

                }
                else {
                    await this.setState({
                        loading:false,
                        refreshing:false
                    });
                }
            }

        } catch (error) {
            //console.log(error);
            await this.setState({
                loading: false,
                refreshing:false
            })
        }
    }

    async goToProduct(id){
        if(await Helpers.CheckNetInfo()){
            return;
        }

        await this.setState({
            showLoadingModal:true
        });

        try {
            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("product_id", id);

            let response = await fetch(this.props.global.baseApiUrl + '/product/getpbyid',
                {
                    method: "POST",
                    body: formData
                });

            if (response.status !== 200) {
                await this.setState({
                    showLoadingModal:false
                });
            }
            else {

                let json = await response.json();

                if(json.success){

                    let product = json.data;

                    product.id = parseInt(product.id);

                    await this.setState({
                        showLoadingModal:false
                    });
                    Actions.push('product',{product})
                }
                else {
                    await this.setState({
                        showLoadingModal:false
                    });
                }
            }

        } catch (error) {
            await this.setState({
                showLoadingModal:false
            });
        }
    }

    renderItem({ item,index }) {

        let totalAmount = 0;

        item.products.map(product => {
            totalAmount += parseInt(product.price) * product.quantity
        });

        return (
            <View style={[styles.addressListItem,{marginTop:index === 0 ? 15 : 20,marginBottom:index + 1 === this.state.orders.length ? 15 : 0}]} key={item.order_id} >
                <View style={styles.addressListItemHeader}>
                    <Text style={styles.addressListItemHeaderText}>شماره فاکتور : {Helpers.ToPersianNumber(item.order_id.toString())}</Text>
                </View>
                <View style={styles.addressListItemAddress}>
                    {item.products.map((item2,index) => {
                        return(
                            <TouchableOpacity key={item2.id} style={{flex:1,flexDirection:'row',justifyContent:'center',alignItems:'center',marginTop: 10}}
                                              activeOpacity={.9} onPress={() => this.goToProduct(item2.id)}>
                                <View style={{flex:1,marginRight: 15}}>
                                    <Text style={styles.orderListProductText} numberOfLines={1}>{item2.name}</Text>
                                    <Text style={styles.orderListProductCount}>تعداد : {Helpers.ToPersianNumber(item2.quantity.toString())}</Text>
                                    {item2.price !== '0' && <NumberFormat value={item2.price} displayType={'text'} thousandSeparator={true} renderText={value =>
                                        <Text style={styles.orderListProductPrice}>قیمت واحد : {Helpers.ToPersianNumber(value)} تومان</Text>
                                    }/>}
                                    {item2.price === '0' && <Text style={[styles.orderListProductPrice,{marginTop:10}]}>رایگان</Text>}
                                </View>
                                <View style={{flex:.5,justifyContent:'center',alignItems:'center'}}>
                                    {(item2.image !== null && item2.image !== false) && <Image resizeMode='contain' style={styles.orderListProductImage} source={{uri:item2.image + '?ref =' + this.newDate}}/>}
                                    {(item2.image === null || item2.image === false ) && <Image resizeMode='contain' style={styles.orderListProductImage} source={require('./../../assets/images/shared/product.png')}/>}
                                </View>
                            </TouchableOpacity>
                        );
                    })}
                </View>
                <View style={styles.addressListItemPhone}>
                    <View style={{flex:1,justifyContent:'center',alignItems:'center',paddingVertical:5}}>
                        <Text style={[styles.addressListItemAddressText,{textAlign:'center'}]}>مبلغ کل : <NumberFormat value={totalAmount} displayType={'text'} thousandSeparator={true} renderText={value =>
                            <Text style={{color:'#1fc792',fontSize:14}}>{'\n' + Helpers.ToPersianNumber(value)} تومان</Text>
                        }/></Text>
                    </View>
                    <View style={{flex:1,justifyContent:'center',alignItems:'center',borderLeftWidth:5/PixelRatio.get(), borderLeftColor:'#eceff1',paddingVertical:5}}>
                        <Text style={[styles.addressListItemAddressText,{textAlign:'center'}]}>تاریخ :{'\n' + Helpers.ToPersianNumber(moment(item.create_at).locale('fa').format('YYYY/MM/DD'))}</Text>
                    </View>
                </View>
                <View style={[styles.addressListItemButtonSection,{flexDirection:'column'}]}>
                    <View style={styles.orderListStatus}>
                        <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                            <Text style={[styles.orderListStatusText,{color:Helpers.ReturnStatusColor(item.status)}]}>{Helpers.ReturnStatus(item.status)}</Text>
                        </View>
                        <View style={{flex:1,flexDirection: 'row',justifyContent:'center',alignItems:'center'}}>
                            <Text style={[styles.orderListStatusText,{marginRight:10,color:'#393939'}]}>وضعیت سفارش :</Text>

                            <Icon style={[styles.orderListStatusIcon,{color:Helpers.ReturnStatusColor(item.status)}]} name={Helpers.ReturnStatusIcon(item.status)}/>
                        </View>
                    </View>
                    {item.status === "pending" && <TouchableOpacity activeOpacity={.7} style={[styles.favoriteEditButton,{marginTop:10,borderColor:'#1fc792'}]} onPress={() => this.pay(item.order_id)}>
                        <Text style={[styles.productHorizontalSectionHeaderMoreText,{fontSize:12,color:this.props.global.grColorTwo}]}>پرداخت فاکتور</Text>
                    </TouchableOpacity>}
                </View>
            </View>
        );
    }
    handleEmpty() {
        if(this.state.loading)
        {
            return(
                <ActivityIndicator style={styles.listLoadingIndicator} color={this.props.global.grColorTwo} size='large' />
            );
        }
        else
        {
            return(
                <Text style={styles.notFoundText}>سابقه خریدی یافت نشد.</Text>
            );
        }
    }
    async handleRefresh() {
        if(await Helpers.CheckNetInfo()){
            return;
        }

        await this.setState({ refreshing : true } ,async () => {
            await this.getOrdersRequest();
        });
    }

    async pay(id){
        await this.setState({
            showLoadingModal:true
        });

        try {

            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("usertoken", this.props.user.apiToken);
            formData.append("order_id", id);

            let response = await fetch(this.props.global.baseApiUrl + '/order/payorder',
                {
                    method: "POST",
                    body: formData
                });

            let json = await response.json();

            if (response.status === 200 && json.success) {

                this.setState({
                    showLoadingModal: false
                });
                Linking.openURL(this.props.global.baseApiUrl + "/order/paybyurl/?payurl=" + json.data.payurl);
                Actions.pop();
            }
            else {
                this.setState({
                    showLoadingModal: false
                });
            }

        } catch (error) {
            console.log(error);
            this.setState({
                showLoadingModal: false
            });
        }
    }

    render() {

        return (
            <Container style={styles.mainContainer}>
                <Image resizeMode='stretch' style={[styles.mainBackground,{height:1000}]} source={require("./../../assets/images/shared/menu-bg.png")} />

                <Header style={styles.header}>
                    <Left style={{flex:1,flexDirection:'row',alignItems:'center',justifyContent:'flex-start'}}>
                        <BackButton/>
                        <SearchButton/>
                        <BasketButton/>
                    </Left>
                    <Right style={{flex:2,alignItems:'center',justifyContent:'flex-end'}}>
                        <HeaderRightLabel title='سوابق خرید'/>
                    </Right>
                </Header>

                <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content" />

                <View style={[styles.productHorizontalSection,{paddingTop:0,flex:1,paddingVertical:0}]}>
                    <FlatList
                        data={this.state.orders}
                        renderItem={this.renderItem.bind(this)}
                        keyExtractor={(item) => item.order_id.toString()}
                        style={styles.addressList}
                        showsVerticalScrollIndicator={false}
                        ListEmptyComponent={this.handleEmpty.bind(this)}
                        refreshing={this.state.refreshing}
                        onRefresh={this.handleRefresh.bind(this)}
                        removeClippedSubviews={true}
                    />
                </View>

                <LoadingModal show={this.state.showLoadingModal} />
            </Container>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setUser : user => {
            dispatch(setUser(user))
        }
    }
};

const mapStateToProps =(state) =>  {
    return{
        user: state.user,
        global:state.global
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(MyOrder);