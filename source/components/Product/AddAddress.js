import React from 'react';
import {
    StatusBar,
    Image,
    TextInput,
    ActivityIndicator,
    Text,
    Dimensions,
    TouchableOpacity,
    View,
    AsyncStorage,
} from 'react-native';
import {Header, Icon, Left, Right, Content, Container, Body} from 'native-base';
import styles from './Style';
import { Actions } from 'react-native-router-flux';
import {setUser} from "../../redux/Actions/Index";
import {connect} from "react-redux";
import HeaderRightLabel from "../Shared/HeaderRightLabel";
import BasketButton from "../Shared/BasketButton";
import BackButton from "../Shared/BackButton";
import SearchButton from "../Shared/SearchButton";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import OtherPasswordIcon from "../../assets/icon/OtherPasswordIcon";
import EditProfilePersonIcon from "../../assets/icon/EditProfilePersonIcon";
import EditProfileCalendarIocn from "../../assets/icon/EditProfileCalendarIocn";
import EditProfileEmailIcon from "../../assets/icon/EditProfileEmailIcon";
import EditProfileMobileIcon from "../../assets/icon/EditProfileMobileIcon";
import LinearGradient from "react-native-linear-gradient";
import Helpers from "../Shared/Helper";
import RequestModal from "../Shared/RequestModal";
import ProfileAddressIcon from "../../assets/icon/ProfileAddressIcon";


class AddAddress extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            fullName:'',
            postalCode:'',
            city:'',
            address:'',
            phoneNumber:'',
            //Request Modal
            showModal:false,
            hasError:false,
            errorDesc:'',
            loading:false
        };

        this.onFullNameChanged = this.onFullNameChanged.bind(this);
        this.onPostCodeChanged = this.onPostCodeChanged.bind(this);
        this.onCityChanged = this.onCityChanged.bind(this);
        this.onAddressChanged = this.onAddressChanged.bind(this);
        this.onPhoneNumberChanged = this.onPhoneNumberChanged.bind(this);

        this.addAddress = this.addAddress.bind(this);
    }

    onFullNameChanged(text) {
        this.setState({
            fullName:text
        });
    }
    onPostCodeChanged(text) {
        this.setState({
            postalCode:text
        });
    }
    onCityChanged(text) {
        this.setState({
            city:text
        });
    }
    onAddressChanged(text) {
        this.setState({
            address:text
        });
    }
    onPhoneNumberChanged(text) {
        this.setState({
            phoneNumber:text
        });
    }

    async addAddress(){
        if(await Helpers.CheckNetInfo()){
            return;
        }

        await this.setState({
            showModal:true,
            loading:true,
            hasError:false,
            errorDesc:''
        });

        //Validation
        let errors = '';

        if(this.state.fullName === '')
        {
            errors += 'تحویل گیرنده نمیتواند خالی باشد. \n'
        }

        if(this.state.phoneNumber === '')
        {
            errors += 'شماره موبایل نمیتواند خالی باشد. \n'
        }

        if(this.state.city === '')
        {
            errors += 'شهر نمیتواند خالی باشد. \n'
        }

        if(this.state.address === '')
        {
            errors += 'آدرس نمیتواند خالی باشد. \n'
        }

        if(errors !== '')
        {
            await this.setState({
                loading:false,
                hasError:true,
                errorDesc:errors
            });
        }
        else {
            //Request
            try {
                let formData = new FormData();
                formData.append("admintoken", this.props.global.adminToken);
                formData.append("usertoken", this.props.user.apiToken);

                formData.append("address", this.state.address);
                formData.append("city", this.state.city);

                if(this.state.postalCode !== ''){
                    formData.append("postcode", this.state.postalCode);
                }

                formData.append("phone_number", this.state.phoneNumber);
                formData.append("fullname", this.state.fullName);

                let response = await fetch(this.props.global.baseApiUrl + '/address/add',
                    {
                        method: "POST",
                        body: formData
                    });

                let json = await response.json();

                if(response.status === 200 && json.success)
                {
                    await this.props.setter({
                        address:json.data
                    });

                    await this.setState({
                        loading:false,
                        hasError: false,
                        errorDesc: 'با موفقیت انجام شد.'
                    });

                    setTimeout(async () => {
                        await this.setState({
                            showModal:false
                        });
                        Actions.pop();
                    },1500)
                }
                else {
                    let messages = '';

                    if(json.messages.length > 0){
                        json.messages.forEach(item => {
                            messages += item + '\n'
                        })
                    }

                    await this.setState({
                        loading: false,
                        hasError: true,
                        errorDesc: messages
                    });
                }

            } catch (error) {
                //console.log(error);
                this.setState({
                    loading: false,
                    hasError: true,
                    errorDesc: 'ارتباط با سرور برقرار نشد!' + '\n' + 'دوباره تلاش کنید'
                });
            }
        }
    }


    render() {

        return (
            <Container style={styles.mainContainer}>
                <Image resizeMode='stretch' style={[styles.mainBackground,{height:1000}]} source={require("./../../assets/images/shared/menu-bg.png")} />

                <Header style={styles.header}>
                    <Left style={{flex:1,flexDirection:'row',alignItems:'center',justifyContent:'flex-start'}}>
                        <BackButton/>
                        <SearchButton/>
                        <BasketButton/>
                    </Left>
                    <Right style={{flex:2,alignItems:'center',justifyContent:'flex-end'}}>
                        <HeaderRightLabel title='افزودن آدرس'/>
                    </Right>
                </Header>

                <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content" />

                <KeyboardAwareScrollView style={{flex:1,backgroundColor:'#ffffff'}} keyboardShouldPersistTaps={'handled'}>
                    <Text style={styles.profileSectionTitle}>آدرس</Text>
                    <View style={styles.profileFormContainer}>
                        <View style={styles.formRow}>
                            <TextInput underlineColorAndroid="rgba(0,0,0,0)" style={styles.textInput}
                                       placeholderTextColor="#cccccc" placeholder='تحویل گیرنده' value={this.state.fullName}
                                       onChangeText={this.onFullNameChanged}/>
                            <EditProfilePersonIcon/>
                        </View>
                        <View style={styles.formRow}>
                            <TextInput underlineColorAndroid="rgba(0,0,0,0)" style={styles.textInput}
                                       placeholderTextColor="#cccccc" placeholder='شماره موبایل' value={this.state.phoneNumber}
                                       onChangeText={this.onPhoneNumberChanged} keyboardType="number-pad" maxLength={11}/>
                            <EditProfileMobileIcon/>
                        </View>
                        <View style={styles.formRow}>
                            <TextInput underlineColorAndroid="rgba(0,0,0,0)" style={styles.textInput}
                                       placeholderTextColor="#cccccc" placeholder='کد پستی' value={this.state.postalCode}
                                       keyboardType="number-pad" maxLength={10} onChangeText={this.onPostCodeChanged}/>
                            <EditProfileEmailIcon/>
                        </View>
                        <View style={styles.formRow}>
                            <TextInput underlineColorAndroid="rgba(0,0,0,0)" style={styles.textInput}
                                       placeholderTextColor="#cccccc" placeholder='شهر' value={this.state.city}
                                       onChangeText={this.onCityChanged} />
                            <ProfileAddressIcon/>
                        </View>
                        <View style={styles.formRow}>
                            <TextInput underlineColorAndroid="rgba(0,0,0,0)" style={styles.textInput} onChangeText={this.onAddressChanged}
                                       placeholderTextColor="#cccccc" placeholder='آدرس' value={this.state.address}/>
                            <ProfileAddressIcon/>
                        </View>

                        <TouchableOpacity style={[styles.colorGrButton,{marginTop:20}]} activeOpacity={.8} onPress={this.addAddress}>
                            <LinearGradient colors={[this.props.global.grColorOne,this.props.global.grColorTwo]} style={styles.grButtonGradient} start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}>
                                <Text style={styles.grButtonText}>افزودن آدرس</Text>
                            </LinearGradient>
                        </TouchableOpacity>
                    </View>
                </KeyboardAwareScrollView>

                <RequestModal showModal={this.state.showModal} setter={this} loading={this.state.loading} hasError={this.state.hasError} errorDesc={this.state.errorDesc}/>

            </Container>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setUser : user => {
            dispatch(setUser(user))
        }
    }
};

const mapStateToProps =(state) =>  {
    return{
        user: state.user,
        global:state.global
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(AddAddress);