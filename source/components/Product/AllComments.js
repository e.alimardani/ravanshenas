import React from 'react';
import {
    StatusBar,
    Image,
    Text,
    TouchableOpacity,
    View,
    AsyncStorage,
    ActivityIndicator,
    FlatList,
    PixelRatio,
    Linking,
    Platform,
    ScrollView,
    TextInput
} from 'react-native';
import RNFetchBlob from 'rn-fetch-blob';
import {Header, Icon, Left, Right, Content, Container, Body} from 'native-base';
import styles from './Style';
import { Actions } from 'react-native-router-flux';
import {setApp} from "../../redux/Actions/Index";
import {connect} from "react-redux";
import HeaderRightLabel from "../Shared/HeaderRightLabel";
import BasketButton from "../Shared/BasketButton";
import BackButton from "../Shared/BackButton";
import SearchButton from "../Shared/SearchButton";
import LinearGradient from "react-native-linear-gradient";
import Swiper from "react-native-swiper";
//import ViewMoreText from 'react-native-view-more-text';
import NumberFormat from "react-number-format";
import Helpers from "../Shared/Helper";
import LoadingModal from "../Shared/LoadingModal";
import Picker from "react-native-picker";
import Modal from "react-native-modal";
import * as Progress from 'react-native-progress';
import HTML from 'react-native-render-html';
import moment from 'jalali-moment';

class AllComments extends React.Component {

    constructor(props) {
        super(props);

        this.newDate = props.global.newDate;

        this.state = {
            Comment:'',
            Comments:[],
            loadingComment:true
        }
        
    }
    componentDidMount(){
        console.log(this.props.data)
    }
    renderItem(item){
        console.log(item)
        return(
            <View style={{padding:10,borderBottomColor:'#f1f4f7',borderBottomWidth:1,paddingBottom:20}}>
                <View style={{flexDirection:'row-reverse',alignItems:'center',marginVertical:5}}>
                    <Image resizeMode='contain' style={{height:60,width:60,borderRadius:30}} source={require("./../../assets/images/shared/avatar.png")} />
                    <View style={{marginRight:10,alignItems:'flex-end',justifyContent:'center',paddingBottom:5}}>
                        {item.item.first_name != null && <Text style={{fontFamily:'IRANSansMobileBold',fontSize:16,color:'#3399db'}}>{item.item.first_name+" "+item.item.last_name}</Text>}
                        {item.item.first_name == null && <Text style={{fontFamily:'IRANSansMobileBold',fontSize:16,color:'#3399db'}}>ناشناس</Text>}
                        <Text style={{fontFamily:'IRANSansMobile',fontSize:10}}>{Helpers.ToPersianNumber(moment(item.item.comment_date, 'YYYY-MM-DD').locale('fa').format('DD MMMM YYYY'))}</Text>
                    </View>
                </View>
                <View>
                    <Text style={{fontFamily:'IRANSansMobileBold',fontSize:13,textAlign:'auto',paddingBottom:10,color:'black'}}>{item.item.comment_content}</Text>
                </View>
            </View>
        )
        
    }
    render() {
        return (
            <Container style={styles.mainContainer}>
                <Image resizeMode='stretch' style={[styles.mainBackground,{height:1000}]} source={require("./../../assets/images/shared/menu-bg.png")} />

                <Header style={styles.header}>
                    <Left style={{flex:1,flexDirection:'row',alignItems:'center',justifyContent:'flex-start'}}>
                        <BackButton/>
                        <BasketButton/>
                    </Left>
                    <Right style={{flex:2,alignItems:'center',justifyContent:'flex-end'}}>
                        <HeaderRightLabel title={'همه دیدگاه ها'}/>
                    </Right>
                </Header>

                <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content" />

                <Content style={{flex:1,backgroundColor:'#ffffff'}}>                    
                    <View style={[styles.productDescInfoContainer,{borderColor:'white'}]}>
                            <View>
                                <FlatList
                                    data={this.props.data}
                                    renderItem={this.renderItem.bind(this)}
                                    keyExtractor={(item) => item.comment_ID.toString()}
                                    style={{backgroundColor:'transparent'}}
                                    showsVerticalScrollIndicator={false}
                                />
                            </View>
                        </View>
                </Content>
            </Container>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setApp : app => {
            dispatch(setApp(app))
        }
    }
};

const mapStateToProps =(state) =>  {
    return{
        user: state.user,
        global:state.global,
        app:state.app
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(AllComments);