import React from 'react';
import {
    StatusBar,
    Image,
    ActivityIndicator,
    Text,
    TouchableOpacity,
    View,
    FlatList,
    PixelRatio,
    TextInput
} from 'react-native';
import {Header, Icon, Left, Right, Content, Container, Body} from 'native-base';
import styles from './Style';
import { Actions } from 'react-native-router-flux';
import {setApp, setUser} from "../../redux/Actions/Index";
import {connect} from "react-redux";
import HeaderRightLabel from "../Shared/HeaderRightLabel";
import BasketButton from "../Shared/BasketButton";
import BackButton from "../Shared/BackButton";
import SearchButton from "../Shared/SearchButton";
import Helpers from "../Shared/Helper";
import NumberFormat from "react-number-format";
import LoadingModal from "../Shared/LoadingModal";

class ProductSearch extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            products:[],
            page:1,
            hasMore:true,
            loading:true,
            refreshing:false,
            showLoadingModal:false,
            query:props.query ? props.query === undefined ? '' : props.query : ''
        };

        this.newDate = props.global.newDate;

        this.getProductsRequest = this.getProductsRequest.bind(this);
        this.search = this.search.bind(this);
        this.onQueryChanged = this.onQueryChanged.bind(this);

        this.addFavorite = this.addFavorite.bind(this);
        this.deleteFavorite = this.deleteFavorite.bind(this);
    }

    async componentWillMount() {
        await this.getProductsRequest();
    }

    async getProductsRequest(){
        //console.log(this.state);
        if(await Helpers.CheckNetInfo()){
            return;
        }

        if(this.state.query === ''){
            await this.setState({
                loading: false
            });
            return;
        }

        try {
            let page = this.state.page;

            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("perpage", '10');
            formData.append("offset", page);
            formData.append("type", "query");
            formData.append("search_query", this.state.query);

            let response = await fetch(this.props.global.baseApiUrl + '/product/all',
                {
                    method: "POST",
                    body: formData
                });

            if (response.status !== 200) {
                await this.setState({
                    loading:false,
                    refreshing:false,
                    hasMore:false
                });
            }
            else {

                let json = await response.json();

                if(json.success){

                    await this.setState(prevState => {
                        return {
                            products: page === 1 ? json.data : [...prevState.products, ...json.data],
                            loading: false,
                            refreshing:false,
                            //hasMore:true
                        }
                    });

                }
                else {
                    await this.setState({
                        loading:false,
                        hasMore:false,
                        refreshing:false
                    });
                }
            }

        } catch (error) {
            console.log(error);
            await this.setState({
                loading: false,
                refreshing:false,
                hasMore:false
            })
        }
    }

    renderItem({ item,index }) {
        return (
            <TouchableOpacity style={[styles.productListItem,{marginTop:index === 0 ? 15 : 15,marginBottom:index + 1 === this.state.products.length ? 15 : 0}]} key={item.id}
                              onPress={() => Actions.push('product',{product:item})} activeOpacity={.7}>
                <View style={styles.productListItemLeft}>
                    {this.props.app.favorite.findIndex(a => a.id === item.id) === -1 && <TouchableOpacity activeOpacity={.7} style={styles.productListItemLeftButton} onPress={() => this.addFavorite(item.id)}>
                        <Icon name='ios-heart' style={[styles.productListItemLeftButtonIcon,{color:'#999999'}]}/>
                    </TouchableOpacity>}
                    {this.props.app.favorite.findIndex(a => a.id === item.id) > -1 && <TouchableOpacity activeOpacity={.7} style={styles.productListItemLeftButton} onPress={() => this.deleteFavorite(item.id)}>
                        <Icon name='ios-heart' style={[styles.productListItemLeftButtonIcon,{color:'#ff4141'}]}/>
                    </TouchableOpacity>}
                </View>
                <View style={styles.productListItemCenter}>
                    <Text style={styles.productListItemCenterName} numberOfLines={2}>{item.title}</Text>
                    {(item.sale_price !== '') && <NumberFormat value={item.price} displayType={'text'} thousandSeparator={true} renderText={value =>
                        <Text style={styles.productListItemCenterOldPrice}>{Helpers.ToPersianNumber(value)} تومان</Text>
                    }/>}
                    {(item.sale_price !== '0' && item.price !== '' && item.price !== '0') && <NumberFormat value={item.sale_price !== '' ? item.sale_price : item.price} displayType={'text'} thousandSeparator={true} renderText={value =>
                        <Text style={[styles.productListItemCenterPrice,{marginTop:item.sale_price !== '' ? 10 : 0}]}>{Helpers.ToPersianNumber(value)} تومان</Text>
                    }/>}
                    {(item.price === '' || item.price === '0' || item.sale_price === '0') && <Text style={[styles.productListItemCenterPrice,{marginTop:10}]}>رایگان</Text>}
                </View>
                <View style={styles.productListItemRight}>
                    {item.image !== null && <Image resizeMode='contain' style={styles.productListItemRightImage} source={{uri:item.image + '?ref =' + this.newDate}}/>}
                    {item.image === null && <Image resizeMode='contain' style={styles.productListItemRightImage} source={require('./../../assets/images/shared/product.png')}/>}
                </View>
            </TouchableOpacity>
        );
    }
    handleEmpty() {
        if(this.state.loading)
        {
            return(
                <ActivityIndicator style={styles.listLoadingIndicator} color={this.props.global.grColorTwo} size='large' />
            );
        }
        else if(this.state.query !== '')
        {
            return(
                <Text style={styles.notFoundText}>کالایی یافت نشد.</Text>
            );
        }
        else
        {
            return(
                <Text style={styles.notFoundText}>دنبال چیزی می‌گردی؟</Text>
            );
        }
    }
    renderFooter() {
        if(this.state.loading && this.state.products.length > 0)
        {
            return <ActivityIndicator style={{alignSelf: 'center',marginBottom: 20}} color={this.props.global.grColorTwo} />
        }
        else {
            return null;
        }
    }
    async handleRefresh() {
        if(await Helpers.CheckNetInfo()){
            return;
        }

        await this.setState({ page : 1 , refreshing : true ,hasMore:true} ,async () => {
            await this.getProductsRequest();
        });
    }
    async handleLoadMore() {
        //console.log('called loadmore');
        if(this.state.hasMore) {
            await this.setState({page : this.state.page + 1 , loading : true}, async () => {
                await this.getProductsRequest()
            })
        }
    }

    async addFavorite(id){

        await this.setState({
            showLoadingModal:true
        });

        try {
            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("usertoken", this.props.user.apiToken);
            formData.append("product_id", id);

            let response = await fetch(this.props.global.baseApiUrl + '/product/favpadd',
                {
                    method: "POST",
                    body: formData
                });

            if (response.status !== 200) {
                await this.setState({
                    showLoadingModal:false
                });
            }
            else {

                let json = await response.json();

                if(json.success){

                    let favorite = [];

                    json.data.map(item => {
                        item.id = parseInt(item.id);
                        favorite.push(item);
                    });

                    await this.props.setApp({
                        ...this.props.app,
                        favorite:favorite
                    });
                    await this.setState({
                        showLoadingModal:false
                    });
                }
                else {
                    await this.setState({
                        showLoadingModal:false
                    });
                }
            }

        } catch (error) {
            //console.log(error);
            await this.setState({
                showLoadingModal:false
            });
        }
    }
    async deleteFavorite(id){
        await this.setState({
            showLoadingModal:true
        });

        try {
            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("usertoken", this.props.user.apiToken);
            formData.append("product_id", id);

            let response = await fetch(this.props.global.baseApiUrl + '/product/favpdel',
                {
                    method: "POST",
                    body: formData
                });

            if (response.status !== 200) {
                await this.setState({
                    showLoadingModal:false
                });
            }
            else {

                let json = await response.json();

                if(json.success){

                    if(json.data === "It's deleted")
                    {
                        await this.props.setApp({
                            ...this.props.app,
                            favorite:[]
                        });
                    }
                    else {
                        let favorite = [];

                        json.data.map(item => {
                            item.id = parseInt(item.id);
                            favorite.push(item);
                        });

                        await this.props.setApp({
                            ...this.props.app,
                            favorite:favorite
                        });
                    }
                    await this.setState({
                        showLoadingModal:false
                    });
                }
                else {
                    await this.setState({
                        showLoadingModal:false
                    });
                }
            }

        } catch (error) {
            //console.log(error);
            await this.setState({
                showLoadingModal:false
            });
        }
    }

    async search(){
        this.searchBox.blur();
        await this.setState({
            page: 1,
            products: [],
            hasMore: true,
            loading: true,
            refreshing:false
        });
        await this.getProductsRequest();
    }
    onQueryChanged(text) {
        this.setState({
            query:text
        });
    }

    render() {

        return (
            <Container style={styles.mainContainer}>
                <Image resizeMode='stretch' style={[styles.mainBackground,{height:1000}]} source={require("./../../assets/images/shared/menu-bg.png")} />

                <Header style={styles.header}>
                    <Left style={{flex:1,flexDirection:'row',alignItems:'center',justifyContent:'flex-start'}}>
                        <BackButton/>
                        <BasketButton/>
                    </Left>
                    <Right style={{flex:2,alignItems:'center',justifyContent:'flex-end',paddingRight:5}}>
                        <HeaderRightLabel title='جستجوی محصول'/>
                    </Right>
                </Header>

                <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content" />

                <View style={styles.searchBoxContainer}>
                    <TextInput underlineColorAndroid="rgba(0,0,0,0)" style={styles.searchBoxInput}
                               placeholderTextColor="#cccccc" ref={ref => this.searchBox = ref}
                               placeholder='نام محصول مورد نظر را بنویس!' value={this.state.query}
                               onChangeText={this.onQueryChanged} onSubmitEditing={this.search} returnKeyType='search'
                    />

                    <TouchableOpacity style={styles.searchBoxButton} onPress={this.search}>
                        <Icon name="ios-search" style={styles.searchBoxIcon}/>
                    </TouchableOpacity>
                </View>

                <View style={{backgroundColor:'#ffffff',flex:1}}>
                    <FlatList
                        data={this.state.products}
                        renderItem={this.renderItem.bind(this)}
                        ListEmptyComponent={this.handleEmpty.bind(this)}
                        ListFooterComponent={this.renderFooter.bind(this)}
                        keyExtractor={(item) => item.id.toString()}
                        style={{backgroundColor:'transparent'}}
                        showsVerticalScrollIndicator={false}
                        refreshing={this.state.refreshing}
                        onRefresh={this.handleRefresh.bind(this)}
                        onEndReached={this.handleLoadMore.bind(this)}
                        onEndReachedThreshold={2}
                        removeClippedSubviews={true}
                    />
                </View>

                <LoadingModal show={this.state.showLoadingModal} />

            </Container>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setApp : app => {
            dispatch(setApp(app))
        }
    }
};

const mapStateToProps =(state) =>  {
    return{
        user: state.user,
        global:state.global,
        app:state.app
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(ProductSearch);