import React from 'react';
import {
    StatusBar,
    Image,
    ActivityIndicator,
    Text,
    TouchableOpacity,
    View,
    Linking,
    FlatList,
    PixelRatio,
    AsyncStorage
} from 'react-native';
import {Header, Icon, Left, Right, Content, Container, Body} from 'native-base';
import styles from './Style';
import { Actions } from 'react-native-router-flux';
import {setApp} from "../../redux/Actions/Index";
import {connect} from "react-redux";

import HeaderRightLabel from "../Shared/HeaderRightLabel";
import BackButton from "../Shared/BackButton";
import Helpers from "../Shared/Helper";
import Modal from "react-native-modal";
import LinearGradient from "react-native-linear-gradient";
import LoadingModal from "../Shared/LoadingModal";

class SelectAddress extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            address:[],
            loading:true,
            refreshing:false,
            showDeleteModal:false,
            deleteLoading:false,
            deleteResponse:'',
            deleteOk:false,
            showLoadingModal:false
        };

        this.deleteAddressText = "";
        this.selectedAddress = null;
        this.selectedAddressIndex = null;

        this.getAddressRequest = this.getAddressRequest.bind(this);
        this.showDeleteModal = this.showDeleteModal.bind(this);
        this.deleteAddress = this.deleteAddress.bind(this);

        this.closeCart = this.closeCart.bind(this);
    }

    async componentWillMount(){
        await this.getAddressRequest();
    }

    async getAddressRequest(){
        //console.log(this.state);
        if(await Helpers.CheckNetInfo()){
            return;
        }

        try {

            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("usertoken", this.props.user.apiToken);

            let response = await fetch(this.props.global.baseApiUrl + '/address/get',
                {
                    method: "POST",
                    body: formData
                });

            //console.log(response);

            if (response.status !== 200) {
                await this.setState({
                    loading:false,
                    refreshing:false
                });
            }
            else {

                let json = await response.json();

                if(json.success){
                    await this.setState({
                        address: json.data ,
                        loading: false,
                        refreshing: false
                    });
                }
                else {
                    await this.setState({
                        loading:false,
                        refreshing:false
                    });
                }
            }

        } catch (error) {
            console.log(error);
            await this.setState({
                loading: false,
                refreshing: false
            });
        }
    }

    renderItem({ item,index }) {
        return (
            <View style={[styles.addressListItem,{marginTop:index === 0 ? 0 : 20,marginBottom:index + 1 === this.state.address.length ? 20 : 0}]} key={item.id} >
                <View style={styles.addressListItemHeader}>
                    <Text style={styles.addressListItemHeaderText}>{item.fullname}</Text>

                    <TouchableOpacity activeOpacity={.7} style={styles.favoriteEditButton} onPress={() => this.closeCart(item.id)}>
                        <Text style={[styles.productHorizontalSectionHeaderMoreText,{fontSize:12,color:'#1fc792'}]}>انتخاب</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.addressListItemAddress}>
                    <Text style={styles.addressListItemAddressText}>شهر : {item.city}</Text>
                    <Text style={[styles.addressListItemAddressText,{marginTop:5}]}>آدرس : {item.address}</Text>
                </View>
                <View style={styles.addressListItemPhone}>
                    <View style={{flex:1,justifyContent:'center',alignItems:'center',paddingVertical:5}}>
                        <Text style={styles.addressListItemAddressText}>تلفن : {Helpers.ToPersianNumber(item.phone_number)}</Text>
                    </View>
                    <View style={{flex:1,justifyContent:'center',alignItems:'center',borderLeftWidth:5/PixelRatio.get(), borderLeftColor:'#eceff1',paddingVertical:5}}>
                        <Text style={styles.addressListItemAddressText}>کد پستی : {Helpers.ToPersianNumber(item.postcode)}</Text>
                    </View>
                </View>
                <View style={styles.addressListItemButtonSection}>
                    <TouchableOpacity activeOpacity={0.8} style={styles.addressListItemButton} onPress={() => this.showDeleteModal(item,index)}>
                        <Text style={[styles.addressListItemButtonText,{color:'#ff4141'}]}>حذف</Text>
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={0.8} style={styles.addressListItemButton} onPress={() => Actions.push('editAddress',{address:item,setter:this.stateSetter.bind(this)})}>
                        <Text style={[styles.addressListItemButtonText,{color:'#028dec'}]}>ویرایش آدرس</Text>
                    </TouchableOpacity>
                    {/*<TouchableOpacity activeOpacity={0.8} style={styles.addressListItemButton}>*/}
                    {/*<Text style={[styles.addressListItemButtonText,{color:'#ffba00'}]}>نمایش روی نقشه</Text>*/}
                    {/*</TouchableOpacity>*/}
                </View>
            </View>
        );
    }
    renderHeader(){
        return(
            <View style={[styles.productHorizontalSectionHeader,{paddingVertical:20}]}>
                <Text style={styles.productHorizontalSectionHeaderText}>لیست آدرس ها</Text>
                <TouchableOpacity activeOpacity={.7} style={styles.favoriteEditButton} onPress={() => Actions.push('addAddress',{setter:this.stateSetter.bind(this)})}>
                    <Text style={[styles.productHorizontalSectionHeaderMoreText,{fontSize:12,color:'#038eea'}]}>افزودن آدرس</Text>
                </TouchableOpacity>
            </View>
        );
    }
    handleEmpty() {
        if(this.state.loading)
        {
            return(
                <ActivityIndicator style={styles.listLoadingIndicator} color={this.props.global.grColorTwo} size='large' />
            );
        }
        else
        {
            return(
                <Text style={styles.notFoundText}>آدرسی یافت نشد.</Text>
            );
        }
    }
    async handleRefresh() {
        if(await Helpers.CheckNetInfo()){
            return;
        }

        await this.setState({ refreshing : true} ,async () => {
            await this.getAddressRequest();
        });
    }

    async stateSetter(value){
        await this.setState(value);
        //console.log(this.state);
    }

    async showDeleteModal(item,index){
        this.deleteAddressText = "آیا مایل به حذف آدرس به نشانی : '" + item.address + "' هستید؟";
        this.selectedAddress = item;
        this.selectedAddressIndex = index;

        await this.setState({
            showDeleteModal:true,
            deleteLoading:false,
            deleteResponse:'',
            deleteOk:false
        });
    }

    async deleteAddress(){

        await this.setState({
            deleteLoading:true
        });

        let address = this.selectedAddress;

        try {
            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("usertoken", this.props.user.apiToken);

            formData.append("add_id", address.id);

            let response = await fetch(this.props.global.baseApiUrl + '/address/delete',
                {
                    method: "POST",
                    body: formData
                });

            let json = await response.json();

            if(response.status === 200 && json.success)
            {
                let address = this.state.address;
                let index = this.selectedAddressIndex;
                address.splice(index,1);

                await this.setState({
                    address,
                    deleteLoading: false,
                    deleteOk: true,
                    deleteResponse: 'با موفقیت حذف شد.'
                });

            }
            else {

                this.setState({
                    deleteLoading: false,
                    deleteOk: false,
                    deleteResponse: 'با موفقیت انجام نشد!' + '\n' + 'دوباره تلاش کنید'
                });
            }

        } catch (error) {
            console.log(error);
            this.setState({
                deleteLoading: false,
                deleteOk: false,
                deleteResponse: 'ارتباط با سرور برقرار نشد!' + '\n' + 'دوباره تلاش کنید'
            });
        }
    }

    async closeCart(id){
        await this.setState({
            showLoadingModal:true
        });

        try {
            let notFreeItemIndex = this.props.app.cart.findIndex(a => a.sale_price !== '0' && a.price !== '' && a.price !== '0');

            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("usertoken", this.props.user.apiToken);
            formData.append("add_id", id);

            this.props.app.cart.map(item => {
                formData.append("product_item[" + item.id + "]", item.count);
            });

            if(this.props.coupon !== ''){
                formData.append("coupon", this.props.coupon);
            }

            let response = await fetch(this.props.global.baseApiUrl + '/order/create',
                {
                    method: "POST",
                    body: formData
                });

            let json = await response.json();

            if(response.status === 200 && json.success && notFreeItemIndex > -1)
            {
                let formData2 = new FormData();
                formData2.append("admintoken", this.props.global.adminToken);
                formData2.append("usertoken", this.props.user.apiToken);
                formData2.append("order_id", json.data.order_id);

                let response2 = await fetch(this.props.global.baseApiUrl + '/order/payorder',
                    {
                        method: "POST",
                        body: formData2
                    });

                let json2 = await response2.json();

                if(response2.status === 200 && json2.success)
                {
                    await this.props.setApp({
                        ...this.props.app,
                        cart:[]
                    });
                    await AsyncStorage.removeItem("cart");

                    this.setState({
                        showLoadingModal: false
                    });
                    Linking.openURL(this.props.global.baseApiUrl + "/order/paybyurl/?payurl=" + json2.data.payurl);
                    Actions.reset('root');
                }
                else {
                    this.setState({
                        showLoadingModal: false
                    });
                }

            }
            else {
                if(notFreeItemIndex === -1){
                    await this.props.setApp({
                        ...this.props.app,
                        cart:[]
                    });
                    await AsyncStorage.removeItem("cart");
                    this.setState({
                        showLoadingModal: false
                    });
                    Actions.reset('root');
                }
                else {
                    this.setState({
                        showLoadingModal: false
                    });
                }
            }

        } catch (error) {
            console.log(error);
            this.setState({
                showLoadingModal: false
            });
        }
    }

    render() {

        return (
            <Container style={styles.mainContainer}>
                <Image resizeMode='stretch' style={[styles.mainBackground,{height:1000}]} source={require("./../../assets/images/shared/menu-bg.png")} />

                <Header style={styles.header}>
                    <Left style={{flex:1,flexDirection:'row',alignItems:'center',justifyContent:'flex-start'}}>
                        <BackButton/>
                    </Left>
                    <Right style={{flex:2,alignItems:'center',justifyContent:'flex-end'}}>
                        <HeaderRightLabel title='انتخاب آدرس'/>
                    </Right>
                </Header>

                <StatusBar backgroundColor="transparent" translucent={true} barStyle="light-content" />

                <View style={[styles.productHorizontalSection,{paddingTop:0,flex:1,paddingVertical:0}]}>
                    <FlatList
                        data={this.state.address}
                        renderItem={this.renderItem.bind(this)}
                        ListEmptyComponent={this.handleEmpty.bind(this)}
                        ListHeaderComponent={this.renderHeader.bind(this)}
                        keyExtractor={(item) => item.id.toString()}
                        style={styles.addressList}
                        showsVerticalScrollIndicator={false}
                        refreshing={this.state.refreshing}
                        onRefresh={this.handleRefresh.bind(this)}
                    />
                </View>

                <Modal isVisible={this.state.showDeleteModal} backdropOpacity={0.5} animationIn="fadeIn" animationOut="fadeOut">
                    {(!this.state.deleteLoading && this.state.deleteResponse === '') && <View style={styles.logOutModal}>
                        <Icon name="md-alert" style={{color:'#ffb941',fontSize:50}} />
                        <Text style={[styles.modalLoadingText,{textAlign:'center'}]}>
                            {this.deleteAddressText}
                        </Text>
                        <View style={{flexDirection:'row',marginTop:25,justifyContent:'center',alignItems:'center'}}>

                            <TouchableOpacity style={[styles.modalColorGrButton]} activeOpacity={.8} onPress={() => { this.setState({showDeleteModal:false}); }}>
                                <LinearGradient colors={[this.props.global.grColorOne,this.props.global.grColorTwo]} style={styles.modalGrButtonGradient} start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}>
                                    <Text style={styles.modalGrButtonText}>خیر</Text>
                                </LinearGradient>
                            </TouchableOpacity>

                            <TouchableOpacity style={[styles.modalColorGrButton,{marginLeft:15}]} activeOpacity={.8} onPress={this.deleteAddress}>
                                <LinearGradient colors={[this.props.global.grColorOne,this.props.global.grColorTwo]} style={styles.modalGrButtonGradient} start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}>
                                    <Text style={styles.modalGrButtonText}>بلی</Text>
                                </LinearGradient>
                            </TouchableOpacity>

                        </View>
                    </View>}
                    {this.state.deleteLoading && <View style={styles.logOutModal}>

                        <View style={{flexDirection:'row',justifyContent:'center',alignItems:'center'}}>

                            <ActivityIndicator color={this.props.global.grColorTwo} size='large'/>

                        </View>
                    </View>}
                    {(!this.state.deleteLoading && this.state.deleteResponse !== '') && <View style={styles.logOutModal}>
                        {!this.state.deleteOk && <Icon name="md-information-circle" style={{color:'#fd5459',fontSize:50}} />}
                        {this.state.deleteOk && <Icon name="md-checkmark-circle" style={{color:'#2fcc71',fontSize:50}} />}
                        <Text style={[styles.modalLoadingText,{textAlign:'center'}]}>
                            {this.state.deleteResponse}
                        </Text>
                        <View style={{flexDirection:'row',marginTop:25,justifyContent:'center',alignItems:'center'}}>

                            <TouchableOpacity style={[styles.modalColorGrButton]} activeOpacity={.8} onPress={() => { this.setState({showDeleteModal:false}); }}>
                                <LinearGradient colors={[this.props.global.grColorOne,this.props.global.grColorTwo]} style={styles.modalGrButtonGradient} start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}>
                                    <Text style={styles.modalGrButtonText}>تایید</Text>
                                </LinearGradient>
                            </TouchableOpacity>

                        </View>
                    </View>}
                </Modal>

                <LoadingModal show={this.state.showLoadingModal} />
            </Container>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setApp : app => {
            dispatch(setApp(app))
        }
    }
};

const mapStateToProps =(state) =>  {
    return{
        user: state.user,
        global:state.global,
        app:state.app
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(SelectAddress);