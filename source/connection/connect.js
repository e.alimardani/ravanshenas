import React, {Component} from 'React';
import {NetInfo,} from 'react-native';

export default class ServerData extends Component {
    state = {
        isConnected: true
    };

    componentDidMount() {
        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected) {
                this.setState({isConnected: true});
            } else {
                this.setState({isConnected: false});
            }
        });
        NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectivityChange);
    }

    componentWillUnmount() {
        NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectivityChange);
    }


    handleConnectivityChange = isConnected => {

        if (isConnected) {
            this.setState({isConnected});
        } else {
            this.setState({isConnected});
        }
    };

    async getData(url) {
        if (!this.state.isConnected) {
            // Toast.show("لطفا اینترنت خود را روشن کنید");
            return 2;
        }
        return await fetch(url)

            .then((response) => {

                if (response.status === 200) {
                    if (response.ok) {
                        return response.json();
                    } else {
                        return 0;
                    }
                } else return 0;

            })
            .catch((error) => {
                console.error(error);
            });
    }

    async sendPost(url, arrayJson) {

        if (!this.state.isConnected) {

            return 2;
        }
        let details = arrayJson;
        let formBody = [];
        for (const property in details) {
            const encodedKey = encodeURIComponent(property);
            const encodedValue = encodeURIComponent(details[property]);
            formBody.push(encodedKey + "=" + encodedValue);
        }
        formBody = formBody.join("&");
        return await fetch(url, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/x-www-form-urlencoded',
                },
                body: formBody
            }
        )
            .then((response) => {
                if (response.status === 200) {
                    if (response.ok) {
                        return response.json();
                    } else {
                        return 0;
                    }

                } else return 0;
            })


            .catch((error) => {
                console.error(error);
            });
    }


}